﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track
{
    class point
    {
//-------data------------------------------------------
        private double _x;
        private double _y;
//------constructors------------------------------------
        public point()
        {
            _x = 0;
            _y = 0;
        }
        public point(double x,double y)
        {
            _x = x;
            _y = y;
        }
//--------methods--------------------------------------
        public double x
        {
            get { return _x; }
        }
        public double y
        {
            get { return _y; }
        }
        public override string ToString()
        {
            return "x: " + _x + "y: " + _y;
        }
    }
}
