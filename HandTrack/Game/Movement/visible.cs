﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/******************************************************************************
 * this class represent the time range that the red circle disapeare          *
 * the time is random double number between the range that define for him     *
 * this class replace the Inv_supprot class that writen by Boaz Ben-Moshe     *
 * writen by Eliya Ben-Abu                                                    *
 * ****************************************************************************/
namespace hend_track
{
    class visible
    {
//----------data---------------------------------------------------------------------
        Range _range;//time to not visible range
        int _oldtime; 
        double _time_to_disapeare;//the random time in the range that the red circle disapeare 
        bool _isVisible;

        public bool IsVisible
        {
            get { return _isVisible; }
            set { _isVisible = value; }
        }
        int _time_to_show;
//-------constructor---------------------------------------------------------------
        public visible(double  mintime, double maxtime,int timetoshow)
        {
            _oldtime = 2;
            _range = new Range(mintime,maxtime);
            _isVisible = false;
            _time_to_show = timetoshow;
        }
//-------property-------------------------------------------------------------------
        //represenet the visible  
        public bool isVisible
        {
            get { return _isVisible; }
            set {_isVisible=value;}
        }
//------method---------------------------------------------------------------------
        //check if the time to change the red circle visible is coming and change it 
        public void updatetime(int time)
        {
            if((!_isVisible)&& (time-_oldtime>_time_to_disapeare))
            {
                _isVisible = !_isVisible;
                _time_to_disapeare = _range.random();
                _oldtime = time;
            }
            if ((_isVisible) && (time - _oldtime > _time_to_show))
            {
                _isVisible = !_isVisible;
                _oldtime = time;
            }
        }
       
    }
}
