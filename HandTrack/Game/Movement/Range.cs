﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/**
 * this code copyied from java code that writen by boaz ben moshe for this program
 * this class represent range between tow number  
 * */

namespace hend_track
{
    class Range
    {
        private double _min, _max;
        Random r;
//-----------constructor--------------------------------------------------------------------------------
        public Range(double min, double max) 
        {
            _min = Math.Min (min,max ); 
            _max = Math.Max (min, max);
            r = new Random();
        }
//---------methods--------------------------------------------------------------------------
        //property
        public double min
        {
            get { return _min; }
        }
        public double max
        {
            get { return _max; }
        }
        
        //this method cheks if the number is in the range
        public Boolean isIn(double v) 
        {
            return v > _min & v < _max; 
        }
        //this methods return the size of range
        public double dx() 
        {
            return _max - _min; 
        }
        //return random number from the range
        public double random() 
        {
            return _min + (r.NextDouble() * dx());
        }
        //return the string of the range
        public String toString() { return "[" + _min + "," + _max + "]"; }

    }
}
