﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Timers;
using System.IO;
/******************************************************************************************
 * this code copyied from java code that writen by boaz ben moshe for this program        *
 * This class represents the main logical model used for                                  *
 * moving the target                                                                      *
 * there is a word document that explain this class at button help or at folder library.  *
 * ****************************************************************************************/
namespace hend_track
{

    class movement_model
    {
//--------Data------------------------------------------------------------------------------
        public readonly int MIRO = 1, PWL = 2;
        private double _speed; // current speed
        public double _angle; // current  [0,2*PI]
        private Rectangle _frame; // the target (red ball) frame
        private double _scale; // linear scale (currently not in use)
        private double _da; // delta angle
        private double _dda; // delta^2 angle
        private point _curr; // current ball (target) position
        private long _start = 0, _last_time;
        private int _type; // MIRO, PWL
        private Range _event_time; // time range: change in angle (PWL/MIRO)
        private double _next_event_t = 0;
        private double _dv;
        visible _inv = null; // supports invisible opp.
        Random random;
        private bool validModel = false;
        //------------------constructor--------------------------------------------------------------------------------------------------
        public movement_model(int type, double speed, Rectangle rectangle, double scale, double delta, double dv, double min_t, double max_t)
        {
            if (type==3)
            {
                type = 2;
                _inv = new visible(0,2,4);
                _inv.isVisible = true;
            }

            if (type == 4)
            {
                type = 1;
                _inv = new visible(0, 2, 4);
                _inv.isVisible = true;
            }

            random = new Random();
            _type = type;
            _frame = new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
            _curr = new point(_frame.Left + (_frame.Width / 2), _frame.Top + (_frame.Height / 2));
            _angle = random .Next (0,(int)(2*Math .PI) );
            _speed = speed*scale ;
            _scale = scale;
            _dv = dv;
            _da = delta;
            _dda = 1;
            _event_time = new Range(min_t, max_t);
            if (_frame.Width * _frame.Height != 0)
                validModel = true;

        }
        //---------------methods----------------------------------------------------------------------------------------------------------
        public void updateInv(int time)
        {
           _inv.updatetime(time);
        }

        public Boolean isVisible()
        {
            if (_inv == null) return true;
            return _inv.isVisible;
        }
        public String toString()
        {
            return "Movment Model: type: " + _type + ", V: " + _speed + ", DV: " + _dv + ", DA: " + _da + ", time range " + _event_time.toString();
        }

        public void start()
        {
            _last_time = this.GetTime();
            _start = _last_time;
            _next_event_t = 5;
        }

        public Boolean isRunning() { return _start != 0; }

        public point  next()
        {
            if (validModel)
            {
                if (!isRunning())
                    this.start();
                double dt = _last_time;
                _last_time = this.GetTime();
                dt = _last_time - dt;
                _curr = move(dt / 1000.0);
            }
            return _curr;
        }

        /** private methods: the actual models */
        private point  move(double time)
        {

            point ans = new point(_curr.x, _curr.y);
            if (isVisible())
                updateAngle(time);

            updateSpeed(time);
            ans = updateLocation(time);
            Point temp_point = new Point((int)ans.x, (int)ans.y);
            if (!_frame.Contains(temp_point ))
            {
                hitFrame(ans);
                ans = updateLocation(time);
            }
            while (!_frame.Contains(temp_point ))
            {
                _angle = random.NextDouble() * Math.PI * 2;
                ans = updateLocation(time);
                temp_point = new Point((int)ans.x, (int)ans.y);
            }
            return ans;
        }

        private void hitFrame(point p)
        {
            if (containsDX(p))
            {
                _angle = 2 * Math.PI - _angle;
            }
            else { _angle = Math.PI - _angle; }
        }
         
        private void updateSpeed(double time)
        {
            double delta = _dv;
            double dv = time * ((random.NextDouble() * delta) - (delta / 2));
            this._speed =_speed + dv;
        }
    
        private point  updateLocation(double time)
        {
            double dx = Math.Cos(_angle) * _speed * time;
            double dy = Math.Sin(_angle) * _speed * time;
            double x = _curr.x + dx;
            double y = _curr.y + dy;
            point ans = new point(x,y);
            return ans;
        }

        private Boolean myevent()
        {
            Boolean ans = false;
            double curr_t = (this.GetTime() - this._start) / 1000.0;
            
            if (_next_event_t < curr_t)
            {
                
                ans = true;
                _next_event_t =_next_event_t + _event_time.random();//return number between the range _event_time
                
            }
            return ans;
        }

        private void updateAngle(double time)
        {
            if (myevent())
            {
                if (_type == MIRO) _dda =_dda * -1;
                if (_type == PWL) _angle = random.NextDouble() * 2 * Math.PI;//angle between 0 and 2pi
            }
            
            if (_type == MIRO)
            {
                this._angle =_angle +( _da * _dda * (0.5 + (random.NextDouble() * 1.5)));

                
                
            }
            

        }

        public Boolean containsDX(point p)
        {
            Boolean contain = false;
            if (p != null)
            {
                if ((p.x  > _frame.X) && (p.y < _frame.Width ))
                    contain = true;
            }
            return contain;
        }
        //return the time since 1970 in millisecond this is like a function get time in class date in java 
        private long GetTime()
        {
            long retval = 0;
            DateTime st = new DateTime(1970, 1, 1);
            TimeSpan t = (DateTime.Now - st);
            retval = (long)(t.TotalMilliseconds + 0.5);
            return retval;
        }
      
    }

}


