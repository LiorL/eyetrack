﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LiorGraphics
{

    class FrameMonitor
    {
        struct RenderData
        {
            long rendertime;
            long timeStamp;
        }

        //Window Render time (Real FPS)
        Queue <RenderData> queRenderData;
        RenderData[] renderData;
        const int NumberOfFrames = 40;
        int FpsChangeThreshold = 1000;
        int FpsReadTimeStamp;
        int DesiredFPS;
        int LastTimeStamp;
        int[] Timings;
        int CurrentFrame;
        int TotalTime;
        int LastFpsRead;

        //FPS
        int DrawStart;
        int[] DrawPreSec;
        int CurrentSec;
        int SecondsInitalized;
        const int NumberOfSeconds = 2;


        public FrameMonitor(int DesiredFrameRate)
        {
            DesiredFPS = DesiredFrameRate;
            Timings = new int[NumberOfFrames];
            DrawPreSec = new int[NumberOfSeconds];
            queRenderData = new Queue<RenderData>();
        }
        public void StartFrame()
        {
            ///////////////////////////////
            LastTimeStamp = Environment.TickCount;
        }
        public void EndFrame()
        {
            if (Environment.TickCount - DrawStart > 1000)
            {
                if (SecondsInitalized < NumberOfSeconds)
                    SecondsInitalized++;

                DrawStart = Environment.TickCount;
                CurrentSec = (CurrentSec + 1) % NumberOfSeconds;
                DrawPreSec[CurrentSec] = 0;
            }
            DrawPreSec[CurrentSec]++;


            int NewTime = Environment.TickCount - LastTimeStamp;
            TotalTime -= Timings[CurrentFrame];
            Timings[CurrentFrame] = NewTime;
            TotalTime += Timings[CurrentFrame];

            //Advance to next frame
            CurrentFrame++;
            CurrentFrame %= NumberOfFrames;
            //fThrowFrames := (ftotaltime div cNumberofFrames) > cDesiredWindowTime ;

        }

        public int GetFPS()
        {
            float fps = 0;
            if (SecondsInitalized > 0)
            {
                if (SecondsInitalized == 1)
                    fps = DrawPreSec[CurrentSec];
                else
                {
                    for (int i = 0; i < SecondsInitalized - 1; i++)
                        fps += DrawPreSec[(CurrentSec - 1 - i + NumberOfSeconds) % NumberOfSeconds];

                    int elapsed = (Environment.TickCount - DrawStart);

                    if (elapsed != 0)
                    {
                        fps += (int)(1000.0 / elapsed * DrawPreSec[CurrentSec]);
                        fps /= SecondsInitalized;
                    }
                    else
                        fps /= (SecondsInitalized - 1);
                }
            }

            return (int)fps;
        }

        public int GetRealFPS()
        {
            if (Environment.TickCount - FpsReadTimeStamp > FpsChangeThreshold)
            {
                FpsReadTimeStamp = Environment.TickCount;
                int fps = 0;
                for (int i = 0; i < NumberOfFrames; i++)
                    fps += Timings[i];

                LastFpsRead = (fps == 0) ? 0 : (int)(1000.0 / fps * NumberOfFrames);
                return LastFpsRead;
            }
            else return LastFpsRead;

        }
    }
}
