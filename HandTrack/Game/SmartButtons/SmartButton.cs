﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace SmartButtons
{
    public class SmartButton
    {
        //private SmartButtonType type;
        private SmartButtonState state;
        private int lastTimePress;
        private int timePress;
        private bool alwaysSendEvents;
        private int buttonID;
        private bool clicked;

        private  InputDevice device;

        public InputDevice Device
        {
            get { return device; }
            //set { deviceid = value; }
        }


        public SmartButton(InputDevice aDevice, int aCurrentButton)
        {
            device = aDevice;
            buttonID = aCurrentButton;
            state = SmartButtonState.Up;
        }

        public bool DownFirst { get { return state == SmartButtonState.DownFirst; } }

        public bool Down { get { return state == SmartButtonState.Down || state == SmartButtonState.DownFirst; } }

        public int TimePressed
        {
            get { return Environment.TickCount - timePress; }
        }

        public SmartButtonState State { get { return state; } }

        //public SmartButtonType Type { get { return type; } }

        public int LastTimePressed { get { return lastTimePress; } }

        public bool Clicked { get { return clicked; } }

        public int ButtonID { get { return buttonID; } }


        internal void SetFired()
        {
            state = SmartButtonState.Fired;
        }

        internal bool SetButtonUp()
        {
            lastTimePress = Environment.TickCount - timePress;
            timePress = 0;
            clicked = false;
            bool modified = false;
            if (state == SmartButtonState.DownFirst || state == SmartButtonState.Down)
                clicked = true;
            switch (state)
            {
                case SmartButtonState.Up:
                    break;
                default:
                    state = SmartButtonState.Up;
                    modified = true;
                    break;
            }
            return modified || alwaysSendEvents;

        }

        internal bool SetButtonDown()
        {
            clicked = false;
            bool modified = true;
            switch (state)
            {
                case SmartButtonState.Up:
                    state = SmartButtonState.DownFirst;
                    timePress = Environment.TickCount;
                    break;
                case SmartButtonState.DownFirst:
                    state = SmartButtonState.Down;
                    break;
                default:
                    modified = false;
                    break;
            }

            return modified || alwaysSendEvents;
        }

        internal void SetOptions(SmartButtonOptions aOptions)
        {
            switch (aOptions)
            {
                case SmartButtonOptions.None:
                    alwaysSendEvents = false;
                    break;
                case SmartButtonOptions.AlwaysSendEvents:
                    alwaysSendEvents = true;
                    break;

            }
        }
    }
}
