﻿using System;
namespace SmartButtons
{
    public enum SmartButtonState { Up, DownFirst, Down, Fired }
    //public enum SmartDeviceType { None, KeyBoard, Joystick }
    
    [Flags]
    public enum SmartButtonOptions { None, AlwaysSendEvents }
    [Flags]
    public enum InputDeviceOptions { None, AlwaysSendEvents }

}