﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartButtons
{
    public class InputManager
    {
        #region Singelton

        public delegate void OnButtonPressDelegate(SmartButton aButton);
        public event OnButtonPressDelegate OnButtonPress;
        public delegate void NeedTOSendUpdateDelegate(SmartButton button);
        private Queue<SmartButton> fButtonUpdateQueue = new Queue<SmartButton>();
        static private object fObjLock = new object();
        static private InputManager fInstance;

        static public InputManager Instance
        {
            get
            {
                if (fInstance == null)
                    lock (fObjLock)
                        if (fInstance == null)
                            fInstance = new InputManager();
                return fInstance;
            }
        }


        private InputManager()
        {
        }
        #endregion

        public InputDevice CreateDevice()
        {
            return new InputDevice(new NeedTOSendUpdateDelegate(AddToUpdateQueue));
        }

        public void HandleInput(InputDevice aDevice ,int aButtonID, bool aIsPressed)
        {
            aDevice.HandleInput(aButtonID, aIsPressed);
        }
       

        
        
        internal void AddToUpdateQueue(SmartButton aButton)
        {
            fButtonUpdateQueue.Enqueue(aButton);
        }

        public void GetUpdates()
        {
            if (OnButtonPress != null)
                while (fButtonUpdateQueue.Count > 0)
                    OnButtonPress(fButtonUpdateQueue.Dequeue());
        }

        public void Flush()
        {
            fButtonUpdateQueue.Clear();
        }

    }
}
