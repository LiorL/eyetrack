﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartButtons
{
    public class InputDevice
    {
        InputDeviceOptions fOptions = InputDeviceOptions.None;
        Dictionary<int, SmartButton> dicSmartButtons;
        private  InputManager.NeedTOSendUpdateDelegate fQueueDelegate;

        public InputDevice(InputManager.NeedTOSendUpdateDelegate aQueueDelegate )
        {
            dicSmartButtons = new Dictionary<int, SmartButton>();
            fQueueDelegate = aQueueDelegate;
        }

        public SmartButton GetButton(int aButtonID)
        {
            SmartButton button = null;
            dicSmartButtons.TryGetValue(aButtonID, out button);
            return button;
                
        }

        public void SetDeviceOptions(InputDeviceOptions aOptions)
        {
            fOptions = aOptions;
        }

        public void HandleInput(int aButtonID, bool aIsPressed)
        {
            SmartButton button;

            if (!dicSmartButtons.TryGetValue(aButtonID, out button))
                dicSmartButtons.Add(aButtonID, button = new SmartButton(this, aButtonID));

            bool modified = aIsPressed ? button.SetButtonDown() : button.SetButtonUp();
            if (modified || fOptions == InputDeviceOptions.AlwaysSendEvents)
                fQueueDelegate(button);

        }

        public InputDeviceOptions DeviceOptions { get { return fOptions; } }

        //public void SetButtonOptions(int aDeviceID, int aButtonID, SmartButtonOptions aOptions)
        //{

        //    SmartButton button =  GetButton(aDeviceID, aButtonID);
        //    button.SetOptions(aOptions);
        //}

    }
}
