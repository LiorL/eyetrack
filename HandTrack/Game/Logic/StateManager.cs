﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track.Logic
{
    public class StateManager
    {
        #region Singelton
        static private object fObjLock = new object();
        static private StateManager fInstance;

        static public StateManager Instance
        {
            get
            {
                if (fInstance == null)
                    lock (fObjLock)
                        if (fInstance == null)
                            fInstance = new StateManager();
                return fInstance;
            }
        }
        private StateManager()
        {
            fDicStateInstance = new Dictionary<string, StateInstance>();
        }
        #endregion

        private Dictionary<string, StateInstance> fDicStateInstance;

        public StateInstance GetOrCreateStateInstance(string aStateInstanceName)
        {
            StateInstance stateInstance;
            if (!fDicStateInstance.TryGetValue(aStateInstanceName, out stateInstance))
            {
                stateInstance = new StateInstance(aStateInstanceName);
                fDicStateInstance[aStateInstanceName] = stateInstance;
            }
            return stateInstance;
        }
    }
}