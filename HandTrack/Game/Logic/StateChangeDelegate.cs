﻿namespace hend_track.Logic
{
    public delegate void StateChangeDelegate(SequenceInstance aInstance);
    public delegate bool StateChangeCheck(SequenceInstance aInstance);
}