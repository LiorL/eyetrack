﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track.Logic
{
    public class StateInstance
    {
        #region Private Fields
        Dictionary<string, List<string>> fDicSequences;
        Dictionary<string, StateDescriptor> fDicStates;
        #endregion

        private string fName;

        public string Name
        {
            get { return fName; }
        }

        internal  StateInstance(string aStateInstanceName)
        {
            fDicSequences       = new Dictionary<string, List<string>>();
            fDicStates          = new Dictionary<string, StateDescriptor>();
            fName               = aStateInstanceName;
        }

        public SequenceInstance CreateSequenceInstance(string aSequenceName,object aUserObject)
        {
            return new SequenceInstance(this,aSequenceName, aUserObject);
        }
        
        public void SetSequenceStates(string aSequenceName, params string[] aStates)
        {
            List<string> sequence = new List<string>(aStates);
            fDicSequences[aSequenceName] = sequence;
        }

        public void CopySequenceStates(string aSequenceSource, string aSequenceTarget)
        {
            List<string> sequence = new List<string>(fDicSequences[aSequenceSource]);
            fDicSequences[aSequenceTarget] = sequence;
        }

        public void DeleteStateForSequence(string aSequenceName, string aStateName)
        {
            List<string> sequence = fDicSequences[aSequenceName];
            sequence.Remove(aStateName);
        }


        public void AddStateHandler(string aStateName, StateChangeDelegate aOnEnter, StateChangeDelegate aOnLeave,StateChangeCheck aOnCanChange)
        {
            fDicStates[aStateName] = new StateDescriptor() { OnEnter = aOnEnter, OnLeave = aOnLeave,OnCanChange = aOnCanChange };
        }

        public bool SequenceExists(string aSequenceName)
        {
            return fDicSequences.ContainsKey(aSequenceName);
        }

        public string GetSequenceFirstState(string aSequenceName)
        {
            if (fDicSequences.ContainsKey(aSequenceName))
                return fDicSequences[aSequenceName].FirstOrDefault();
            else
                return null;
        }

        internal void MoveToNextState(SequenceInstance aInstance)
        {
            List<string> sequence = fDicSequences[aInstance.SequenceName];
            int currentIndex = aInstance.StateIndex;
            string oldStateName = sequence[currentIndex];
            int newIndex = (currentIndex + 1) % sequence.Count;
            string newStateName = sequence[newIndex];

            StateDescriptor oldDesc = fDicStates[oldStateName];
            StateDescriptor newDesc = fDicStates[newStateName];

            if (oldDesc.OnCanChange == null || (oldDesc.OnCanChange != null && oldDesc.OnCanChange(aInstance)))
            {
                if (oldDesc.OnLeave != null)
                    oldDesc.OnLeave(aInstance);
                
                aInstance.StateIndex = newIndex;
                
                if (newDesc.OnEnter != null)
                    newDesc.OnEnter(aInstance);
             
            }
        }
    }
}
