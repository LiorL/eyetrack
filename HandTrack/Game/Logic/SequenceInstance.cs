﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track.Logic
{

    //Holds a sequence of states
    public class SequenceInstance
    {
        private string          fSequenceName;
        private StateInstance   fStateInstance;
        private object          fUserObject;

        private int             fStateIndex;

        public object Userbject {get { return fUserObject;}}

        public int StateIndex
        {
            get { return fStateIndex; }
            set { fStateIndex = value; }
        }
        public string SequenceName { get { return fSequenceName; } }

        
        public SequenceInstance(StateInstance aStateInstabce, string aSequenceName,object aUserObject)
        {
            fStateInstance = aStateInstabce;
            fUserObject = aUserObject;
            fStateIndex = 0;
            if (!fStateInstance.SequenceExists(aSequenceName))
                throw new Exception("Sequence does not exists");
            else
            {
                fSequenceName = aSequenceName;
            }
        }

        public void Next()
        {
            fStateInstance.MoveToNextState(this);   
        }
    }
}
