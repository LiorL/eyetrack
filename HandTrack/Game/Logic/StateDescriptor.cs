﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track.Logic
{
    internal class StateDescriptor
    {
        private StateChangeCheck fCanChange;

        public StateChangeCheck OnCanChange
        {
            get { return fCanChange; }
            set { fCanChange = value; }
        }

        private StateChangeDelegate fOnEnter;

        internal StateChangeDelegate OnEnter
        {
            get { return fOnEnter; }
            set { fOnEnter = value; }
        }


        private StateChangeDelegate fOnLeave;

        internal StateChangeDelegate OnLeave
        {
            get { return fOnLeave; }
            set { fOnLeave = value; }
        }
        
    }
}
