﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track
{
    public  class RankUpdate
    {
        public int rank;
        public bool locked;
        public RankUpdate(int aRank, bool aLocked)
        {
            rank = aRank;
            locked = aLocked;
        }
    }
}
