﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
    public class CommonMessage
    {
        public MessageType type;
        public object data;
        
        public CommonMessage(MessageType aType, object aData)
        {
            type = aType;
            data = aData;
        }
    }

