﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
/***************************************************************
 * this class represent object2ds collection                   * 
 * this class extends from Dictionary<string, Object2d>        *
 * writen by Eliya Ben-Abu                                     * 
 * *************************************************************/
namespace hend_track
{
    class Objects2d : Dictionary<string, Object2d>
    {
        //this function call to the function draw in all the objects
        public void Draw(SpriteBatch spritebatch)
        {
            foreach (KeyValuePair<string, Object2d> objPair in this)
                objPair.Value.Draw(spritebatch);
        }
    }
}
