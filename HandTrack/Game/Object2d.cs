﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics;
namespace hend_track
{
    class Object2d
    {
        Texture2D   fTexture;
        Rectangle   fDestRect;
        Vector2     fSize;
        Vector2     fPosition;
        Color       fBlendColor;
        bool        fVisible;
        int         fFrames;
        double      fFrameWidth;
        double      fFrameheight;
        Rectangle   fSourceRect;
        float       fOpacity;
        int         fCurrentFrame;
        

        public Object2d( Texture2D  aTexture, Vector2 aSize, Vector2 aPosition)
        {
            fCurrentFrame = -1;
            fFrames = 0;
            fOpacity = 1.0f;
            fSize = aSize;
            fPosition = aPosition;
            fBlendColor = Color.White;
            IsVisible = true;
            
            Texture = aTexture;
            
            UpdateDestRect();
        }

        public Object2d(Texture2D aTexture) : this(aTexture, Vector2.Zero, Vector2.Zero) { }

        public Object2d()
        {
        
        }

        public int CurrentFrame { 
            get { return fCurrentFrame; } 
            set {
                if (fCurrentFrame != value)
                {
                    fCurrentFrame = value;
                    UpdateSourceRect();
                    
                }
            } }

        public int TotalFrames { get { return fFrames; } 
            set 
            {
                if (fFrames != value)
                {
                    fFrames = value;
                    RefreshSource();
                }
            } 
        }

        private void RefreshSource()
        {
            if (fTexture != null)
            {
                if (fFrames == 0)
                    fFrames = 1;
                fFrameWidth = (fTexture.Width / fFrames);
                fFrameheight = fTexture.Height;
                fCurrentFrame = 0;
            }

            UpdateSourceRect();
        }

        public Texture2D Texture
        {
            get { return fTexture; }
            set 
            {
                fTexture = value;

                RefreshSource();
            }
        }

        private void UpdateSourceRect()
        {
            if (fCurrentFrame != -1)
                fSourceRect = new Rectangle((int)(fFrameWidth * fCurrentFrame), 0, (int)fFrameWidth, (int)fFrameheight);
        }

        public Vector2 Size
        {
            get { return fSize; }
            set
            {
                fSize = value;
                UpdateDestRect();
            }
        }

        public bool IsVisible
        {
            get
            {
                return fVisible;
            }
            set
            {
                fVisible = value;
            }
        }

        public Vector2 Position
        {
            get { return fPosition; }
            set
            {
                fPosition = value;
                UpdateDestRect();

            }
        }
        public Color BlendColor
        {
            set { fBlendColor = value; }
        }

        public Rectangle DestinationRect
        {
            get
            {
                return fDestRect;
            }
            set
            {
                fPosition = new Vector2(value.X, value.Y);
                fSize = new Vector2(value.Width, value.Height);
                fDestRect = value;
            }
        }

        private void UpdateDestRect()
        {
            fDestRect = new Microsoft.Xna.Framework.Rectangle((int)(fPosition.X - fSize.X / 2), (int)(fPosition.Y - fSize.Y / 2), (int)Size.X, (int)Size.Y);
        }
        
        public void Draw(SpriteBatch spritebatch)
        {
            if ((fVisible) && (fTexture != null))
                spritebatch.Draw(fTexture, fDestRect, fSourceRect, fBlendColor);
        }

        public int distance(Object2d o)
        {
            int x = Math.Abs((int)this.fPosition.X - (int)o.fPosition.X);
            int y = Math.Abs((int)this.fPosition.Y - (int)o.fPosition.Y);
            return (int)Math.Sqrt((x * x) + (y * y));
        }

        public bool IsContain(Object2d o)
        {
            Point p1 = this.fDestRect.Center;
            Point p2 = o.fDestRect.Center;
            int x = Math.Abs(p1.X - p2.X);
            int y = Math.Abs(p1.Y - p2.Y);
            int distance = (int)Math.Sqrt((x * x) + (y * y));
            return distance < (this.fSize.X / 2);
        }

    }
}
    

