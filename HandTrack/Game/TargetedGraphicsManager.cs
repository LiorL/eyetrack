﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using hend_track;
using System.Diagnostics;

public class TargetedGraphicsDeviceManager : GraphicsDeviceManager
{
    private int target = 1;
    private bool ranked = false;

    public TargetedGraphicsDeviceManager(Game game, int displayTarget)
        : base(game)
    {
        target = displayTarget;
    }

    protected override void RankDevices(List<GraphicsDeviceInformation> foundDevices)
    {
        if (ranked)
            return;
        List<GraphicsDeviceInformation> removals = new List<GraphicsDeviceInformation>();

        for (int i = 0; i < foundDevices.Count; i++)
            if (i != target)
                removals.Add(foundDevices[i]);

        foreach (GraphicsDeviceInformation info in removals)
            foundDevices.Remove(info);
 
        base.RankDevices(foundDevices);
        ranked = true;
    }
}