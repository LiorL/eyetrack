﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track.Questions
{
    class QuestionCollection
    {
        //private QuestionType fCurrentQuestion;
        private Dictionary<QuestionType, QuestionData> fDicCollection;
        private QuestionType[] fQuestions;
        private QuestionEntity[] fEntities;
        private int fcurrentQuestionIdx = 0;
        
        public QuestionCollection(QuestionType[] aTypes, QuestionEntity[] aEntities )
        {
            fQuestions = aTypes;
            fEntities = aEntities;
            fDicCollection = new Dictionary<QuestionType, QuestionData>();
            //fCurrentQuestion = QuestionType.None;
            
            foreach (QuestionType type in aTypes)
                fDicCollection.Add(type, new QuestionData(fEntities));
        }
        public List<ResultRecord> FormatResults()
        {
            List<ResultRecord> results = new List<ResultRecord>();
            foreach (KeyValuePair<QuestionType, QuestionData> pair in fDicCollection)
                foreach  (QuestionEntity entity in fEntities)
                    results.Add(new ResultRecord() { Key = string.Format("{0}-{1}",pair.Key,entity),Value = pair.Value.GetEntity(entity).Rank.ToString()});
            return results;
        }





        public QuestionType GetCurrentQuestionType()
        {
            return fQuestions[fcurrentQuestionIdx];// fCurrentQuestion;
        }

        public QuestionData GetCurrentQuestion()
        {
            return fDicCollection[GetCurrentQuestionType()];
        }
        public bool NextQuestion()
        {
            if (fDicCollection[fQuestions[fcurrentQuestionIdx]].Locked)
            {
                fcurrentQuestionIdx++;
                if (!Ended)
                    return true;
            }
            return false;
        }

        public bool Ended
        {
            get { return fcurrentQuestionIdx >= fQuestions.Length; }
        }
    }
}
