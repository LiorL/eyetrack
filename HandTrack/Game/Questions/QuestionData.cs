﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hend_track.Questions;

namespace hend_track
{
    class QuestionData
    {
        Dictionary<QuestionEntity, QuestionRecord> fDicRecords;

        public QuestionData(QuestionEntity[] aEntities)
        {
            fDicRecords = new Dictionary<QuestionEntity, QuestionRecord>();
            foreach (QuestionEntity entity in aEntities)
                fDicRecords.Add(entity, new QuestionRecord());
        }

        public bool Locked { get {
            foreach (QuestionRecord record in fDicRecords.Values)
                if (!record.locked)
                    return false;

            return true;
        }}

        public QuestionRecord GetEntity(QuestionEntity aEntity)
        {
            return fDicRecords[aEntity];
        }
    }
}
