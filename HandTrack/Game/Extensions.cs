﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Common
{
    public static class Extensions
    {
        public static Vector2 ToXNAVect(this System.Drawing.PointF point)
        {
            return new Microsoft.Xna.Framework.Vector2(point.X, point.Y);
        }

        public static Microsoft.Xna.Framework.Color ToXNAColor(this System.Drawing.Color color)
        {
            return new Microsoft.Xna.Framework.Color(color.R, color.G, color.B);
        }
    }
}
