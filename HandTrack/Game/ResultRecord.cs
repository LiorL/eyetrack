﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace hend_track
{
    class ResultRecord
    {
        
        private string fkey;

        public string Key
        {
            get { return fkey; }
            set { fkey = value; }
        }

        private string fvalue;

        public string Value
        {
            get { return fvalue; }
            set { fvalue = value; }
        }

        private float fScale = 1.0f;

        public float Scale
        {
            get { return fScale; }
            set { fScale = value; }
        }

        private System.Drawing.Color fKeyColor = Color.Black;

        public System.Drawing.Color KeyColor
        {
            get { return fKeyColor; }
            set { fKeyColor = value; }
        }

        private System.Drawing.Color fValueColor = Color.DarkBlue;

        public System.Drawing.Color ValueColor
        {
            get { return fValueColor; }
            set { fValueColor = value; }
        }





        
    }
}
