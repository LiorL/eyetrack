﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hend_track.Logic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SmartButtons;
using Common;
using System.IO;

namespace hend_track.Apps
{
    public class NewApp : GameThreadSafe
    {
        private enum ExperimentState { Unstarted, DriftCorrect,ShowImage, Triggered }
        private int fScreenWidth = Globals.settings.ScreenWidth;
        private int fScreenHeight = Globals.settings.ScreenHeight;
        const string StateInstanceName = "NewApp";
        const string SequenceName = "sequence";
        readonly bool EnableEyeLink = Globals.settings.EnableEyeLink;
        
        Object2d fDriftGaze;
        List<Object2d> fImages;
        Object2d fImageRelaxation;
        SpriteFont fFont;
        SpriteBatch fSpriteBatch;
        GraphicsDeviceManager fGraphics;
        GraphicsDevice fDevice;
        SmartButtons.InputDevice keyboard;
        ExperimentSettings fSettings;
        int fCurrentImage;
        bool fImageVisible;
        
        private SequenceInstance fSequenceInstance;
        protected override void OnExiting(object sender, EventArgs args)
        {
            if (EnableEyeLink)
                EyeLinkManager.Instance.stop_recording(null);
        }
        public NewApp(object aInitObject) : base(aInitObject)
        {
            fCurrentImage = 0;
            fSettings = aInitObject as ExperimentSettings;
            
            keyboard = InputManager.Instance.CreateDevice();
            InputManager.Instance.OnButtonPress += new InputManager.OnButtonPressDelegate(Instance_OnButtonPress);
            
            Content.RootDirectory = "Content";
            fGraphics = new TargetedGraphicsDeviceManager(this, 0);

            InitStates();
            InitSequences();

            
        }

        void Instance_OnButtonPress(SmartButton aButton)
        {
            if (aButton.Device == keyboard)
            {
                if (IsExperimentDone)
                    Exit();
                else
                    if (aButton.DownFirst)
                        fSequenceInstance.Next();
            }
        }

        StateInstance GetStateInstance
        {
            get { return StateManager.Instance.GetOrCreateStateInstance(StateInstanceName); }
        }

        protected override void Initialize()
        {
            fGraphics.PreferredBackBufferWidth = fScreenWidth;
            fGraphics.PreferredBackBufferHeight = fScreenHeight;
            fGraphics.IsFullScreen = Globals.settings.FullScreen;
            try
            {
                fGraphics.ApplyChanges();
            }
            catch (Exception e)
            {
                Exit();
            }
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            fDevice = fGraphics.GraphicsDevice;
            fSpriteBatch = new SpriteBatch(fDevice);
            fFont = Content.Load<SpriteFont>("SpriteFont1");
            fDriftGaze = new Object2d(Content.Load<Texture2D>("redcircle"), new Vector2(20, 20), new Vector2(fScreenWidth / 2, fScreenHeight / 2));
            fDriftGaze.IsVisible = false;
            fImageRelaxation = new Object2d(Content.Load<Texture2D>("imag1"), new Vector2(fScreenWidth, fScreenHeight), new Vector2(fScreenWidth / 2, fScreenHeight / 2));
            //image.IsVisible = false;
            fImageRelaxation.IsVisible = false;
            LoadAllImages();
        }

        public void LoadAllImages()
        {
            fImages = new List<Object2d>();
            string strTextFile = File.ReadAllText(Path.Combine(Globals.PathSequences, "sequence1.txt"));
            string []Images = strTextFile.Split(new string[] { "\r\n"},StringSplitOptions.RemoveEmptyEntries);

            foreach (string strImage in Images)
                fImages.Add(new Object2d(Content.Load<Texture2D>(strImage), new Vector2(fScreenWidth, fScreenHeight), new Vector2(fScreenWidth / 2, fScreenHeight / 2)));

        }

        protected override void BeginRun()
        {
            OnStartEnter(fSequenceInstance);
        }

        public bool IsExperimentDone
        {
            get
            {
                return fCurrentImage >=fImages.Count;
            }

        }

        protected override void Draw(GameTime gameTime)
        {
            fDevice.Clear(Color.Black);
            fSpriteBatch.Begin(SpriteSortMode.Deferred,BlendState.NonPremultiplied);
            fImageRelaxation.Draw(fSpriteBatch);

            fDriftGaze.Draw(fSpriteBatch);

            if (fImageVisible)
                fImages[fCurrentImage].Draw(fSpriteBatch);

            if (DrawWelcomeMessage)
            {
                string msg =  "Press and Key To ";
                msg += IsExperimentDone ? "Exit" : "Start";
                fSpriteBatch.DrawString(fFont, msg, new Vector2(fScreenWidth / 2, fScreenHeight / 2), Color.White);
            }

            if (true)
            {
                string msg = "Experiment State: " + ((ExperimentState)fSequenceInstance.StateIndex).ToString();
                fSpriteBatch.DrawString(fFont, msg, Vector2.Zero, Color.White);
            }

            fSpriteBatch.End();
            
        }

        private void InitStates()
        {
            StateInstance stateInstance = GetStateInstance;
            
            stateInstance.AddStateHandler(ExperimentState.Unstarted.ToString(), OnStartEnter, OnStartLeave, CanStart);
            stateInstance.AddStateHandler(ExperimentState.DriftCorrect.ToString(), OnDriftEnter, OnDriftLeave, CanDrift);
            stateInstance.AddStateHandler(ExperimentState.ShowImage.ToString(), OnShowImageEnter, OnShowImageLeave, null);
            stateInstance.AddStateHandler(ExperimentState.Triggered.ToString(), TriggerEnter, TriggerLeave, IsTriggerEnd);
        }

        
        private int fLockTIme;
        private const int LockDelay = 6000;
        bool DrawWelcomeMessage;


        private void OnDriftLeave(SequenceInstance aInstance)
        {
            fDriftGaze.IsVisible = false;
        }
        private void OnDriftEnter(SequenceInstance aInstance)
        {
            fDriftGaze.IsVisible = true;
            EyeLinkManager.Instance.drift_correct(fScreenWidth, fScreenHeight);

        }
        private bool CanDrift(SequenceInstance aInstance)
        {
            EyeLinkManager.Instance.AcceptTrigger();
            short result = EyeLinkManager.Instance.GetCalibrationResult();
            if (result == EyeLinkManager.OK_RESULT)
            {
                EyeLinkManager.Instance.ApplyDriftCorrect();
                return true;
            }
            return false;
        }
        //private void OnDriftEnter(SequenceInstance aInstance)
        //{
        //}

        //private void CanDrift(SequenceInstance aInstance)
        //{
        //    DrawWelcomeMessage = true;
        //    fImageRelaxation.IsVisible = true;
        //}

  private void OnStartEnter(SequenceInstance aInstance)
        {
            DrawWelcomeMessage = true;
            fImageRelaxation.IsVisible = true;
        }
        private void OnStartLeave(SequenceInstance aInstance)
        {
            DrawWelcomeMessage = false;
            fImageRelaxation.IsVisible = false;
        }

        private void TriggerEnter(SequenceInstance aInstance)
        {
            if (EnableEyeLink)
                EyeLinkManager.Instance.Message("Nistagmus Triggered");
            fLockTIme = Environment.TickCount;
        }
        private void TriggerLeave(SequenceInstance aInstance)
        {
            fImageVisible = false;
            fCurrentImage++;
            
            if (EnableEyeLink)
                EyeLinkManager.Instance.stop_recording("Untitled.edf");
            fLockTIme = 0;
        }
        private bool IsTriggerEnd(SequenceInstance aInstance)
        {
            return Environment.TickCount - fLockTIme > LockDelay;
        }

       
    
        

        private void OnShowImageLeave(SequenceInstance aInstance)
        {
         
        }

        string fCurrentPicture = "";
        private void OnShowImageEnter(SequenceInstance aInstance)
        {
            if (EnableEyeLink)
            {
                EyeLinkManager.Instance.start_recording();
                if (EnableEyeLink)
                {
                    string msg = "Show Picture " + fCurrentPicture;
                    EyeLinkManager.Instance.Message(msg);
                }
            }
            fImageVisible = true;
            
        }

        private bool CanStart(SequenceInstance aInstance)
        {
            return true;
        }

        private void InitSequences()
        {
            StateInstance stateInstance = StateManager.Instance.GetOrCreateStateInstance(StateInstanceName);

            if (EnableEyeLink)
            
            stateInstance.SetSequenceStates(SequenceName,
                ExperimentState.Unstarted.ToString(),
                ExperimentState.DriftCorrect.ToString(),
                ExperimentState.ShowImage.ToString(),
                ExperimentState.Triggered.ToString());

            else

                stateInstance.SetSequenceStates(SequenceName,
                ExperimentState.Unstarted.ToString(),
                ExperimentState.ShowImage.ToString(),
                ExperimentState.Triggered.ToString());

            fSequenceInstance = GetStateInstance.CreateSequenceInstance(SequenceName, null);
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();
            keyboard.HandleInput((int)Keys.Enter, keyState.IsKeyDown(Keys.Enter));

            if (fSequenceInstance.StateIndex == (int)ExperimentState.Triggered)
                if (IsTriggerEnd(fSequenceInstance))
                    fSequenceInstance.Next();

            InputManager.Instance.GetUpdates();
            
            base.Update(gameTime);
        }
    }
}
