﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace hend_track
{
    public class GameThreadSafe : Game
    {
        private static object objLock = new object();
        protected override void Update(GameTime gameTime)
        {
            lock (objLock)
                base.Update(gameTime);
        }

        public GameThreadSafe(object aInitObj) { }
    }
}
