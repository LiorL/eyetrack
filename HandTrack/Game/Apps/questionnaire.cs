﻿using Microsoft.Xna.Framework;
using Common.Messages;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using hend_track;
using System;
public class Questionnaire : GameThreadSafe
{
    private const int fScreenWidth = 800;
    private const int fScreenHeight = 600;
    private int fCurrentTrial = 0;

    public const string ThreadName = "Questionnaire";
    GraphicsDeviceManager fGraphics;
    public enum QuestionnarieDisplayState  {None,Experimenting,ShowRankQuesion};
    
    QuestionnarieDisplayState fState = QuestionnarieDisplayState.Experimenting;
    SpriteBatch fSpriteBatch;
    GraphicsDevice fDevice;
    SpriteFont fFont;
    RankUpdate fLastUpdate;
    string fQuestion;
    int fTotalTrials;

    public Questionnaire(object aInitObj) : base(aInitObj)
    {
        fGraphics = new TargetedGraphicsDeviceManager(this, 1);
        Content.RootDirectory = "Content";
        fGraphics.SynchronizeWithVerticalRetrace = true;
        EventMessages.Instance.RegisterThread(ThreadName);
        fQuestion = "";
        fTotalTrials = (int)aInitObj;

    }

   
    protected override void Initialize()
    {
        fGraphics.PreferredBackBufferWidth = fScreenWidth;
        fGraphics.PreferredBackBufferHeight = fScreenHeight;
        fGraphics.IsFullScreen = false;
        try
        {
            fGraphics.ApplyChanges();
        }
        catch (Exception e)
        {
            MessageBox.Show("Second Monitor is not connected");
            Exit();
        }
        base.Initialize();
    }
    protected override void Draw(GameTime gameTime)
    {
        fGraphics.GraphicsDevice.Clear(Color.Black);
        fSpriteBatch.Begin();
        switch (fState)
        {
            case QuestionnarieDisplayState.None:
                break;
            case QuestionnarieDisplayState.Experimenting:
                DrawExperimenting();
                break;
            case QuestionnarieDisplayState.ShowRankQuesion:
                DrawRank();
                break;
            default:
                break;
        }
        fSpriteBatch.End();
        base.Draw(gameTime);
    }

    private void DrawExperimenting()
    {
        float xMargin = 0.04f;
        float xPadding = 0.3f;
        float ySpacing = 0.1f;
        float yPos = 0.05f;
        Color TextColor = Color.White;
        fSpriteBatch.DrawString(fFont, "Experimenting...", new Vector2(fScreenWidth * xMargin, fScreenHeight* yPos), TextColor,0,Vector2.Zero,3.0f,SpriteEffects.None,0);
        yPos += ySpacing;
        fSpriteBatch.DrawString(fFont, String.Format("Trial {0} of {1}", fCurrentTrial + 1, fTotalTrials), new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), TextColor, 0, Vector2.Zero, 2.0f, SpriteEffects.None, 0);
        yPos += ySpacing;
        
    }

    private void DrawRank()
    {
        float xMargin = 0.04f;
        float xPadding = 0.3f;
        float ySpacing = 0.1f;
        float yPos = 0.05f;
        Color labelColor = Color.GreenYellow;
        
        fSpriteBatch.DrawString(fFont, fQuestion, new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), labelColor);
        yPos += ySpacing;
        if (fLastUpdate != null)
        {
            Color resultColor = fLastUpdate.locked ? Color.Red : Color.White;
            fSpriteBatch.DrawString(fFont, "Rank: ", new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), labelColor);
            fSpriteBatch.DrawString(fFont, fLastUpdate.rank.ToString(), new Vector2(fScreenWidth * xPadding, fScreenHeight * yPos), resultColor);
            yPos += ySpacing;
        }
    }
        

    protected override void LoadContent()
    {
        fDevice = fGraphics.GraphicsDevice;
        fSpriteBatch = new SpriteBatch(fDevice);
        fFont = Content.Load<SpriteFont>("SpriteFont1");
        base.LoadContent();
    }

    protected override void Update(GameTime gameTime)
    {
        List<object> messages = EventMessages.Instance.GetAllMessages();
        if (messages != null)
        {
            foreach (CommonMessage msg in messages)
            {
                switch (msg.type)
                {
                    case MessageType.None:
                        break;
                    case MessageType.DisplayState:
                        fState = (QuestionnarieDisplayState)msg.data; 
                        break;
                    case MessageType.Close:
                        Exit();
                        break;
                    case MessageType.RankUpdate:
                        fLastUpdate = (msg.data as RankUpdate);
                        break;
                    case MessageType.SetTitle:
                        fQuestion = msg.data as string;
                        break;
                    case MessageType.TextField:
                        fCurrentTrial = (short)msg.data;
                        break;
                    default:
                        break;
                }
                
            }

        }
        base.Update(gameTime);
    }
    protected override void OnExiting(object sender, System.EventArgs args)
    {
        EventMessages.Instance.UnRegister();
        base.OnExiting(sender, args);
    }
}