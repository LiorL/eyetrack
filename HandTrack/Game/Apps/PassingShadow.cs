﻿#region File Description
//-----------------------------------------------------------------------------
// Game1.cs
//
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using hend_track;
using System.Threading;
using System.IO;
using Serial;
using Common;
using System.Diagnostics;
using SmartButtons;
using Common.Messages;
using System.Collections;
using SlimDX.RawInput;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Security.Permissions;
using System.Security;
using hend_track.Questions;
using LiorGraphics;
using hend_track.Logic;
using System.Text;


namespace hend_track
{
    
    public class PassingShadow : GameThreadSafe
    {

        private enum ExperimentState { Unstarted, Running, ShowQuestions, Stopped, DriftCorrect, WaitingForUserInput }
        private enum Sequence { Normal, NoDrift }
        private enum ActionID { NONE, Progress, ForceEndTrial, ForceExit }
        private enum TrainingState { None, Failure, Success }

        public const string ThreadName = "MainThread";
        private const bool UseRawInput = true;
        private  float initialMoney = Globals.settings.InitialMoney;
        private int TrainingMaxTrials = Globals.settings.TrainingCount;
        private float TargetTrainingScore = Globals.settings.TrainingScore;
        
        //Graphics
        GraphicsDeviceManager fGraphics;
        SpriteBatch fSpriteBatch;
        SpriteFont fFont;
        GraphicsDevice fDevice;
        Objects2d fObjects2d;
        bool enqueueFullScreenToggle;
        //bool questionnaireLoaded;
        Questionnaire questionnaire;

        //Coordinates
        private  double Screenratio = 2.0 / 3.0;
        Vector2 fUpperCenter;
        Vector2 fLowerCenter;
        Vector2 fScreenCenter;
        int red_circle_width;
        int red_circle_hight;
        int blue_rectangle_width;
        int blue_rectangle_hight;
        int red_rectangle_width;
        int red_rectangle_hight;

        //Experiment Logic
        ExperimentSettings fSettings;
        ExperimentState fExperimentState = ExperimentState.Unstarted;
        SequenceInstance fSeqeunceInstance;

        //Trial Logic
        TrialData fCurrentTrialData;
        movement_model _movement_model_for_redcircle;//class for move the shapes
        movement_model _movement_model_for_redrectangle;
        int fScreenWidth;
        int fScreenHeight;
        System.Drawing.Rectangle fUpperrectangle;
        System.Drawing.Rectangle fLowerrectangle;
        bool centerRedRectangle;
        short fCurrentTrial = 0;
        short fCurrentTrialRetry = 0;
        Timer fLandoltTimer;
        Random fRandom;
        TrainingState fTrainingState;
        bool fDriftCorrectionDone;
        Thread fQuestionnaireThred;

        //Concurrect Experiment  Data
        int fTotalUpdates;
        int fCountSightInTarget;
        int fIsSightOnTarget;
        int fISRedRectangleInsideMecryRect;
        int fCountRedRectInsideMercyZone;
        int fTargetTravelDistance;
        int fCrosshairTravelDistance;
        int fFalseAlarms;
        int fTotalClicks;
        
        double fPercentOnTarget;
        double fPercentOnMercyZone;
        double fperformanceOnTrial;
        double fBonusPoints;

        //User Input
        private Dictionary<IntPtr, InputDevice> dicAlias = new Dictionary<IntPtr,  InputDevice>();
        InputDevice fKeyboardInput;
        InputDevice fSecondKeyboardInput;
        InputDevice fJoystickInput;
        EyeLinkManager fEyelink;
        int fTrialStartTick;
        SlimDX.DirectInput.Joystick fJoystick;
        SlimDX.DirectInput.DirectInput fDirectInput;
        InputManager fInputManager;
        AtlasSerialPoller fSerial;
        float[] fRegions; // joystick regions

        //Data
        int fTimeLastSerialMsg;

        //Results
        List<ResultRecord>  fAnswers;
        List<ResultRecord>  fPerformace;
        DisplayResults      fUserResults;
        List<ResultRecord>  fDisplayResults;
        QuestionCollection  fQuestionCollection;
        StringBuilder       fCSVSB;
        int                 fCSVMessageID;

        //debug
        FrameMonitor    frameMonitor;
        FrameMonitor    UpdateMonitor;
        bool            fDebugInfoEnabled;
        Object2d        fLeftEye;
        Object2d        fRightEye;


        protected override void OnExiting(object sender, EventArgs args)
        {
            DisposeAll();
            base.OnExiting(sender, args);
        }
        

        private float Slew(float value)
        {
            
            float result = 0;
            float valueABS = Math.Abs(value);
            if (valueABS < Globals.settings.R1x)
            {
                result = fRegions[0] * valueABS - fRegions[0] * Globals.settings.R1x + Globals.settings.R1y;
            }
            else if (valueABS < Globals.settings.R2x)
            {
                result = fRegions[1] * valueABS - fRegions[1] * Globals.settings.R2x + Globals.settings.R2y;
            }
            else
            {
                result = fRegions[2] * valueABS - fRegions[2] * 1.0f + 1.0f;
            }
            if (value < 0)
                result = -result;

            return result;
        }

        
        private void DisposeAll()
        {
            if (fJoystick != null)
                fJoystick.Unacquire();

            if (fSerial != null)
            {
                fSerial.Close();
            }

            if (fEyelink != null)
            {
                fEyelink.stop_recording("");
                fEyelink.close();
            }
            if (UseRawInput)
                SlimDX.RawInput.Device.KeyboardInput -= new EventHandler<SlimDX.RawInput.KeyboardInputEventArgs>(Device_KeyboardInput);

            if (fQuestionnaireThred != null)
            {
                EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.Close, null));
                fQuestionnaireThred.Join();
            }

            EventMessages.Instance.UnRegister();
        }

        private float GetDistance(float A, float B)
        {
            return (A - Math.Abs(A - B)) / A;
        }

        private bool IsQualifed()
        {

            return fUserResults.pathRatio >= TargetTrainingScore
                   && fUserResults.falseAlarms >= TargetTrainingScore
                   && fUserResults.insideMercyZone >= TargetTrainingScore
                   && fUserResults.insidetarget >= TargetTrainingScore;
        }


        public PassingShadow(object aInitObject) : base (aInitObject)
        {
            
            fSettings = aInitObject as ExperimentSettings;
            frameMonitor = new FrameMonitor(30);
            fCSVSB = new StringBuilder();
            fCSVMessageID = 1;
            UpdateMonitor = new FrameMonitor(30);
            InitSequences();
            InitStates();
            string currentSequence =  (fSettings.EyeLink ? Sequence.Normal : Sequence.NoDrift).ToString();
                        

            fBonusPoints = initialMoney;
            ShowResultsScreen();
            EventMessages.Instance.RegisterThread(ThreadName);
            fRegions = new float[3];
            fRegions[0] = Globals.settings.R1y / Globals.settings.R1x;
            fRegions[1] = (Globals.settings.R2y - Globals.settings.R1y) / (Globals.settings.R2x - Globals.settings.R1x);
            fRegions[2] = (1 - Globals.settings.R2y) / (1 - Globals.settings.R2x);
            
            fObjects2d = new Objects2d();

            if (fSettings.Physical)
            {
                fSerial = new AtlasSerialPoller(Globals.settings.SensorsComPort, 115200);
                try { fSerial.Open(); }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("בדוק אם המדדים מחוברים");
                    this.Exit();
                }
            }

             if (fSettings.EyeLink)
             {
                 fEyelink = EyeLinkManager.Instance;
             }
            
            fGraphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            fGraphics.SynchronizeWithVerticalRetrace = true;
            

            fInputManager = InputManager.Instance;
            
            fInputManager.OnButtonPress += new InputManager.OnButtonPressDelegate(OnUserInput);
            fJoystickInput = fInputManager.CreateDevice();
            fRandom = new Random();
        }

        private void InitStates()
        {
            //StateManager manager = StateManager.Instance;
            //manager.AddStateHandler(epx
        }

        private void InitSequences()
        {
            //StateManager manager = StateManager.Instance;

            //manager.SetSequenceStates(Sequence.Normal.ToString(), 
            //    ExperimentState.Unstarted.ToString() ,
            //    ExperimentState.Running.ToString(),
            //    ExperimentState.ShowQuestions.ToString(),
            //    ExperimentState.Stopped.ToString(),
            //    ExperimentState.DriftCorrect.ToString(),
            //    ExperimentState.WaitingForUserInput.ToString());


            //manager.CopySequenceStates(Sequence.Normal.ToString(), Sequence.NoDrift.ToString());
            //manager.DeleteStateForSequence(Sequence.NoDrift.ToString(), ExperimentState.DriftCorrect.ToString());
        }

        protected override void Initialize()
        {
            fGraphics.PreferredBackBufferWidth = Globals.settings.ScreenWidth;//pixel screen width
            fGraphics.PreferredBackBufferHeight = Globals.settings.ScreenHeight;//pixel screen hight
            enqueueFullScreenToggle = Globals.settings.FullScreen;
            fGraphics.ApplyChanges();
            base.Initialize();
        }

        private void ResetObjectsPosition()
        {
            fObjects2d["background"].Position =  fScreenCenter;
            fObjects2d["redcircle"].Position = fUpperCenter;
            fObjects2d["redcircle"].IsVisible = true;
            fObjects2d["bluerectangle"].Position = fLowerCenter;
            fObjects2d["redrectangle" ].Position = fLowerCenter;
            fObjects2d["crosshair"].Position = fUpperCenter;
            fObjects2d["mercyrectangle"].Position =fLowerCenter;
            fObjects2d["oldcrosshair"].Position =fObjects2d["crosshair"].Position;
            fObjects2d["oldredcircle"].Position =fObjects2d["redcircle"].Position;

            if (fSettings.Layout == ExperimentLayOut.Modern)
                fObjects2d["clouds"].Position = fScreenCenter;
        }

        private void SetObjectsSize()
        {
            fObjects2d["background"].Size = new Vector2(fScreenWidth, fScreenHeight);
            fObjects2d["redcircle"].Size= new Vector2(red_circle_width, red_circle_hight);
            fObjects2d["bluerectangle"].Size= new Vector2(blue_rectangle_width, blue_rectangle_hight);
            fObjects2d["redrectangle"].Size= new Vector2(red_rectangle_width, red_rectangle_hight);
            fObjects2d["crosshair"].Size= new Vector2(red_circle_width, red_circle_hight);
            fObjects2d["mercyrectangle"].Size= new Vector2(blue_rectangle_width * 1.1f, blue_rectangle_hight * 1.1f);
            fObjects2d["oldcrosshair"].Size= fObjects2d["crosshair"].Size;
            fObjects2d["oldredcircle"].Size = fObjects2d["redcircle"].Size;

            if (fSettings.Layout == ExperimentLayOut.Modern)
                fObjects2d["clouds"].Size = new Vector2(fScreenWidth, fScreenHeight);
        }

        protected override void LoadContent()
        {
            fDevice = fGraphics.GraphicsDevice;
            fSpriteBatch = new SpriteBatch(fDevice);
            fFont = Content.Load<SpriteFont>("SpriteFont1");

            switch (fSettings.Layout)
            {
                case ExperimentLayOut.NONE:
                    break;
                case ExperimentLayOut.Classic:
                    fObjects2d.Add("background", new Object2d(Content.Load<Texture2D>("background")));
                    fObjects2d.Add("redcircle", new Object2d(Content.Load<Texture2D>("redcircle")));
                    break;
                case ExperimentLayOut.Modern:
                    fObjects2d.Add("background", new Object2d(Content.Load<Texture2D>("SkyWide")));
                    fObjects2d.Add("redcircle", new Object2d(Content.Load<Texture2D>("helli")));
                    fObjects2d.Add("clouds", new Object2d(Content.Load<Texture2D>("clouds")));
                    break;
                case ExperimentLayOut.LendoltC:
                    {
                        Screenratio = 1.0;
                        //fLandoltTimer = new Timer(new TimerCallback(OnLandoltTimer));
                        fObjects2d.Add("background", new Object2d(Content.Load<Texture2D>("background")));
                        Object2d target = new Object2d(Content.Load<Texture2D>("redcircledot"));
                        //target.TotalFrames = 8;
                        target.BlendColor = Color.White;
                        fObjects2d.Add("redcircle", target);
                    }
                    break;
                default:
                    break;
            }
            fObjects2d.Add("bluerectangle", new Object2d(Content.Load<Texture2D>("bluerectangle")));
            fObjects2d.Add("redrectangle", new Object2d(Content.Load<Texture2D>("redrectangle")));
            fObjects2d.Add("mercyrectangle", new Object2d(null));

            if (fSettings.Layout == ExperimentLayOut.LendoltC)
            {
                fObjects2d["bluerectangle"].IsVisible = false;
                fObjects2d["redrectangle"].IsVisible = false;
                fObjects2d["background"].IsVisible = false;
            }


            fObjects2d["mercyrectangle"].IsVisible = false;
            fObjects2d.Add("oldcrosshair", new Object2d(null));
            fObjects2d.Add("oldredcircle", new Object2d(null));
            fObjects2d.Add("crosshair", new Object2d(Content.Load<Texture2D>("crosshair")));
            fObjects2d["crosshair"].BlendColor = Color.Teal;

            //Drift correction target
            fObjects2d.Add("dctarget", new Object2d(Content.Load<Texture2D>("redcircle")));

            fLeftEye = new Object2d(Content.Load<Texture2D>("redcircle"));

            fLeftEye = new Object2d(Content.Load<Texture2D>("eye"), new Vector2(50, 50), new Vector2(100, 100));

            fLeftEye.BlendColor = new Color(75, 252, 84, 127); ;
                
            fRightEye = new Object2d(Content.Load<Texture2D>("eye"), new Vector2(50, 50), new Vector2(150, 100));
            fRightEye.BlendColor = new Color(255, 72, 132, 127);
            
            SetNextTrial();

            centerRedRectangle = false;
            try
            {
                
                fDirectInput = new SlimDX.DirectInput.DirectInput();
                IList<SlimDX.DirectInput.DeviceInstance> gameControllerList = fDirectInput.GetDevices(SlimDX.DirectInput.DeviceClass.GameController, SlimDX.DirectInput.DeviceEnumerationFlags.AttachedOnly);
                IOrderedEnumerable<SlimDX.DirectInput.DeviceInstance> gameControllerListOrdered = gameControllerList.OrderBy<SlimDX.DirectInput.DeviceInstance, SlimDX.DirectInput.DeviceType>(x => x.Type);
                 
                IList<SlimDX.DirectInput.DeviceInstance> JoySticks = new List<SlimDX.DirectInput.DeviceInstance>();

                foreach (SlimDX.DirectInput.DeviceInstance device in gameControllerListOrdered)
                    if (device.Type == SlimDX.DirectInput.DeviceType.Joystick || device.Type == SlimDX.DirectInput.DeviceType.FirstPerson)
                        JoySticks.Add(device);

                if (JoySticks.Count > 0)
                {
                    fJoystick = new SlimDX.DirectInput.Joystick(fDirectInput, JoySticks[0].InstanceGuid);
                    fJoystick.Acquire();
                }
                else
                {
                    //.break.System.Windows.Forms.MessageBox.Show("בדוק אם הג'ויסטיק מחובר");
                    //.break.break.Exit();
                }
                //fJoystick = new JoystickInterface.Joystick(fDevice.PresentationParameters.DeviceWindowHandle);
                //fJoystick.AcquireJoystick(fJoystick.FindJoysticks()[0]);
            }
            catch
            {
                //System.Windows.Forms.MessageBox.Show("בדוק אם הג'ויסטיק מחובר");
                //Exit();
            }

            
            //lastKnownWindowhandle = this.Window.Handle;
            if (UseRawInput)
            {
                SlimDX.RawInput.Device.RegisterDevice(SlimDX.Multimedia.UsagePage.Generic, SlimDX.Multimedia.UsageId.Keyboard, DeviceFlags.None, this.Window.Handle, true);
                SlimDX.RawInput.Device.KeyboardInput += new EventHandler<SlimDX.RawInput.KeyboardInputEventArgs>(Device_KeyboardInput);
                ICollection<SlimDX.RawInput.DeviceInfo> devices = SlimDX.RawInput.Device.GetDevices();
                int idx = 0 ;
                InputDevice kbddevice;
                foreach (DeviceInfo device in devices)
                       if (device.DeviceType == DeviceType.Keyboard
                        &&( ( device as KeyboardInfo).DeviceName.IndexOf("ACPI")  != -1
                        || (device as KeyboardInfo).DeviceName.IndexOf("HID")  != -1))
                    {
                        kbddevice = fInputManager.CreateDevice();
                        kbddevice.SetDeviceOptions(InputDeviceOptions.AlwaysSendEvents);
                       if (idx ==  Globals.settings.KeyBoardPrim)
                           fKeyboardInput = kbddevice;
                       else if (idx == Globals.settings.KeyBoardSec)
                           fSecondKeyboardInput = kbddevice;
                        
                       idx++;
                       dicAlias.Add(device.Handle, kbddevice);
                    }
            }
        }


        private void OnLandoltTimer(object aObj)
        {
            Object2d landolt = fObjects2d["redcircle"];
            int chooseFrame = (int)(fRandom.NextDouble() * 8);
            landolt.CurrentFrame = chooseFrame;
            SetLandoltChange();
        }
        
        void Device_KeyboardInput(object sender, SlimDX.RawInput.KeyboardInputEventArgs e)
        {
            InputDevice device = dicAlias[e.Device];
            device.HandleInput((int)e.Key, e.State == SlimDX.RawInput.KeyState.Pressed);
        }

        protected override void Update(GameTime gameTime)
        {
            UpdateMonitor.StartFrame();
            MyFixedTimeUpdate(gameTime);            
            base.Update(gameTime);
            UpdateMonitor.EndFrame();
         }


        private void MyFixedTimeUpdate(GameTime gameTime)
        {
            if (enqueueFullScreenToggle)
            {
                fGraphics.ToggleFullScreen();
                enqueueFullScreenToggle = false;
            }

            UpdateUserInput(gameTime);

            if (fExperimentState == ExperimentState.Running)
            {
                int elapsed = Environment.TickCount - fTrialStartTick;
                UpdateTarget(elapsed);
                UpdateRedrectangle();
                UpdateData();

                if (!fSettings.Demo && elapsed >= Globals.settings.MiliSecondsToEndExperiment)
                    ProcessAction(ActionID.ForceEndTrial);
            }
        }
        

        private void UpdateUserInput(GameTime gameTime)
        {
            if (fJoystick != null)
                processJoystick(gameTime);

            fInputManager.GetUpdates();
        }

        private void UpdateTarget(int aElapsed)
        {
            point p = _movement_model_for_redcircle.next();//new point to move
            Object2d target = fObjects2d["redcircle"];
            target.Position = new Vector2((float)p.x, (float)p.y);
            if (fCurrentTrialData._type == 3 || fCurrentTrialData._type == 4)
            {
                _movement_model_for_redcircle.updateInv(aElapsed / 1000);
                target.IsVisible = _movement_model_for_redcircle.isVisible();
            }

            //Debug.WriteLine("X: " + p.x + "Y: " + p.y + " Visible " + _movement_model_for_redcircle.isVisible());
                
        }


        private void SetNextTrial()
        {
            
            if (fExperimentState != ExperimentState.Unstarted)
            {
                if (!fSettings.IsTraining)
                        fCurrentTrial++;
                else
                {
                    if (IsQualifed())
                        fCurrentTrial++;
                    fCurrentTrialRetry++;
                }
            }

            bool endTrials = fCurrentTrial >= fSettings.Trials.Count;
            
            if (!fSettings.IsTraining)
            {
                if (endTrials)
                    Exit();
            }
            
            if (!endTrials)
            {
                SetTrialData(fSettings.Trials[fCurrentTrial]);
                ResetObjectsPosition();

                if (fExperimentState == ExperimentState.Unstarted)
                    SetObjectsSize();

                fExperimentState = ExperimentState.WaitingForUserInput;
            }
        }

        private void SetResult()
        {
            fPercentOnTarget = (double)fCountSightInTarget / fTotalUpdates * 100;
            fPercentOnMercyZone = (double)fCountRedRectInsideMercyZone / fTotalUpdates * 100;

            fperformanceOnTrial = 0.05 * (fTotalClicks <= 0 ? 1.0 : ((double)(fTotalClicks - fFalseAlarms) / fTotalClicks) ) + 
                                  0.05 * (double)(fPercentOnMercyZone / 100) + 
                                  0.9  * (double)(fPercentOnTarget / 100);
            fBonusPoints -= (1.0 - fperformanceOnTrial) / 6.0;
            fAnswers = fQuestionCollection.FormatResults();
            fPerformace = new List<ResultRecord>();
            //fEyelink.Message(String.Format(",{0},{1},{2},{3},{4},{5},{6},{7}","PathOfTarget", "PathOfSight", "PercentOnTarget", "PercentMercyZone", "FalseAlarms", "TotalClicks","MyRank","SupervisorRank"));    
            fPerformace.Add(new ResultRecord() {Key = "PathOfTarget",Value = fTargetTravelDistance.ToString()});
            fPerformace.Add(new ResultRecord() {Key = "PathOfSight",Value = fCrosshairTravelDistance.ToString()});
            fPerformace.Add(new ResultRecord() {Key = "PercentOnTarget",Value  = fPercentOnTarget.ToString()});
            fPerformace.Add(new ResultRecord() {Key = "PercentMercyZone",Value = fPercentOnMercyZone.ToString()});
            fPerformace.Add(new ResultRecord() {Key = "FalseAlarms",Value = fFalseAlarms.ToString()});
            fPerformace.Add(new ResultRecord() {Key = "TotalClicks",Value = fTotalClicks.ToString()});

            fPerformace.Add(new ResultRecord() { Key = "PerformanceOnTrial", Value = fperformanceOnTrial.ToString("F2") });
            fPerformace.Add(new ResultRecord() { Key = "BonusPoints", Value = fBonusPoints.ToString("F2") });

            fUserResults.falseAlarms = (fTotalClicks > 0  ? 1.0f- (float)fFalseAlarms / fTotalClicks : 1.0f);
            fUserResults.pathRatio = GetDistance(fTargetTravelDistance, fCrosshairTravelDistance);
            fUserResults.insideMercyZone = (float)fCountRedRectInsideMercyZone / fTotalUpdates;
            fUserResults.insidetarget = (float)fCountSightInTarget / fTotalUpdates;
            
            fDisplayResults = new List<ResultRecord>();

            fDisplayResults.Add(new ResultRecord() { Key = "Primary Scores", Value = "", Scale = 1.8f,KeyColor = System.Drawing.Color.Orange});
            fDisplayResults.Add(new ResultRecord() { Key = "On Target", Value = NumberToString(fUserResults.insidetarget) , Scale = 1.2f });
            fDisplayResults.Add(new ResultRecord() { Key = "Path Ratio", Value = NumberToString(fUserResults.pathRatio), Scale = 1.2f });

            fDisplayResults.Add(new ResultRecord() { Key = "Secondary Scores", Value = "", Scale = 1.2f, KeyColor = System.Drawing.Color.Orange });
            fDisplayResults.Add(new ResultRecord() { Key = "Inside", Value = NumberToString(fUserResults.insideMercyZone), Scale = 0.8f });
            fDisplayResults.Add(new ResultRecord() { Key = "Correct Clicks", Value = NumberToString(fUserResults.falseAlarms), Scale = 0.8f  });
        }

        private void EndTrial()
        {
            fExperimentState = ExperimentState.Stopped;
            SetResult();
            StopLandolt();

            string fileName = fCurrentTrialData._nameOftest + "_" + DateTime.Now.ToString("dd-M-yy_HH-mm-ss");
            string AbsoluteFolder = Path.Combine(Globals.PathResults, fSettings.UserProfile.ID, fSettings.PhaseName);
            string AbsoluteFileName = Path.Combine(AbsoluteFolder, fileName);
            if (!Directory.Exists(AbsoluteFolder))
                Directory.CreateDirectory(AbsoluteFolder);

            string header;
            string data;
            List<ResultRecord> jointResults = new List<ResultRecord>();
            jointResults.AddRange(fPerformace);
            jointResults.AddRange(fAnswers);
            FormatDataResults(jointResults, out header, out data);

            File.WriteAllText(Path.ChangeExtension(AbsoluteFileName, ".csv"),header + "\r\n" + data);

            ProcessMessage(header);
            ProcessMessage(data);
            

            if (fSettings.EyeLink)
            {
                fEyelink.stop_recording(Path.ChangeExtension( AbsoluteFileName,".edf"));
            }
            else
            {
                File.WriteAllText(Path.ChangeExtension(AbsoluteFileName, ".asc"),  fCSVSB.ToString() );
            }

            fInputManager.Flush();
            if (fSettings.IsTraining && !Globals.settings.DebugMode)
            {
                bool qualifed = IsQualifed();
                bool endAll = fCurrentTrialRetry >= TrainingMaxTrials - 1;
                bool finalTrial = fCurrentTrial >= fSettings.Trials.Count - 1;
                bool pass = finalTrial && qualifed;

                if (pass)
                    fTrainingState = TrainingState.Success;

                if (endAll && !pass)
                    fTrainingState = TrainingState.Failure;
            }

            //if not training bypass score stage
            //if (!fSettings.IsTraining)
            //    ProcessAction(ActionID.Progress);
            
            //    fExperimentState = ExperimentState.WaitingForUserInput;

        }

        /*
        private string FindSutiablFolder(string aDesiredFolder)
        {
            string BaseName = Path.Combine(Globals.PathResults,aDesiredFolder);
            string foundDirectory;
            int currentIdx = 0;
            while (Directory.Exists(foundDirectory =  BaseName + "_" + currentIdx.ToString()))
                currentIdx++;

            Directory.CreateDirectory(foundDirectory);
            return foundDirectory;
        }*/

        private void FormatDataResults(List<ResultRecord> aRecords,out string aHeader,out string aData)
        {
            aHeader = aData = "";
            foreach (ResultRecord record in aRecords)
            {
                aHeader += "," + record.Key;
                aData += "," + record.Value;
            }

        }

        private void SetLandoltChange()
        {
            if (fLandoltTimer != null)
            {
                int dueLandoltChange = (int)(fRandom.NextDouble() * 1000 + 500);
                fLandoltTimer.Change(dueLandoltChange,System.Threading.Timeout.Infinite);
            }
        }

        private void StopLandolt()
        {
            if (fLandoltTimer != null)
                fLandoltTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
        }

        private void StartTrial()
        {
            fExperimentState = ExperimentState.Running;
            fTrialStartTick = Environment.TickCount;
            fCSVSB.Clear();
            fCSVMessageID = 1;

            SetLandoltChange();
            if (fSettings.EyeLink)
            {
                fEyelink.start_recording();
            }

            ProcessMessage("start experiment");
            ProcessMessage(String.Format(",{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", "TargetX", "TargetY", "CrossHairX", "CrossHairY", "SightOnTarget", "RectInMercy", "BM-FSR", "BM-GSR", "BM-PPG", "BM-ASR", "TargetVisible"));
            
        }

        private void HideDcTarget()
        {
            Object2d drifttarget = fObjects2d["dctarget"];
            if (drifttarget != null)
            {
                drifttarget.IsVisible = false;
            }
            
        }

    

        private void processJoystick(GameTime gametime)
        {
            SlimDX.DirectInput.JoystickState joyState = fJoystick.GetCurrentState();
            bool[] Buttons = joyState.GetButtons();

            fJoystickInput.HandleInput(2, Buttons[2]);
            fJoystickInput.HandleInput(1, Buttons[1]);
            fJoystickInput.HandleInput(0, Buttons[0]);

            if (fExperimentState == ExperimentState.Running)
            {
                if (fSettings.UseEyeLinkAsController)
                {
                    System.Drawing.PointF point = fEyelink.GetEye(fSettings.Eye);
                    Vector2 vec = new Vector2(point.X, point.Y);
                    fObjects2d["crosshair"].Position = vec;
                    if (fDebugInfoEnabled)
                    {
                        fLeftEye.Position =  fEyelink.GetEye(EyeConfig.Left).ToXNAVect();
                        fRightEye.Position = fEyelink.GetEye(EyeConfig.Right).ToXNAVect();
                    }

                }
                {
                    float speedx = (joyState.X - 32768) / 65536.0f * 2;
                    float speedy = (joyState.Y - 32768) / 65536.0f * 2;


                    if (Math.Abs(speedx) < Globals.settings.JoystickDeadZone)
                        speedx = 0;

                    if (Math.Abs(speedy) < Globals.settings.JoystickDeadZone)
                        speedy = 0;

                    speedx = Slew(speedx);
                    speedy = Slew(speedy);


                    float joysticktopixelsFactor = Globals.settings.JoystickSpeedMultiplier * (float)gametime.ElapsedGameTime.TotalMilliseconds;

                    float pixelsX = joysticktopixelsFactor * speedx;
                    float pixelsY = joysticktopixelsFactor * speedy;

                    if (Math.Abs(pixelsX) < Globals.settings.JoystickMinSpeed)
                        pixelsX = (float)(Globals.settings.JoystickMinSpeed * Math.Sign(pixelsX));

                    if (Math.Abs(pixelsY) < Globals.settings.JoystickMinSpeed)
                        pixelsY = (float)(Globals.settings.JoystickMinSpeed * Math.Sign(pixelsY));

                    MoveCrossHair(pixelsX, pixelsY);


                }
            }
        }

        private void MoveCrossHair(float pixelsX, float pixelsY)
        {
            Vector2 vec = fObjects2d["crosshair"].Position;
            vec += new Vector2(pixelsX, pixelsY);

            if (vec.X < 0)
                vec.X = 0;

            if (vec.Y < 0)
                vec.Y = 0;

            if (vec.Y > fScreenHeight * Screenratio)
                vec.Y = (float)(fScreenHeight * Screenratio);

            if (vec.X > fScreenWidth)
                vec.X = fScreenWidth;

            fObjects2d["crosshair"].Position = vec;
        }

        private void ProcessAction(ActionID aAction)
        {
            switch (aAction)
            {
                case ActionID.NONE:
                    break;
                case ActionID.Progress:
                    switch (fExperimentState)
                    {
                        case ExperimentState.Running:
                            centerRedRectangle = true;
                            break;
                        case ExperimentState.Stopped:
                            if (fTrainingState != TrainingState.None)
                                ProcessAction(ActionID.ForceExit);
                            else
                            {
                                SetNextTrial();
                                if (fSettings.EyeLink)
                                {
                                    fDriftCorrectionDone = false;
                                    fExperimentState = ExperimentState.DriftCorrect;
                                    fEyelink.drift_correct(fScreenWidth, fScreenHeight);
                                }
                            }
                            
                            break;

                        case ExperimentState.DriftCorrect:
                            if (fDriftCorrectionDone && fCurrentTrial <= fSettings.Trials.Count)
                                fExperimentState = ExperimentState.WaitingForUserInput;
                            

                            break;

                        case ExperimentState.WaitingForUserInput:
                            UpdateQuestionaire();
                            if (fCurrentTrial <= fSettings.Trials.Count)
                                StartTrial();
                            break;
                        

                        case ExperimentState.ShowQuestions:
                            
                           if (fQuestionCollection.Ended)
                            {
                                if (fCurrentTrial <= fSettings.Trials.Count)
                                    EndTrial();

                            }
                            break;

                           default:
                            break;
                    }
                    break;
                case ActionID.ForceEndTrial:
                    if (!fQuestionCollection.Ended)
                    {
                        fExperimentState = ExperimentState.ShowQuestions;

                        if (Globals.MultipleMonitors)
                        {
                            EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.SetTitle, GetCurrentQuestion(fQuestionCollection.GetCurrentQuestionType(), QuestionEntity.Tester)));
                            EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.DisplayState, Questionnaire.QuestionnarieDisplayState.ShowRankQuesion));
                            
                        }
                    }
                    else 
                    {
                        if (fCurrentTrial <= fSettings.Trials.Count)
                            EndTrial();
                    }

                        break;
                    
                case ActionID.ForceExit:
                    Exit();
                    break;
                default:
                    break;
            }
        }

        private void UpdateQuestionaire()
        {
            if (Globals.MultipleMonitors)
            {
                EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.DisplayState, Questionnaire.QuestionnarieDisplayState.Experimenting));
                if (fSettings.IsTraining)
                    EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.TextField, (short)(fCurrentTrialRetry)));
                else
                    EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.TextField, (short)(fCurrentTrial)));
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            frameMonitor.StartFrame();
            //Debug.WriteLine(myRanklocked);
            Color bgColor;
                bool DrawObjects = false;
                bool drawResults = false;
                bool drawMoneyLeft = false;
                bool drawTitle = false;
                bool drawQuestions = false;
                bool drawTrainingState = false;
                bool drawDriftCorrect = false;

                switch (fExperimentState)
                {
                    case ExperimentState.WaitingForUserInput:
                        DrawObjects = true;
                        drawTitle = true;
                        break;
                    case ExperimentState.Unstarted:
                        drawTitle = true;
                        break;
                    case ExperimentState.ShowQuestions:
                       drawQuestions = true;
                       break;

                    case ExperimentState.DriftCorrect:
                       drawDriftCorrect = true;
                       break;
                    case ExperimentState.Running:
                        DrawObjects = true;
                        break;
                    case ExperimentState.Stopped:
                        if (fTrainingState != TrainingState.None)
                        {
                            drawTrainingState = true;
                        }
                        else
                        {
                            if (fSettings.IsTraining)
                                drawResults = true;
                            else
                                drawMoneyLeft = true;
                        }
                        
                        break;
                    default:
                        break;
                }

                bgColor = Color.Gray;
                
            //if (drawResults || drawQuestions || drawMoneyLeft || drawDriftCorrect)
            //        bgColor = Color.Gray;
            //    else
            //        bgColor = Color.White;
                
            if (fTrainingState != TrainingState.None)
                bgColor = (fTrainingState == TrainingState.Success ?  Color.Green : Color.Red);

            fGraphics.GraphicsDevice.Clear(bgColor);

            fSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

                if (drawTrainingState)
                    DrawTrainingState();
    
                if (drawResults)
                    DrawResults();

                if (drawMoneyLeft)
                    DrawMoneyLeft();

                if (DrawObjects)
                    fObjects2d.Draw(fSpriteBatch);

                if (drawDriftCorrect)
                {
                    Object2d drifttarget = fObjects2d["dctarget"];
                    if (drifttarget != null)
                    {
                        drifttarget.IsVisible = true;
                        drifttarget.Position = new Vector2(fScreenWidth / 2, fScreenHeight / 2);
                        drifttarget.Size = new Vector2(20,20);
                        drifttarget.Draw(fSpriteBatch);
                    }
                }

                if (drawTitle)
                    fSpriteBatch.DrawString(fFont, String.Format("Press fire to start {0} trial", fCurrentTrialData._nameOftest), new Vector2(fScreenCenter.X / 4, fScreenCenter.Y / 8), Color.Black);



                if (fDebugInfoEnabled)
                {
                    fSpriteBatch.DrawString(fFont, String.Format("FPS {0}, UPS {1}", frameMonitor.GetFPS(), UpdateMonitor.GetFPS()), new Vector2(20, 20), Color.Black, 0.0f, Vector2.Zero, 0.5f, SpriteEffects.None, 1.0f);
                    fLeftEye.Draw(fSpriteBatch);
                    fRightEye.Draw(fSpriteBatch);
                }

                //fSpriteBatch.DrawString(fFont, fCurrentTrialData._nameOftest, new Vector2(20, fScreenHeight - 30), Color.Black, 0.0f, Vector2.Zero, 0.8f, SpriteEffects.None, 1.0f);
                

                if (drawQuestions)
                    DrawRank();

                fSpriteBatch.End();
                base.Draw(gameTime);
                frameMonitor.EndFrame();
        }

        private void DrawTrainingState()
        {
            string msg =  fTrainingState == TrainingState.Failure ?
                "Unfortunately you did not reach the required level" :
                "You may proeced to the experiment";
            
            fSpriteBatch.DrawString(fFont, msg, new Vector2(fScreenCenter.X / 4, fScreenCenter.Y / 8), Color.Black);
            
        }

        private void DrawMoneyLeft()
        {
            float xMargin = 0.04f;
            float xPadding = 0.3f;
            float ySpacing = 0.1f;
            float yPos = 0.05f;

            List<ResultRecord> records = new List<ResultRecord>();

            records.Add(new ResultRecord() { Key = "Performance", Value = "", Scale = 1.9f,KeyColor = System.Drawing.Color.Orange });
            records.Add(new ResultRecord() { Key = "Performance on this Trial: ", Value = (fperformanceOnTrial * 100).ToString("F2") + "%", Scale = 1.2f});
            records.Add(new ResultRecord() { Key = "Bonus Points for this session:", Value = fBonusPoints.ToString("F2"), Scale = 1.2f });

            foreach (ResultRecord record in records)
            {
                fSpriteBatch.DrawString(fFont, record.Key, new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), record.KeyColor.ToXNAColor(), 0, Vector2.Zero, record.Scale, SpriteEffects.None, 1.0f);
                fSpriteBatch.DrawString(fFont, record.Value, new Vector2(fScreenWidth * xPadding, fScreenHeight * yPos), record.ValueColor.ToXNAColor(), 0, Vector2.Zero, record.Scale, SpriteEffects.None, 1.0f);
                yPos += ySpacing;
            }
        }

        private string GetCurrentQuestion(QuestionType aQuestionType, QuestionEntity aEntity) 
        {
            string result = "";
            switch (aQuestionType)
            {
                case  QuestionType.Performance:
                    switch (aEntity)
                    {
                        case QuestionEntity.Testee:
                            result = "Rank your performance";
                            break;
                        case  QuestionEntity.Tester:
                            result = "Rank Testee performance";
                            break;

                    }
                    break;

                case QuestionType.Pressure:
                    switch (aEntity)
                    {
                        case QuestionEntity.Testee:
                            result = "Rank your pressure level";
                            break;
                        case QuestionEntity.Tester:
                            result = "Rank Testee pressure level";
                            break;
                    }
                    break;
            }
            return result;
        }

        private void DrawRank()
        {
            float xMargin = 0.04f;
            float xPadding = 0.3f;
            float ySpacing = 0.1f;
            float yPos = 0.05f;
            Color labelColor = Color.GreenYellow;
            QuestionData data = fQuestionCollection.GetCurrentQuestion();
            QuestionRecord record =  data.GetEntity(QuestionEntity.Testee);
            Color resultColor = record.locked ? Color.Red : Color.White;
            string question = GetCurrentQuestion(fQuestionCollection.GetCurrentQuestionType(), QuestionEntity.Testee);
            fSpriteBatch.DrawString(fFont, question, new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), labelColor);
            yPos += ySpacing;

            if (record.Rank != 0)
            {
                fSpriteBatch.DrawString(fFont, "Rank: ", new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), labelColor);
                fSpriteBatch.DrawString(fFont, record.Rank.ToString(), new Vector2(fScreenWidth * xPadding, fScreenHeight * yPos), resultColor);
                yPos += ySpacing;
            }
                
                
        }
        
        private string NumberToString(float aNum)
        {
            return ((int)Math.Round(aNum * 100)).ToString()+ "%";
        }
        
        private void DrawResults()
        {
            float xMargin = 0.04f;
            float xPadding = 0.3f;
            float ySpacing = 0.1f;
            float yPos = 0.05f;

            foreach (ResultRecord record in fDisplayResults)
            {
                fSpriteBatch.DrawString(fFont, record.Key, new Vector2(fScreenWidth * xMargin, fScreenHeight * yPos), record.KeyColor.ToXNAColor() ,0,Vector2.Zero,record.Scale,SpriteEffects.None,1.0f);
                fSpriteBatch.DrawString(fFont, record.Value, new Vector2(fScreenWidth * xPadding, fScreenHeight * yPos), record.ValueColor.ToXNAColor(),0, Vector2.Zero, record.Scale, SpriteEffects.None, 1.0f);
                yPos += ySpacing;
            }
        }

        private void UpdateData()
        {
            fTotalUpdates++;

            Object2d target = fObjects2d["redcircle"];
            Object2d crosshair = fObjects2d["crosshair"];
            
            float TargetX = (int)Math.Round(target.Position.X);
            float TargetY = (int)Math.Round(target.Position.Y);

            float CrossHairX = (int)Math.Round(crosshair.Position.X);
            float CrossHairY = (int)Math.Round(crosshair.Position.Y);



            fCrosshairTravelDistance = fCrosshairTravelDistance + fObjects2d["crosshair"].distance(fObjects2d["oldcrosshair"]);
            
            fTargetTravelDistance = fTargetTravelDistance + fObjects2d["redcircle"].distance(fObjects2d["oldredcircle"]);


            fIsSightOnTarget = fObjects2d["redcircle"].IsContain(fObjects2d["crosshair"]) ? 1 : 0;
            fCountSightInTarget +=  fIsSightOnTarget;
            
            fObjects2d["oldredcircle"].Position = fObjects2d["redcircle"].Position;
            fObjects2d["oldcrosshair"].Position = fObjects2d["crosshair"].Position;
            
            Rectangle outer = fObjects2d["mercyrectangle"].DestinationRect;
            Rectangle inner = fObjects2d["redrectangle"].DestinationRect;
            fISRedRectangleInsideMecryRect = outer.Contains(inner) ? 1 : 0;
            
            fCountRedRectInsideMercyZone +=  fISRedRectangleInsideMecryRect;
            
            int currentTick = Environment.TickCount;
            if (fSettings.Physical && currentTick - fTimeLastSerialMsg >= 10  && fSerial.Read())
            {
                AtlasSerialPoller.SensorsData data = fSerial.GetAllData();
                string msg = String.Format(",{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", 
                    TargetX,TargetY,CrossHairX,CrossHairY, fIsSightOnTarget,fISRedRectangleInsideMecryRect,data.FSR,data.GSR,data.PPG,data.ASR,target.IsVisible ? 1 : 0);
                ProcessMessage(msg);
                fTimeLastSerialMsg = currentTick;
            }
        }
        
        private void ProcessMessage(string aMsg)
        {
            if (fSettings.EyeLink)
            {
                fEyelink.Message(aMsg);
            }
            else
            {
                fCSVSB.Append("MSG " + fCSVMessageID++);
                fCSVSB.Append(aMsg);
                fCSVSB.Append("\n");
            }
            
        }

        private void UpdateRedrectangle()
        {
            if (fExperimentState == ExperimentState.Running)
            {
                Object2d RedRectangle = fObjects2d["redrectangle"];
                if (centerRedRectangle)
                {
                    RedRectangle.Position = fLowerCenter;

                     _movement_model_for_redrectangle = new movement_model(2,
                         fCurrentTrialData._red_rectangle_speed * fScreenWidth, fLowerrectangle, fCurrentTrialData._evp,
                         fCurrentTrialData._red_rectangle_delta / 1000, fCurrentTrialData._dv / 1000,
                         fCurrentTrialData._Mmin_t, fCurrentTrialData._Mmax_t);
                     centerRedRectangle = false;
                }
                else
                {
                    point next = _movement_model_for_redrectangle.next();//new point to move
                    RedRectangle.Position = new Vector2((float)next.x, (float)next.y);
                }
            }
            
        }
        

        private void SetTrialData(TrialData aTrialData)
        {
            //(type-1=MIRO or 2=PWL,
            //speed-speed * scale
            //rectangle- screen,
            //scale-influance the speed,
            //dv-control the speed,
            //delta-control the angel ,
            //minimum of range ,maximum of range) 
            List <QuestionEntity> entities = new List<QuestionEntity>();
            entities.Add(QuestionEntity.Testee);
            if (Globals.MultipleMonitors)
                entities.Add(QuestionEntity.Tester);
            
            fQuestionCollection = new QuestionCollection(new QuestionType[] { QuestionType.Performance, QuestionType.Pressure }, entities.ToArray());
            fCurrentTrialData = aTrialData;
            InitializeCoordinates();
            ResetResult();

            _movement_model_for_redrectangle = new movement_model(2,
            fCurrentTrialData._red_rectangle_speed * fScreenWidth, fLowerrectangle, fCurrentTrialData._evp,
            fCurrentTrialData._red_rectangle_delta / 100, fCurrentTrialData._Mrda / 100,
            fCurrentTrialData._Mmin_t, fCurrentTrialData._Mmax_t);

            _movement_model_for_redcircle = new movement_model(fCurrentTrialData._type,
                fCurrentTrialData._speed * fScreenWidth, fUpperrectangle, fCurrentTrialData._evp,
                fCurrentTrialData._delta / 100, fCurrentTrialData._dv / 100,
                fCurrentTrialData._minT, fCurrentTrialData._maxT);
        }

        private void ResetResult()
        {
            fCountSightInTarget =
            fTotalUpdates =
            fTargetTravelDistance =
            fCrosshairTravelDistance =
            fCountSightInTarget =
            fCountRedRectInsideMercyZone =
            fFalseAlarms =
            fTotalClicks = 0;

            fPercentOnMercyZone = 
            fPercentOnTarget = 
            fperformanceOnTrial = 0;
        }

        private void InitializeCoordinates()
        {
            fScreenWidth = fDevice.PresentationParameters.BackBufferWidth;
            fScreenHeight = fDevice.PresentationParameters.BackBufferHeight;


            //absolute size
            red_circle_width = 75;
            red_circle_hight = 75;
            //red_circle_width = (int)Math.Round((fCurrentTrialData._TargetSize * fScreenWidth));
            //red_circle_hight = red_circle_width ; //redc (int)(currentTrialData._TargetSize);

            blue_rectangle_width = (int)(fScreenWidth * fCurrentTrialData._blue_rectangle_size / 100.0);
            blue_rectangle_hight = (int)((int)(fScreenHeight - (fScreenHeight * Screenratio)) * fCurrentTrialData._blue_rectangle_size / 100.0);

            red_rectangle_width = (int)(blue_rectangle_width * (fCurrentTrialData._red_rectangle_size / 100.0) );
            red_rectangle_hight = (int)(blue_rectangle_hight * (fCurrentTrialData._red_rectangle_size / 100.0) );

            System.Drawing.Point upperLocation = new System.Drawing.Point(red_circle_width / 2, red_circle_hight / 2);
            System.Drawing.Size upperSize = new System.Drawing.Size(fScreenWidth - red_circle_width, (int)(fScreenHeight * Screenratio) - red_circle_hight);
            fUpperrectangle = new System.Drawing.Rectangle(upperLocation, upperSize);

            System.Drawing.Point lowerLocation = new System.Drawing.Point(red_rectangle_width / 2, (int)(fScreenHeight * Screenratio + red_rectangle_hight / 2));
            System.Drawing.Size lowerSize = new System.Drawing.Size((int)(fScreenWidth - red_rectangle_width), (int)(fScreenHeight - (fScreenHeight * Screenratio) - red_rectangle_hight));
            fLowerrectangle = new System.Drawing.Rectangle(lowerLocation, lowerSize);

            fUpperCenter.X = fUpperrectangle.Width / 2 + upperLocation.X;
            fUpperCenter.Y = fUpperrectangle.Height / 2 + upperLocation.Y;

            fScreenCenter.X = fScreenWidth / 2;
            fScreenCenter.Y = fScreenHeight / 2;

            fLowerCenter.X = fLowerrectangle.Width / 2 + lowerLocation.X;
            fLowerCenter.Y = fLowerrectangle.Height / 2 + lowerLocation.Y;
        }

      
        private void ShowResultsScreen()
        {
            if (Globals.MultipleMonitors)
            {
                fQuestionnaireThred = new Thread(delegate()
                {
                    int totalTrials = fSettings.IsTraining ? TrainingMaxTrials : fSettings.Trials.Count;
                    using (questionnaire = new Questionnaire(totalTrials))
                        questionnaire.Run();
                }
            );
                fQuestionnaireThred.Start();
            }
             
        }

        private bool HandleSpecificRankButton(SmartButton aButton,int newRank,QuestionRecord aQuestionRecord,int aTime)
        {
            bool update = false;
            if (newRank != aQuestionRecord.Rank)
            {
                aQuestionRecord.Rank = newRank;
                aQuestionRecord.locked = false;
                update = true;
            }
            if (aButton.TimePressed >= aTime)
            {
                aQuestionRecord.locked = true;
                aButton.SetFired();
                update = true;
            }
            return update;
        }

        private void HandleRankButton(SmartButton aButton,QuestionEntity aEntity )
        {
            if (aButton.ButtonID >= (int)Keys.D1 && aButton.ButtonID <= (int)Keys.D6 && (aButton.State == SmartButtonState.Down || aButton.State == SmartButtonState.DownFirst))
            {
                int newrank = (int)aButton.ButtonID - 48;
                QuestionRecord record = fQuestionCollection.GetCurrentQuestion().GetEntity(aEntity);

                switch (aEntity )
                {
                    case QuestionEntity.None:
                        break;
                    case QuestionEntity.Testee:
                        HandleSpecificRankButton(aButton, newrank, record, 0);
                        break;
                    case QuestionEntity.Tester:
                        if (HandleSpecificRankButton(aButton, newrank, record,500))
                            EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.RankUpdate, new RankUpdate(record.Rank, record.locked)));
                        break;
                    default:
                        break;
                }


                
                if (fQuestionCollection.NextQuestion())
                    EventMessages.Instance.SendMessage(Questionnaire.ThreadName, new CommonMessage(MessageType.SetTitle, GetCurrentQuestion(fQuestionCollection.GetCurrentQuestionType(), QuestionEntity.Tester)));

                //auto progress when questions ended
                if (fQuestionCollection.Ended)
                    ProcessAction(ActionID.Progress);
                
            }

        }

        private void OnUserInput(SmartButton aButton) 
        {
            HandleTesteeInput(aButton);
            HandleTesterInput(aButton);
         }

        private void HandleTesteeInput(SmartButton aButton)
        {
            if (aButton.Device == fJoystickInput)
                {
                    switch (aButton.ButtonID)
                    {
                        case 0 : // Main Fire Button
                            if (aButton.State == SmartButtonState.DownFirst)
                            {
                                if (fExperimentState == ExperimentState.Running && fISRedRectangleInsideMecryRect == 1)
                                    fFalseAlarms++;

                                if (fExperimentState == ExperimentState.Running)
                                    fTotalClicks++;

                                ProcessAction(ActionID.Progress);
                            }
                            /*
                            if (aButton.State == SmartButtonState.Down && aButton.TimePressed > 2000)
                            {
                                aButton.SetFired();
                                //Debug.WriteLine("Continues Press for" + aButton.TimePressed + " millisecs");
                            }
                              */  
                            
                            
                            break;
                        case 1:
                            if (aButton.State == SmartButtonState.Down /*&& aButton.TimePressed > 500*/)
                            {
                                //aButton.SetFired();
                                ProcessAction(ActionID.ForceExit);
                            }
                            break;
                        case 2:
                            if (aButton.State == SmartButtonState.DownFirst /*&& aButton.TimePressed > 500*/)
                            {
      
                                //aButton.SetFired();
                                //ProcessAction(ActionID.ForceExit);
                            }
                            break;


                    }
                }
                else   if (aButton.Device == fKeyboardInput)
                {
                    
                    switch (aButton.ButtonID)
                    {
                        case (int)Keys.Escape:
                            if (aButton.State == SmartButtonState.DownFirst)
                            {
                                if (fExperimentState == ExperimentState.Running)
                                    ProcessAction(ActionID.ForceEndTrial);                                
                            }
                            break;

                        case (int)Keys.Space:
                            if (fSettings.EyeLink && fExperimentState == ExperimentState.DriftCorrect)
                            {
                                fEyelink.AcceptTrigger();
                                short result = fEyelink.GetCalibrationResult();
                                if (result == EyeLinkManager.OK_RESULT)
                                {
                                    fEyelink.ApplyDriftCorrect();
                                    fDriftCorrectionDone = true;
                                    HideDcTarget();
                                    ProcessAction(ActionID.Progress);
                                }
                            }
                            break;

                        case (int)Keys.Enter:
                            {
                                if (aButton.State == SmartButtonState.DownFirst)
                                    ProcessAction(ActionID.Progress);                                
                            }
                            break;

                        case (int)Keys.Tab:
                            if (aButton.State == SmartButtonState.DownFirst)
                            fDebugInfoEnabled = !fDebugInfoEnabled;
                            break;
                        case (int)Keys.F12:
                            {

                                //SmartButton altBUtton = fKeyboardInput.GetButton((int)Keys.LeftAlt);
                                    if (aButton.State == SmartButtonState.DownFirst)
                                    ProcessAction(ActionID.ForceExit);
                            }
                            break;
                        default:
                            {
                                if (fExperimentState == ExperimentState.ShowQuestions)
                                    HandleRankButton(aButton, QuestionEntity.Testee);

                            }
                            break;
                        case (int)Keys.Up:
                            MoveCrossHair(0, -10);
                            break;
                        case (int)Keys.Down:
                            MoveCrossHair(0, 10);
                            break;
                        case (int)Keys.Left:
                            MoveCrossHair(-10, 0);
                            break;
                        case (int)Keys.Right:
                            MoveCrossHair(10, 0);
                            break;
                    }
                }
        }

        private void HandleTesterInput(SmartButton aButton)
        {
               if (aButton.Device == fSecondKeyboardInput)
                {
                    switch (aButton.ButtonID)
                    {
                        case (int)Keys.Enter:
                            {
                                if (aButton.State == SmartButtonState.DownFirst)
                                    ProcessAction(ActionID.Progress);
                            }
                            break;
                        default:
                            if (fExperimentState == ExperimentState.ShowQuestions)
                                HandleRankButton(aButton, QuestionEntity.Tester);
                            break;
                    }
                    //  Exit();
                }
        }

        
    }
 
}