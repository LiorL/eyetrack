﻿using Microsoft.Xna.Framework;
using Common.Messages;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using hend_track;
using System;
using System.Linq;
using Common;
using hires;

public class Test : GameThreadSafe
{

    public class Packet
    {
        public Vector2 Gaze;
        public Vector2 Eyes;
        public double Time;
    }
    private const float Speed = 30;
    private const int MaxHistoryPackets = 20;
    private const int MinHistoryPackets = 20;
    private const bool MinPacketsActive = true;
    private const double MaxTime = 1.0 / 4; 

    private int fScreenWidth = Globals.settings.ScreenWidth;
    private int fScreenHeight = Globals.settings.ScreenHeight;
    private StopWatch fStopWatch;
    
    public const string ThreadName = "test";
    GraphicsDeviceManager fGraphics;

    SpriteBatch fSpriteBatch;
    GraphicsDevice fDevice;
    SpriteFont fFont;
    Object2d fText;
    Object2d fImage;
    Object2d fMousePointer;
    Vector2 fScreenCenter;
    Vector2 fMovementVector;
    EyeLinkManager fEyeLink;

    CyclicQueue<Packet> fPackets = new CyclicQueue<Packet>(MaxHistoryPackets);

    public Test(object aObj) : base(aObj)
    {
        fMovementVector = Vector2.Zero;
        fStopWatch = new StopWatch();
        fStopWatch.Start();
       
        Random rnd = new  Random();
   
        fGraphics = new TargetedGraphicsDeviceManager(this, 0);
        Content.RootDirectory = "Content";
        fGraphics.SynchronizeWithVerticalRetrace = true;
        //EventMessages.Instance.RegisterThread(ThreadName);
        if (Globals.settings.EnableEyeLink)
        {
            fEyeLink = EyeLinkManager.Instance;
            if (fEyeLink.Connected)
                fEyeLink.start_recording();
        }

        fScreenCenter = new Vector2(fScreenWidth / 2, fScreenHeight / 2);
    }


    private double GetWeightenedValue(double aValue, double aMaxValue, string aFunction)
    {
        return Math.Pow((aValue / aMaxValue), 4);
    }

    private double GetWeightenedArea(double aValue, double aMaxValue, string aFunction)
    {
        return Math.Pow((aValue / aMaxValue), 5) / 4;
    }

    protected override void Initialize()
    {
        fGraphics.PreferredBackBufferWidth = fScreenWidth;
        fGraphics.PreferredBackBufferHeight = fScreenHeight;
        fGraphics.IsFullScreen = Globals.settings.FullScreen;
        try
        {
            fGraphics.ApplyChanges();
        }
        catch (Exception e)
        {
            MessageBox.Show("Second Monitor is not connected");
            Exit();
        }

        InitializeInput();

        base.Initialize();
    }

    SlimDX.DirectInput.DirectInput  fDirectInput;
     SlimDX.DirectInput.Mouse fMouse;
    
    private void InitializeInput()
    {
            fDirectInput = new SlimDX.DirectInput.DirectInput();
           fMouse = new SlimDX.DirectInput.Mouse(fDirectInput);
            fMouse.Acquire();
    }


    protected override void Draw(GameTime gameTime)
    {
        fGraphics.GraphicsDevice.Clear(Color.Black);
        fSpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

        fImage.Draw(fSpriteBatch);
        fMousePointer.Draw(fSpriteBatch);
        fSpriteBatch.End();
        base.Draw(gameTime);
    }


    protected override void LoadContent()
    {
        fDevice = fGraphics.GraphicsDevice;
        fSpriteBatch = new SpriteBatch(fDevice);
        fFont = Content.Load<SpriteFont>("SpriteFont1");

        fMousePointer = new Object2d(Content.Load<Texture2D>("eye"), new Vector2(50, 50), new Vector2(100, 100));
        fText = new Object2d(Content.Load<Texture2D>("text1"), new Vector2(1557, 821), fScreenCenter);
        fImage = new Object2d(Content.Load<Texture2D>("Imag1"), new Vector2(4000, 4000 * (10.0f/16) ),fScreenCenter);

        base.LoadContent();
    }

    private float Clamp(float aValue, float aMin, float aMax)
    {
        if (aValue < aMin)
            aValue = aMin;
        else
            if (aValue > aMax)
                aValue = aMax;
        return aValue;
    }

    private void AddVector(Vector2 aVector)
    {
        Vector2 force = fScreenCenter - aVector;
        Vector2 direction = force;
        
        fStopWatch.Stop();
        Packet packet = new Packet();
        packet.Time = fStopWatch.Elapsed;
        packet.Eyes = force;
        fPackets.Enqueue(packet);
    }


    private void ProcessMouse()
    {
        SlimDX.DirectInput.MouseState state = fMouse.GetCurrentState();
        //Debug.WriteLine("X " + state.X + " Y " + state.Y);
        Vector2 position = fMousePointer.Position;
        position += new Vector2(state.X, state.Y);
        position.X = Clamp(position.X, 0, fScreenWidth);
        position.Y = Clamp(position.Y, 0, fScreenHeight);
        fMousePointer.Position = position;

        if (state.IsPressed(0))
            AddVector(position);
    }

    private void ProcessEyeLink()
    {
        if (Globals.settings.EnableEyeLink && fEyeLink.Connected)
        {
            System.Drawing.PointF gaze = fEyeLink.GetEye(EyeConfig.Binocular);
            Debug.WriteLine("X: " + gaze.X + "Y: " + gaze.Y);

            if (Math.Abs(gaze.X) < 3000 && Math.Abs(gaze.Y) < 3000)
                AddVector(new Vector2(gaze.X, gaze.Y));
        }
    }

    private void CaluculateMovementVector()
    {
        int size = fPackets.Size;
        fStopWatch.Stop();
        double currentTime = fStopWatch.Elapsed;
        int totalProcessed = 0;
        int lastKnownGoodIdx = 0;
       
        for ( int i = size - 1; i >= 0; i--)
        {
            Packet packet = fPackets[i];
            double timeElapsed = currentTime - packet.Time;
            if (timeElapsed < MaxTime)
            {
                totalProcessed++;
                double weightenedValue = GetWeightenedValue(timeElapsed, MaxTime, "not implemented");
                Vector2 weightenedVector = packet.Eyes * (float)weightenedValue;
                fMovementVector += weightenedVector;
                lastKnownGoodIdx = i;

            }
            else
                break;
        }
      
        if (totalProcessed > 0)
        {
            if (MinPacketsActive && totalProcessed < MinHistoryPackets)
            {
                int idx = lastKnownGoodIdx;
                Debug.WriteLine(totalProcessed);
                //Vector2 complement = fPackets[idx].Eyes * (float)(GetWeightenedValue( (currentTime - fPackets[idx].Time), MaxTime, "not implemented")) * (MinHistoryPackets - totalProcessed);
                Vector2 complement = fPackets[idx].Eyes * (float)(GetWeightenedValue(MaxTime / 2, MaxTime, "not implemented")) * (MinHistoryPackets - totalProcessed);
                fMovementVector += complement;
                fMovementVector /= MinHistoryPackets;
            }
            else
                fMovementVector /= totalProcessed;

            fMovementVector *= (Speed / 100.0f);
        }
        else
            fMovementVector = Vector2.Zero;
    }


    protected override void Update(GameTime gameTime)
    {
        ProcessMouse();
        ProcessEyeLink();
        
        CaluculateMovementVector();
        CommitMovement();

        base.Update(gameTime);
    }

    private void CommitMovement()
    {
        if (fMovementVector != Vector2.Zero)
        {
            Vector2 move = fMovementVector;

            fImage.Position += move;

            Vector2 pos =  fImage.Position;
            pos.X = Clamp(pos.X, -1000,2600);
            pos.Y = Clamp(pos.Y, -800,2000);

            fImage.Position = pos;

        }
    }

    private float sqrtABS(float num)
    {
        return num < 0 ? (float)-Math.Sqrt(-num) : (float)Math.Sqrt(num);
    }
    protected override void OnExiting(object sender, System.EventArgs args)
    {
        base.OnExiting(sender, args);
    }
}