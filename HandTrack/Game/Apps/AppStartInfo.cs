﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hend_track.Apps
{
    public class AppStartInfo
    {
        private Type fAppType;

        public Type AppType
        {
            get { return fAppType; }
            set { fAppType = value; }
        }


        private object fStartParam;

        public object StartParam
        {
            get { return fStartParam; }
            set { fStartParam = value; }
        }
        
    }
}
