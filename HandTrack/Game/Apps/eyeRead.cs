﻿using Microsoft.Xna.Framework;
using Common.Messages;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using hend_track;
using System;

public class EyeRead : GameThreadSafe
{
    private  int fScreenWidth = Globals.settings.ScreenWidth;
    private  int fScreenHeight = Globals.settings.ScreenHeight;

    public const string ThreadName = "eyeRead";
    GraphicsDeviceManager fGraphics;

    SpriteBatch fSpriteBatch;
    GraphicsDevice fDevice;
    SpriteFont fFont;
    Object2d fText;
    Object2d fLeftEye;
    Object2d fRightEye;
    EyeLinkManager fEyeLink;

    public EyeRead(object aInitObject)
        : base(aInitObject)
    {
        fGraphics = new TargetedGraphicsDeviceManager(this, 0);
        Content.RootDirectory = "Content";
        fGraphics.SynchronizeWithVerticalRetrace = true;
        EventMessages.Instance.RegisterThread(ThreadName);
        if (Globals.settings.EnableEyeLink)
        {
            fEyeLink = EyeLinkManager.Instance;
            fEyeLink.start_recording();
        }
    }

    protected override void Initialize()
    {
        fGraphics.PreferredBackBufferWidth = fScreenWidth;
        fGraphics.PreferredBackBufferHeight = fScreenHeight;
        fGraphics.IsFullScreen = Globals.settings.FullScreen;
        try
        {
            fGraphics.ApplyChanges();
        }
        catch (Exception e)
        {
            MessageBox.Show("Second Monitor is not connected");
            Exit();
        }
        base.Initialize();
    }
    protected override void Draw(GameTime gameTime)
    {
        fGraphics.GraphicsDevice.Clear(Color.Black);
        fSpriteBatch.Begin(SpriteSortMode.Deferred,BlendState.NonPremultiplied);
        fText.Draw(fSpriteBatch);
        fLeftEye.Draw(fSpriteBatch);
        fRightEye.Draw(fSpriteBatch);
       
        fSpriteBatch.End();
        base.Draw(gameTime);
    }


    protected override void LoadContent()
    {
        fDevice = fGraphics.GraphicsDevice;
        fSpriteBatch = new SpriteBatch(fDevice);
        fFont = Content.Load<SpriteFont>("SpriteFont1");

        fLeftEye = new Object2d(Content.Load<Texture2D>("eye"),new Vector2(50,50),new Vector2(100,100));

        fLeftEye.BlendColor = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        fRightEye = new Object2d(Content.Load<Texture2D>("eye"),new Vector2(50,50),new Vector2(150,100));
        fRightEye.BlendColor = new Color(1.0f,1.0f,1.0f,0.5f);
        fText = new Object2d(Content.Load<Texture2D>("text1"),  new Vector2(1557, 821), new Vector2(fScreenWidth / 2, fScreenHeight / 2));

        base.LoadContent();
    }

    protected override void Update(GameTime gameTime)
    {
        if (Globals.settings.EnableEyeLink)
        {
            System.Drawing.PointF point = fEyeLink.GetEye(Common.EyeConfig.Left);
            fLeftEye.Position = new Vector2(point.X, point.Y);
            System.Drawing.PointF point1 = fEyeLink.GetEye(Common.EyeConfig.Right);
            fRightEye.Position = new Vector2(point1.X, point1.Y);
        }
        base.Update(gameTime);
    }
    protected override void OnExiting(object sender, System.EventArgs args)
    {
        EventMessages.Instance.UnRegister();
        if (Globals.settings.EnableEyeLink)
        {
            fEyeLink.stop_recording(null);
        }

        base.OnExiting(sender, args);
    }
}