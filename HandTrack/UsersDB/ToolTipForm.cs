﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace UsersDB
{
    public partial class ToolTipForm : Form
    {
        Timer fTimer;
        int fDelay;
        int fStartTick;
        int fTimerCount = 0;
        int fHeight = 0;
        int fWidth = 0;
        Control fAttached;
        Form fForm;
        int fVerticalPosition = 0;
        int fHorizontalPosition = 0;
        
        const int TimerInterval = 50;
        const double MaxOpacity = 0.8;
        const double OpacitySteps = 15;
        const int LayoutMargin = 10;
        const int InnerMargin = 7;
        const int LineSpacing = 5;
        const int ValueSpacing = 12;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
private static extern IntPtr CreateRoundRectRgn
(
    int nLeftRect, // x-coordinate of upper-left corner
    int nTopRect, // y-coordinate of upper-left corner
    int nRightRect, // x-coordinate of lower-right corner
    int nBottomRect, // y-coordinate of lower-right corner
    int nWidthEllipse, // height of ellipse
    int nHeightEllipse // width of ellipse
 );



        public ToolTipForm()
        {
            InitializeComponent();
            fTimer = new Timer();
            fTimer.Interval = TimerInterval;
            fTimer.Tick +=new EventHandler(fTimer_Tick);
            Visible = false;
            
        }

        protected override bool ShowWithoutActivation{get{return true;}}

        void fTimer_Tick(object sender, EventArgs e)
        {

            if (fHoldAnimation)
                return;

            fTimerCount++;

            if (fTimerCount < OpacitySteps)
            {
                double innerStep = (fTimerCount + 1) / (double)OpacitySteps;
                //SetHeight((int)(innerStep * fHeight));
                Opacity = innerStep * MaxOpacity;
            }
            else
                if (fTimerCount > fDelay / TimerInterval - OpacitySteps)
                {
                    double outerStep = ((fDelay / TimerInterval - fTimerCount) / OpacitySteps);
                    Opacity =  outerStep * MaxOpacity;
                    SetHeight((int)(outerStep * fWidth), (int)(outerStep * fHeight));
                }

            if (Opacity == 0)
            {
                fTimer.Enabled = false;
                Close();
                Dispose();
            }
        }

        

        Point GetLocation(Control aAttached)
        {
            Point controlLocation = aAttached.PointToScreen(Point.Empty);
            controlLocation.X += aAttached.Width + LayoutMargin;
            controlLocation.Y -= this.Height / 2;
            controlLocation.Y += aAttached.Height / 2;
            return controlLocation;
        }

        

        private Label CreateLabel()
        {
            Label lbl = new Label();
            lbl.AutoSize = true;
            Controls.Add(lbl);
            return lbl;
        }

        public void AddElement(string aKey, string aValue)
        {
            Label lbl = CreateLabel();
            //lbl.ForeColor = Color.FromArgb(106, 60, 5);
            lbl.ForeColor = Color.FromArgb(127, 72, 6);
            Font font = new Font("Miriam Fixed", 11);

            while (aKey.Length < 10)
                aKey =  aKey.Insert(aKey.Length, " ");
            aKey += ':';

            
            

            lbl.Font =  font;
            lbl.Text = aKey;
            lbl.Left = InnerMargin;

            if (fVerticalPosition == 0)
                fVerticalPosition = InnerMargin;
                
            lbl.Top = fVerticalPosition;
            Controls.Add(lbl);

            Graphics graphics = this.CreateGraphics();
            SizeF textSize = graphics.MeasureString(aKey, lbl.Font);

            lbl = CreateLabel();
            //lbl.ForeColor = Color.FromArgb(220, 174, 85);
            lbl.ForeColor = Color.FromArgb(255, 209, 102);
            lbl.Text = aValue;
            lbl.Top = fVerticalPosition;
            
            lbl.Left = (int)(InnerMargin + textSize.Width + ValueSpacing);

            fVerticalPosition += (int)(textSize.Height + LineSpacing);

            textSize = graphics.MeasureString(aValue, lbl.Font);

            fHorizontalPosition = (int)Math.Max(fHorizontalPosition, lbl.Left + textSize.Width + InnerMargin);

            fHeight = fVerticalPosition + InnerMargin;
            fWidth = fHorizontalPosition + InnerMargin;

        }

        private void SetHeight(int aWidth,int aHeight)
        {
            Width = aWidth;
            Height = aHeight;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
        }

        public void Show(int aDelay,Control aAttached)
        {
            SetHeight(fWidth,fHeight);
            
            fAttached = aAttached;
            
            Control control = aAttached;
            while (!(control is Form))
                control = control.Parent;

            fForm = control as Form;

            fForm.LocationChanged += new EventHandler(fForm_LocationChanged);


            Location = GetLocation(aAttached);
            
            fDelay = aDelay;
            if (fDelay < TimerInterval * OpacitySteps * 2)
                fDelay = (int)(TimerInterval * OpacitySteps * 2);
            fStartTick = Environment.TickCount;
            Opacity = 0;
            Visible = true;
            fTimer.Enabled = true;
            
        }

        void fForm_LocationChanged(object sender, EventArgs e)
        {
            Location = GetLocation(fAttached);
        }

        private void ToolTipForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            fForm.LocationChanged -= fForm_LocationChanged;
        }

        bool fHoldAnimation;
        private void ToolTipForm_MouseEnter(object sender, EventArgs e)
        {
            fHoldAnimation = true;
        }

        private void ToolTipForm_MouseLeave(object sender, EventArgs e)
        {
            fHoldAnimation = false;
        }
    }
}
