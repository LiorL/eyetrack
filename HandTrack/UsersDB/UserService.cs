﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace UsersDB
{
    public class UserService
    {
        
        private Dictionary<string,Dictionary<string, string>> fdicIDtoData;
        
        private HashSet<string> fUsers;
        private string fCurrentPath = @"C:\";
        private string fExtension = "json";
        private AutoCompleteStringCollection fAutoCompleteIds;
        private AutoCompleteStringCollection fAutoCompleteNames;
        private Dictionary<string, DataNameEntry> fDicNameRefCount;
        private Dictionary<string, Dictionary<string, string>> fDicNametoData;


        #region Singelton
        static private object fObjLock = new object();
        static private UserService fInstance;

        static public UserService Instance
        {
            get
            {
                if (fInstance == null)
                    lock (fObjLock)
                        if (fInstance == null)
                            fInstance = new UserService();
                return fInstance;
            }
        }


        private UserService()
        {
            fUsers = new HashSet<string>();
            fDicNametoData = new Dictionary<string, Dictionary<string, string>>();
            fDicNameRefCount  = new Dictionary<string, DataNameEntry>();
            fdicIDtoData = new Dictionary<string, Dictionary<string, string>>();
            fAutoCompleteIds = new AutoCompleteStringCollection();
            fAutoCompleteNames = new AutoCompleteStringCollection();
        }
        #endregion

        

        public void ShowUserForm()
        {
            UserInputForm form  = new UserInputForm();
            form.fOnUserSave = RequestSave;
            form.Show();
            
        }

        private void RemoveUser(string aUserID)
        {
            RemoveUserFromHash(aUserID);
        }
            

        private void RequestSave(Dictionary<string, string> aUserInfo)
        {
            string fullPath  = Path.ChangeExtension( Path.Combine(fCurrentPath,aUserInfo["id"]),fExtension);

            string strJson = JsonConvert.SerializeObject(aUserInfo,Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(fullPath, strJson);
            AddUser(aUserInfo);
            
        }

        private void AddUser(Dictionary<string, string> aUserInfo)
        {
            string userID = aUserInfo["id"];
            if (UserExists(userID))
                RemoveUser(userID);

            AddUserToHash(aUserInfo);
        }

        private string GetUniqueName(string aFullname,string aUserID)
        {
            return fDicNameRefCount[aFullname].hasBeenUniqed ? CreateUniqueName(aFullname,aUserID) : aFullname;
        }

        private string CreateUniqueName(string aFullname, string aUserID)
        {
            return String.Format("{0} [{1}]", aFullname, aUserID);
        }

        private void RemoveUserFromHash(string aUserID)
        {
                Dictionary<string, string> oldData = GetData(aUserID);
                string oldFullName = GetFullNameFromUserInfo(oldData);

                fDicNameRefCount[oldFullName].refCount--;
                string olduniquename = GetUniqueName(GetFullNameFromUserInfo(oldData), aUserID);
                fDicNametoData.Remove(olduniquename);
                fAutoCompleteNames.Remove(olduniquename);
                fAutoCompleteIds.Remove(aUserID);
                fdicIDtoData.Remove(aUserID);
                fUsers.Remove(aUserID);
        }

        private void AddUserToHash(Dictionary<string, string> aUserInfo)
        {
            
            string userID = aUserInfo["id"];
            if (fUsers.Contains(userID))
                throw new Exception("Duplicate user found");

            string userFullName = GetFullNameFromUserInfo(aUserInfo);

            fUsers.Add(userID);
            DataNameEntry dne;

            fDicNameRefCount.TryGetValue(userFullName,out dne);

            if (dne == default(DataNameEntry))
                dne = new DataNameEntry();
            fDicNameRefCount[userFullName] = dne;
            
            dne.refCount++;
            if (!dne.hasBeenUniqed && dne.refCount == 2)
            {
                UniqifiyUserName(userFullName);
                dne.hasBeenUniqed = true;
            }

            string userUniqueName = GetUniqueName(userFullName, userID);

            fDicNametoData[userUniqueName] = aUserInfo;
            fdicIDtoData[userID] = aUserInfo;
            fAutoCompleteIds.Add(userID);
            fAutoCompleteNames.Add(userUniqueName);
        }


        private void UniqifiyUserName(string aUserGlobalname)
        {
            Dictionary<string, string> userData = fDicNametoData[aUserGlobalname];
            fDicNametoData.Remove(aUserGlobalname);
            string userID = userData["id"];
            string userName = GetFullNameFromUserInfo(userData);
            string uniqueUserName = CreateUniqueName(userName, userID);
            fDicNametoData.Add(uniqueUserName, userData);
            fAutoCompleteNames.Remove(userName);
            fAutoCompleteNames.Add(uniqueUserName);
        }

        private string GetFullNameFromUserInfo(Dictionary<string, string> aUserInfo)
        {
            string result = "";
            if (aUserInfo["firstname"] != null)
                result += aUserInfo["firstname"];
            if (aUserInfo["lastname"] != null)
                result += " " + aUserInfo["lastname"];
            return result.ToLower();
        }


        public void Initialize(string aPath)
        {
            fCurrentPath = aPath;
            if (!Directory.Exists(fCurrentPath))
                Directory.CreateDirectory(fCurrentPath);
            ReloadUsers();
        }

        private void ReloadUsers()
        {
            string []files = Directory.GetFiles(fCurrentPath,"*." + fExtension);
            foreach (string file in files)
            {
                try
                {
                    string strJson = File.ReadAllText(file);
                    Dictionary<string, string> userInfo = new Dictionary<string, string>();
                    JObject obj = JsonConvert.DeserializeObject(strJson) as  JObject;
                    foreach (KeyValuePair<string, JToken> pair in obj)
                        userInfo.Add(pair.Key, ((pair.Value as JValue).Value) as string);
                    AddUserToHash(userInfo);
                }
                catch { }
                
            }

        }


        # region queries
        internal bool UserExists(string p)
        {
            return fUsers.Contains(p);
        }

        internal Dictionary<string,string> GetData(string aUserID)
        {
            return UserExists(aUserID) ? fdicIDtoData[aUserID] : null;
        }

        public Dictionary<string, string> GetDataByName(string aUserName)
        {
            string userName = aUserName.ToLower();
            return fDicNametoData.ContainsKey(userName) ? fDicNametoData[userName] : null;
            
        }

        internal IEnumerable<string> GetIDsLike(string aID)
        {
            return from n in fUsers where n.IndexOf(aID) > 0 select n;
        }

        public AutoCompleteStringCollection GetUserIdSet()
        {
            return fAutoCompleteIds;
        }

        public AutoCompleteStringCollection GetUserNameSet()
        {
            return fAutoCompleteNames;
        }

        internal string GetFirstIDStratingWith(string aID)
        {
            int length = aID.Length;
            IEnumerable <string> results =  from n in fUsers where n.Length >= length && n.Substring(0,length) == aID select n;
            return results.First() ;
        }

        #endregion

        public  void ShowInfoWindow(string aUserID,Control aAttached)
        {
            if (UserExists(aUserID))
            {
                ToolTipForm InfoWindow = new ToolTipForm();
                Dictionary<string, string> userData = fdicIDtoData[aUserID];
                InfoWindow.AddElement("ID", userData["id"]);
                InfoWindow.AddElement("First Name", userData["firstname"]);
                InfoWindow.AddElement("Last Name", userData["lastname"]);
                DateTime time;
                if (DateTime.TryParse(userData["age"], out time))
                {
                    TimeSpan age = DateTime.Now - time;
                    InfoWindow.AddElement("Age", (age.Days / 365).ToString());
                }
                
                
                InfoWindow.Show(3000, aAttached);
            }

        }
    }
}
