﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Common;

namespace UsersDB
{
    public partial class UserInputForm : Form
    {
        public delegate void SaveUserDelegate(Dictionary<string, string> aUserInfo);
        private Dictionary<string, Control> fDicInputs;

        public SaveUserDelegate fOnUserSave;
        public UserInputForm()
        {
            InitializeComponent();
            txtID.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtID.AutoCompleteCustomSource = UserService.Instance.GetUserIdSet();

            txtName.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtName.AutoCompleteCustomSource = UserService.Instance.GetUserNameSet();
        }



        private void UserInputForm_Load(object sender, EventArgs e)
        {
            fDicInputs = new Dictionary<string, Control>();
            List<Control> lstControls =  Utility.GetAllControls(this);
            foreach (Control control in lstControls)
            {
                string strTag =  control.Tag as string;
                if (strTag != null)
                {
                    if (((string)control.Tag).Substring(0,5).ToLower() == "input")
                        AddInput(control);
                }
            }
        }

        private void AddInput(Control control)
        {
            string key = control.Tag as string;
            if (fDicInputs.ContainsKey(key))
                throw new Exception("Duplicate Value Found");
            fDicInputs.Add(key, control);
        }

        public Dictionary<string, string> CurrentData
        {
            get
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (Control input in fDicInputs.Values)
                {
                    string key = (input.Tag as string).Split('_')[1].ToLower();
                    string value = "";
                    if (input is TextBox)
                        value = (input as TextBox).Text;
                    else if (input is CheckBox)
                        value = (input as CheckBox).Checked ? "yes" : "no";
                    else if (input is RadioButton)
                        value = (input as RadioButton).Checked ? "yes" : "no";
                    else if (input is ComboBox)
                        value = (input as ComboBox).SelectedItem as string;

                    else if (input is DateTimePicker)
                        value = (input as DateTimePicker).Value.ToString();


                    if (value == null)
                        value = "";
                    result.Add(key, value);
                }
                return result;
            }

            set
            {

                foreach (Control input in fDicInputs.Values)
                {
                    try
                    {
                        string key = (input.Tag as string).Split('_')[1].ToLower();
                        if (!value.ContainsKey(key))
                            continue;
                        string val = value[key];
                        if (input is TextBox)
                            input.Text = val;
                        else if (input is CheckBox)
                            (input as CheckBox).Checked = val == "yes" ? true : false;
                        else if (input is RadioButton)
                            (input as RadioButton).Checked = val == "yes" ? true : false;
                        else if (input is ComboBox)
                            (input as ComboBox).SelectedItem = val;
                        else if (input is DateTimePicker)
                            (input as DateTimePicker).Value = DateTime.Parse(val);
                    }
                    catch { }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool shouldSave = false;
            if (ValidateID())
            {
                if (UserService.Instance.UserExists(txtID.Text))
                {
                    if (MessageBox.Show("Overwrite User ?","",MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    shouldSave = true;
                }
                else
                {
                    shouldSave = true;
                }
            }

            if (shouldSave &&  fOnUserSave != null)
                    fOnUserSave(CurrentData);
        }

        private bool ValidateID()
        {
            bool valid = true; 
            if (!((txtID.Text != null && txtID.Text != "")))
            {
                errorProvider1.SetError(txtID, "Please enter valid ID");
                valid = false;
            }

            return valid;
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

            //return;
            //IEnumerable<string> query =  UserService.Instance.GetIDsLike((sender as TextBox).Text);
            
            //TextBox textBox = (sender as TextBox);
            //if (!String.IsNullOrWhiteSpace(textBox.Text))
            //{
            //    string id = UserService.Instance.GetFirstIDStratingWith(textBox.Text);
            //    if (id != null)
            //    {
            //        string originalID = textBox.Text;

            //        //textBox.HandleTextChange = false;
            //        textBox.Text = id;
            //        //textBox.HandleTextChange = true;
            //        textBox.Select(originalID.Length, id.Length - originalID.Length);
            //    }
            //}



            //foreach (string result in query)
            //{
            //    (sender as TextBox).Text
            //    se
            //    listBox1.Items.Add(result);
            //}
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TextBox textbox = (sender as TextBox);
                string userid = textbox.Text;
                Dictionary<string, string> data = UserService.Instance.GetData(userid);
                if (data != null)
                    CurrentData = data;
            }

        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TextBox textbox = (sender as TextBox);
                string userName = textbox.Text;
                Dictionary<string, string> data = UserService.Instance.GetDataByName(userName);
                if (data != null)
                    CurrentData = data;
            }

        }
        
    }
}
