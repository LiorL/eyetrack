﻿using System.Diagnostics;
using System.Xml.Serialization;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using System.IO;
public class Settings
{
    public int      InitialMoney = 100; 
    public int      TrainingCount = 4;
    public float    TrainingScore = 0.6f;
  
    public bool DebugMode = false;
    public bool FullScreen = true;
    public bool EnableEyeLink = false;
    public bool EnableSensors = false;

    public int KeyBoardPrim = 2;
    public int KeyBoardSec  = 1;
  
    public int ScreenWidth = -1;
    public int ScreenHeight = -1;

    public int MiliSecondsToEndExperiment = 45000;
    public string SensorsComPort = "COM5";

    public float R1x = 0.4f;
    public float R1y = 0.15f;

    public float R2x = 0.8f;
    public float R2y = 0.5f;

    public float JoystickSpeedMultiplier = 2.0f; 
    public float JoystickMinSpeed = 0; 
    public float JoystickDeadZone = 0.05f;

    public static Settings CreateFromFile(string afileName)
    {
        Settings settings = Common.XMLSerializationUtility.Deserialize<Settings>(afileName);
        settings.Initialize();
        return settings;
    }


    public void SaveTO(string aFileName)
    {
        string strXml = Common.XMLSerializationUtility.SerializeObject<Settings>(Encoding.UTF8,this,true,true);
        File.WriteAllText(aFileName,strXml);
    }


    public Settings()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (ScreenWidth == -1 || ScreenHeight == -1)
        {
            Rectangle rect = Screen.PrimaryScreen.Bounds;
            ScreenWidth = rect.Width;
            ScreenHeight = rect.Height;
        }
        
    }
}