﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Common;
using hend_track;

namespace Common
{
    public class TrialData
    {
        //the name of the test 
        //build like name_speed_kind of experiment
        //for exemple test_150_m
        public string _nameOftest;
        //the type-m-1,p-2,d-3
        public int  _type;
        //the red circle and crosshair size 
        //defult-70
        public double _TargetSize;
        //the speed of red circle
        //defult- four levels:50,80,110,140 
        public double _speed;


        public double _dv;
        //the maximum angel that the red circle move
        public double _delta;



        public double _evp;
        //red circle:minimum and maximum range of time to change the speed and angel to move
        //defult- 3-5
        public double _minT;
        public double _maxT;


        public double _invTime;
        //the angel of the red rectangle
        public double _red_rectangle_delta;
        //red rectangle:minimum and maximum range of time to change the speed and angel to move
        //defult- 3-5
        public double _Mmax_t;
        public double _Mmin_t;

        public double _Mrda;
        //the speed of red rectangle
        public double _red_rectangle_speed;
        //the blue rectangle size in percent
        //defult-50
        public int   _blue_rectangle_size;
        // the red rectangle size in percent
        //defult-50
        public int   _red_rectangle_size;
        //the mercy zone size in percent
        //defult-10
        public int _mercy_zone;
        public TrialData()
        {

            _nameOftest = "demo";
            _TargetSize = 80;
            _type = 1;
            _speed = 90;
            _dv = 0.5;
            _delta = 0.001;
            _evp = 1;
            _minT = 3;
            _maxT = 5;
            _invTime = -1;
            _Mmax_t = 5;
            _Mmin_t = 2;
            _Mrda = 0.5;
            _red_rectangle_speed = 70;
        }
        public TrialData(string name, int type, int targetsize, int speed, 
             double RRspeed,int blue,int red,int mercy)
        {
            _nameOftest = name;
            _type = type;
            _TargetSize = targetsize;
            _speed = speed;
            _delta = 0.001;
            _evp = 1;
            _minT = 3;
            _maxT = 5;
            _invTime = -1;
            _red_rectangle_delta = 0.001;
            _Mmin_t = 3;
            _Mmax_t = 5;
            _Mrda = 0.5;
            _red_rectangle_speed = RRspeed;
            _blue_rectangle_size = blue;
            _red_rectangle_size = red;
            _mercy_zone = mercy;
        }

        public TrialData(TrialData e)
        {
            _nameOftest = e._nameOftest;
            _type = e._type;
            _TargetSize = e._TargetSize;
            _speed = e._speed;
            _delta = e._delta;
            _evp = e._evp;
            _minT = e._minT;
            _maxT = e._maxT;
            _invTime = e._invTime;
            _red_rectangle_delta = e._red_rectangle_delta;
            _Mmin_t = e._Mmin_t;
            _Mmax_t = e._Mmax_t;
            _Mrda = e._Mrda;
            _red_rectangle_speed = e._red_rectangle_speed;
            _blue_rectangle_size = e._blue_rectangle_size;
            _red_rectangle_size = e._red_rectangle_size;
            _mercy_zone = e._mercy_zone;


        }

        public static TrialData Deserialize(string aFilePath)
        {
            TrialData result = null;
            try
            {
                result = Common.XMLSerializationUtility.Deserialize<TrialData>(aFilePath);
            }
            catch
            {
            }
            return result;

        }
       
        public void Serialize()
        {
            try
            {
                System.Xml.Serialization.XmlSerializer writer =
               new System.Xml.Serialization.XmlSerializer(this.GetType());
                System.IO.StreamWriter file = new System.IO.StreamWriter(Path.Combine(Globals.PathLevels,this._nameOftest + ".xml"));
                writer.Serialize(file, this);
                file.Close();
                System.Windows.Forms.MessageBox.Show("בניית הניסוי הצליחה");
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("יצירת הקובץ נכשלה");
            }

        }

    }
}
