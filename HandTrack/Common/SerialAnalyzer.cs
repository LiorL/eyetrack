﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;

namespace hend_track
{
    class SerialAnalyzer
    {
        public  delegate void ParsePacketDelegate(byte[] packet);
        ParsePacketDelegate ParsePacket;
        Thread pollingThread;
        SerialPort port;
        Control Parent;
        int Interval;
        //const int MaximumBufferSize = 4096;
        const int MaximumBufferSize = 0xFFFFFF;
        int Offset;
        int LastOffset;
        byte[] comBuffer;

        public SerialAnalyzer(Control control,string PortName,int BaudRate,int pollingInterVal,ParsePacketDelegate ParsePacketDelegate)
        {
            Parent = control;
            Interval = pollingInterVal;
            ParsePacket = ParsePacketDelegate;
            port = new SerialPort(PortName);
            port.BaudRate = 115200;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.ReadTimeout = 500;
            comBuffer = new byte[MaximumBufferSize];
            pollingThread = new Thread(new ThreadStart(SerialPolling));
            pollingThread.IsBackground = true;
        }
        
        void SerialPolling()
        {
            while (true)
            {
                int bytestoread = port.BytesToRead;

                if (bytestoread > 0)
                {
                    if (Offset + bytestoread < MaximumBufferSize)
                    {
                        port.Read(comBuffer, Offset, bytestoread);
                        LastOffset = Offset;
                        Offset = (Offset + bytestoread) % MaximumBufferSize;
                    }
                    else
                    {
                        int read1 = MaximumBufferSize - Offset;
                        int read2 = bytestoread - read1;

                        port.Read(comBuffer, Offset, read1);
                        port.Read(comBuffer, 0, read2);
                        LastOffset = Offset + read1;
                        Offset = read2;
                    }

                    byte[] Packet = BuildPacket();



                    if (Parent != null)
                    {
                        Parent.Invoke(ParsePacket, Packet);
                    }
                }
                Thread.Sleep(Interval);
            }
  
        }
        

        public void Open()
        {
            
                port.Open();
                pollingThread.Start();
          
        }

        public void Close()
        {
            port.Close();
            pollingThread.Abort();
        }
        private int FindHeader(int end)
        {
            int headerIDX = end;
            while (comBuffer[headerIDX % MaximumBufferSize] != 255)
            {
                headerIDX = headerIDX - 1;
                if (headerIDX < 0)
                    headerIDX += MaximumBufferSize;
                if (headerIDX == end)
                    return -1;
            }
            return headerIDX;
        }

    
    private byte[] GetSubBuffer(int start, int end)
        {
            byte[] subBuffer = new byte[Distance(start, end)];
            
            if (start < end)
            {
                int size = end - start;
                
                Array.Copy(comBuffer,start, subBuffer, 0,size);
            }
            else
            {
                int read1 = MaximumBufferSize - start;
                int read2 = end;
                Array.Copy(comBuffer,start,subBuffer,0, read1);
                Array.Copy(comBuffer,0, subBuffer, read1,end);
           
            }
                return subBuffer;

        }


        private int Distance(int start, int end)
        {
            if (start == end)
                return 0;
            else if (start < end) 
                return end - start;
            else 
                return (MaximumBufferSize - start + end);
        }
        
        private byte[] BuildPacket()
        {
            int Headeridx = FindHeader(Offset);
            int LastHeaderIdx = FindHeader(Headeridx - 1 );
            //Amount of new data in the last header
            int HeaderLength = Distance(LastHeaderIdx,Headeridx);

         
            int dist = Distance(Headeridx,Offset);


            //return GetSubBuffer(LastHeaderIdx + 1 , LastHeaderIdx + HeaderLength + 4);

            if (dist == 0)
                return null;

            
            
            byte [] first = GetSubBuffer(Headeridx + 1,Offset);

            byte[] Second = null;
            int Remaning = HeaderLength - dist;
            if (Remaning > 0)
            {
                Second = GetSubBuffer(((Headeridx - Remaning) + MaximumBufferSize) % MaximumBufferSize , Headeridx);
            }

            byte [] result = new byte[HeaderLength];

            first.CopyTo(result,0);
            
            if (Second != null)
            {
                Second.CopyTo(result,first.Length);
            }

            for (int i = 0; i < result.Length;i++)
            {
                //if (result[i] != comBuffer[(i +  MaximumBufferSize) % MaximumBufferSize])
                //{
                //    int b = 0;
                //}
            }

            return result;

        }
    }
}
