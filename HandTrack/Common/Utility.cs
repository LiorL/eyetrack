﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Common
{
    public class Utility
    {

        public static List<Control>  GetAllControls(Control aRootControl)
        {
            List<Control> result = new List<Control>();
            GetAllControls(aRootControl,result);
            return result;
            
        }

        private static void GetAllControls(Control aRootControl ,List<Control> aList)
        {
            if (aRootControl != null)
            {
                aList.Add(aRootControl);
                foreach (Control control in aRootControl.Controls)
                    GetAllControls(control,aList);
            }

        }

        public static void ShowCenteredModalForm(Form aParent, Form aChild)
        {
            CenterControl(aParent, aChild);
            aChild.Location = new System.Drawing.Point(1000, 0);// Left = 1000;
            aChild.ShowDialog();
        }

        public static void CenterControl(Control aParent, Control aChild)
        {
            aChild.Left = (aParent.Width - aChild.Width) / 2 + aParent.Left;
            aChild.Top = (aParent.Height - aChild.Height) / 2 + aParent.Top;
        }
    }
}
