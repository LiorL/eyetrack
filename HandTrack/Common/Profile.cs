﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Profile
    {
        string id;
        public string ID { get { return id; } }
        public Profile(string aId)
        {
            id = aId;
            if (string.IsNullOrEmpty(id))
                id = "UnTitled";
        }
    }
}
