﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Timers;
using System.Data;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using System.Xml;
namespace Common
{
    public class XMLSerializationUtility
    {
        public static T DeserializeObject<T>(Encoding encoding, string xml)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream(StringToByteArray(encoding, xml)))
                {
                    using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, encoding))
                    {
                        XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

                        return (T)xmlSerializer.Deserialize(memoryStream);
                    }
                }
            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        public static string SerializeObject<T>(Encoding encoding, T obj)
        {
            return SerializeObject<T>(encoding, obj, false, true);
        }

        public static string SerializeObject<T>(Encoding encoding, T obj, bool aIndent, bool aOmitXmlDecleration)
        {
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.OmitXmlDeclaration = aOmitXmlDecleration;
                writerSettings.Encoding = encoding;
                writerSettings.Indent = aIndent;

                using (XmlWriter xmlTextWriter = XmlTextWriter.Create(memoryStream, writerSettings))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(xmlTextWriter, obj, ns);
                }

                return ByteArrayToString(encoding, memoryStream.ToArray());
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        private static Byte[] StringToByteArray(Encoding encoding, string xml)
        {
            return encoding.GetBytes(xml);
        }

        private static string ByteArrayToString(Encoding encoding, byte[] byteArray)
        {
            return encoding.GetString(byteArray);
        }

        public static T Deserialize<T>(string aFileName)
        {
            XmlSerializer reader = new XmlSerializer(typeof(T));
            FileStream fileStream = new FileStream(aFileName, FileMode.Open, FileAccess.Read);
            return (T)reader.Deserialize(fileStream);
        }
    }


}    
