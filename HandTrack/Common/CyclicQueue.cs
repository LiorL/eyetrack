﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class CyclicQueue<T>
    {
        private int fMaxSize;
        private int fNextPosition;
        private int fElements;
        T[] fData;
        public CyclicQueue(int aSize)
        {
            fMaxSize = aSize;
            UpdateSize();
        }

        private void UpdateSize()
        {
            fData = new T[fMaxSize];
        }


        public void Enqueue(T aObj)
        {
            fData[fNextPosition] = aObj;
            
            if (fElements < fMaxSize)
                fElements++;

            fNextPosition = (fNextPosition + fMaxSize + 1) % fMaxSize;

        }


        public T this[int idx]
        {
            get { return fData[(idx + fNextPosition) % fElements]; }
        }

        public int Size { get { return fElements; } }
    }
}
