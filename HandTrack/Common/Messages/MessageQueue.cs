﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Messages
{
    internal class MessageQueue
    {
        private int fThreadID;
        private List<object> fPendingMessages;
        private bool fMessagePending;

        private bool registered;

        public bool Registered
        {
            get { return registered; }
            set { registered = value; }
        }



        

        public bool MessagePending 
        {
            get
            {
                return fMessagePending;
            }
            }

        public MessageQueue(int aThreadID,bool aRegister)
        {
            fThreadID = aThreadID;
            fPendingMessages = new List<object>();
            fMessagePending = false;
            registered = aRegister;
        }

        public List<object> CopyAndClear()
        {
            List<object> tmp = fPendingMessages;
            fPendingMessages = new List<object>();
            fMessagePending = false;
            return tmp;
        }

        public void Add(object aMessage)
        {
            fPendingMessages.Add(aMessage);
            fMessagePending = true;
        }

        
    }
}
