﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;

namespace Common.Messages
{
    public class EventMessages
    {
      #region Singelton
      static private object fObjLock = new object();
      static private EventMessages fInstance;
      
      static public EventMessages Instance
      {
        get
        {
          if (fInstance == null)
            lock(fObjLock)
              if (fInstance == null)
                fInstance = new EventMessages();
          return fInstance;
        }
      }
      #endregion

      private Dictionary<int, MessageQueue> fThreadingMessages;
      private Dictionary<string, int> fDicThreadNameID;

       
        private EventMessages()
      {
          fThreadingMessages = new Dictionary<int, MessageQueue>();
          fDicThreadNameID = new Dictionary<string, int>();
      }

        public void RegisterThread(string aID) 
        {
            lock (fObjLock)
            {
                if (!fDicThreadNameID.ContainsKey(aID))
                {
                    
                    int threaadID = Thread.CurrentThread.ManagedThreadId;
                    if (!fThreadingMessages.ContainsKey(threaadID))
                        fThreadingMessages.Add(threaadID, new MessageQueue(threaadID,true));
                    else
                        fThreadingMessages[threaadID].Registered = true;

                    fDicThreadNameID.Add(aID, threaadID);
                }
                else
                    throw new Exception("Thread already registered");
            }
        }

        public void UnRegister()
        {
            int threaadID = Thread.CurrentThread.ManagedThreadId;

            MessageQueue threadingMessages;
            if (fThreadingMessages.TryGetValue(threaadID, out threadingMessages))
             {
                 if (threadingMessages.Registered)
                 {
                     threadingMessages.Registered = false;
                     string key = null;
                     foreach (KeyValuePair<string,int> pair in fDicThreadNameID)
	                    if ( pair.Value == threaadID)
                            key = pair.Key;

                     if (key != null)
                     fDicThreadNameID.Remove(key);
                 }
             }
        }

        public List<object> GetAllMessages()
        {
            lock (fObjLock)
            {
                int threaadID = Thread.CurrentThread.ManagedThreadId;

                MessageQueue threadingMessages;
                if (fThreadingMessages.TryGetValue(threaadID, out threadingMessages))
                {
                    if (threadingMessages.MessagePending)
                        return threadingMessages.CopyAndClear();
                }

                return null;
            }
        }

        public void SendMessage(string aThreadName,object aMessage)
        {
            lock (fObjLock)
            {
                int threadID;
                if (fDicThreadNameID.TryGetValue(aThreadName, out  threadID))
                    fThreadingMessages[threadID].Add(aMessage);
            }
        }

        //public void SendMessage(int aThreadID)
        //{
        //}

        //public object GetNextMessage()
        //{
        //    return null;
        //}



     
    }
}
