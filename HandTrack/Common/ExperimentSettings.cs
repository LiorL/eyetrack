﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public enum EyeConfig { None, Left, Right, Binocular }
    public class ExperimentSettings
    {
        bool useEyeLinkAsController;
        bool demo;
        bool eyeLink;
        bool physical;
        bool isTraining;
        List<TrialData> trials;
        ExperimentLayOut layout;
        string phaseName;
        Profile userProfile;
        EyeConfig eye;

        public EyeConfig Eye { get { return eye; } set { eye = value; } }
        public bool UseEyeLinkAsController { get { return useEyeLinkAsController; } set { useEyeLinkAsController = value;} }
        public bool Demo { get {return demo;} set {demo = value;}}
        public bool EyeLink { get { return eyeLink; } set {eyeLink = value;} }
        public bool Physical { get {return physical;} set {  physical = value;} }
        public List<TrialData> Trials { get { return trials; } set { trials = value;} }
        public ExperimentLayOut Layout { get { return layout; } set {layout = value;} }
        public string PhaseName { get { return phaseName; } set { phaseName = value; } }
        public Profile UserProfile { get { return userProfile; } set { userProfile = value; } }
        public bool IsTraining{ get { return  isTraining; } set { isTraining= value; } }        

        /*
        public ExperimentSettings(bool aDemo,bool aEyeLink, bool aPhysical,bool aUseEyeLinkAsController,Profile aUserProfile ,List<TrialData> aTrials,string aName, ExperimentLayOut aLayout)
        {
            useEyeLinkAsController = aUseEyeLinkAsController;
            demo = aDemo;
            physical = aPhysical;
            eyeLink = aEyeLink;
            layout = aLayout;
            trials = aTrials;
            phaseName = aName;
            userProfile = aUserProfile;
        }*/
        
    }
}
