using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using hend_track;
using System.Diagnostics;
using Interop.SREyeLink;
using Common;

namespace hend_track
{
    public  class EyeLinkManager
    {
        private static readonly object cLockObj = new object();
        private static EyeLinkManager cInstance;
        
        public static EyeLinkManager Instance
        {
            get
            {
                if (cInstance == null)
                    lock (cLockObj)
                        if (cInstance == null)
                            cInstance = new EyeLinkManager();
                return cInstance;
            }
        }

        EyeLink fEyelink;

        private EyeLinkManager()
        {
            fEyelink = new EyeLink();
            Connect();
        }

        public EyeLinkManager(EyeLinkManager other)
        {
            fEyelink = other.fEyelink;
        }

        public const short OK_RESULT = 0;
        public const short OP_FAILED = -1;
        public const short POOR_CALIBRATION = 1;
        public const short ABORT_REPLY = 27;
        public const short NO_REPLY = 1000;

        const int KB_BUTTON = 0xFF00;
        const int KB_PRESS = 10;
        const int KB_RELEASE = -1; 
        const int KB_REPEAT =  1 ;
        const int F1_KEY = 1;

        public short GetLastCommand()
        {
            return fEyelink.commandResult();
        }

        public short GetCalibrationResult()
        {
            return fEyelink.getCalibrationResult();
        }

        public void SendKey(Keys aKey)
        {
            Connect();
            fEyelink.sendKeybutton((int)aKey, 0, KB_PRESS);
        }

        public void calibration()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            //open the exe file
            process.StartInfo.FileName = Path.Combine(Globals.PathTools, "simple.exe");
            process.Start();
            process.WaitForExit();
        }

        public void ApplyDriftCorrect()
        {
            fEyelink.applyDriftCorrect();
        }

        public bool Connected 
        {
            get
            {
                return fEyelink.isConnected();
            }
        }
        

        public short AcceptTrigger()
        {
            if (fEyelink.isConnected())
                return fEyelink.acceptTrigger();
            else
                return OP_FAILED; 
        }

        public void drift_correct(int aWidth, int aHeight)
        {
             Connect();
             fEyelink.startDriftCorrect((short)(aWidth /2), (short)(aHeight /2));
        }
        public void continue_recording() 
        {
            fEyelink.startRecording(true, true, true, true);
        }

        public void start_recording()
        {
            Connect();
            if (Connected)
            {
                fEyelink.openDataFile("temp.edf");//tmporary file to save data    
                fEyelink.startRecording(true, true, true, true);
            }
        }

        private void Connect()
        {
            if (!fEyelink.isConnected())
            {
                string msg;
                try
                {
                    fEyelink.open("100.1.1.1", 1);
                }

                catch 
                {
                    return;
                }
                fEyelink.sendCommand("select_parser_configuration 0");//
                fEyelink.sendCommand("scene_camera_gazemap = NO");
                //send what we want to measure
                msg = "file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON";
                fEyelink.sendCommand(msg);

                msg = "file_sample_data  = LEFT,RIGHT,GAZE,PUPIL,AREA,GAZERES,STATUS";
                fEyelink.sendCommand(msg);

                msg = "link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,BUTTON";
                fEyelink.sendCommand(msg);
                msg = "link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,PUPIL,AREA,STATUS";
                fEyelink.sendCommand(msg);

                msg = String.Format("screen_pixel_coords =0,0,{0},{1}", Globals.settings.ScreenWidth - 1, Globals.settings.ScreenHeight - 1);
                fEyelink.sendCommand(msg);
                msg = "calibration_type = HV9";
                fEyelink.sendCommand(msg);
            }
        }

        public PointF GetEye(EyeConfig aEye)
        {
            EL_EYE eye = EL_EYE.EL_EYE_NONE;
            switch (aEye)
            {
                case EyeConfig.None:
                    break;
                case EyeConfig.Left:
                    eye =  EL_EYE.EL_LEFT;
                    break;
                case EyeConfig.Right:
                    eye =  EL_EYE.EL_RIGHT;
                    break;
                case EyeConfig.Binocular:
                    eye = EL_EYE.EL_BINOCULAR;
                    break;
                default:
                    break;
            }
            return GetEye(eye);
        }
        
        private PointF GetEye(EL_EYE aEye)
        {
            Sample sample =  fEyelink.getNewestSample();
            return aEye == EL_EYE.EL_BINOCULAR ?  
                new PointF((sample.get_gx(EL_EYE.EL_LEFT) + sample.get_gx(EL_EYE.EL_RIGHT)) / 2, (sample.get_gy(EL_EYE.EL_LEFT) + sample.get_gy(EL_EYE.EL_RIGHT)) / 2) :
                new PointF(sample.get_gx(aEye), sample.get_gy(aEye));
        }

        public PointF LeftEye
        {
            get
            {
                return GetEye(EL_EYE.EL_LEFT);
            }
        }

        public PointF RightEye
        {
            get
            {
                return GetEye(EL_EYE.EL_RIGHT);
            }
        }

        public PointF BothEyes
        {
            get
            {
                return GetEye(EL_EYE.EL_BINOCULAR);
            }
        }
         
        public void close()
            {
                fEyelink .close ();
            }

        public void stop_recording() 
        {
            fEyelink.stopRecording();
        }

        public void stop_recording(string pathname)
        {
            bool processResult =  !String.IsNullOrEmpty(pathname);
            fEyelink.stopRecording();
            fEyelink.closeDataFile();

            if (processResult)
                fEyelink.receiveDataFile("temp", pathname);
            fEyelink.close();
            if (processResult)
            {
                
                //open the program edf2asc and put the asc file when the edf file is. 
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.FileName = Path.Combine(Globals.PathTools, "edf2asc.exe");
                process.StartInfo.Arguments = "-y " + pathname;
                process.Start();
                process.WaitForExit();
            }
        }

        public void SendImage(Bitmap aBitmap)
        {
            //TODO: implelement send image
            //fEyelink.SendImageToTracker(new 
        }

        public void Message(string messege)
        {
            fEyelink.sendMessage(messege);
        }
        public void Command(string command) 
        {
            fEyelink.sendCommand(command);
        }
    }
}
