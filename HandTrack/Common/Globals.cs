﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace hend_track
{
    public class Globals
    {
        public const string Title = "Passing Shadow Control Panel";
        public static readonly string SettingsFileName = "settings.xml";
        public static Settings settings = ReloadSettings();
        public static readonly string PathTools = Path.Combine(GetRootPath(), @"Tools");
        public static readonly string PathMedia = Path.Combine( GetRootPath(), @"Media");
        
        public static readonly string PathLevels = Path.Combine(PathMedia, "Levels");
        public static readonly string PathPhases = Path.Combine(PathMedia, "Phases");
        public static readonly string PathSequences = Path.Combine(PathMedia, "Sequences");
        public static readonly string PathDocumentation = Path.Combine(PathMedia, "Documentation");
        public static readonly string PathResults = Path.Combine(PathMedia, "Results");
        
        public static readonly bool MultipleMonitors = System.Windows.Forms.Screen.AllScreens.Length >= 2;
        

        public static Settings ReloadSettings()
        {
             settings =  Settings.CreateFromFile(SettingsFileName);
             return settings;
        }

        public static void SaveSettings()
        {
            settings.SaveTO(SettingsFileName);
        }

        public static string GetRootPath()
        {
            string path =  Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            return UpDir(path);
        }

        public static string UpDir(string aPath)
        {
           int idx = aPath.LastIndexOf('\\');
           if (idx > -1)
               return aPath.Remove(idx, aPath.Length - idx);
           else
               return aPath;
        }

        
        
    }
}
