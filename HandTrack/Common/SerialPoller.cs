﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace Serial
{
    public class SerialPoller
    {
        
        SerialPort port;
        byte[] comBuffer;
        const int MaximumBufferSize = 4096;
        //const int MaximumBufferSize = 0xFFFFFF;
        int Offset;
        int LastOffset;
        


        public SerialPoller(string PortName, int BaudRate)
        {
            Init(PortName,BaudRate);
        }

        private void Init(string PortName, int BaudRate)
        {
            port = new SerialPort(PortName);
            port.BaudRate = BaudRate;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.ReadTimeout = 500;
            comBuffer = new byte[MaximumBufferSize];
        }
        
        

        public byte[] SerialPolling()
        {
            //if (Parent != null)
            //    throw new Exception("Auto Polling Initiated");
            try
            {
                int bytestoread = port.BytesToRead;
                if (bytestoread > 0)
                {
                    if (bytestoread > MaximumBufferSize)
                    {
                        //Copy all Remaning buffer , Don't loose.
                        byte[] tempbuffer = new byte[bytestoread];
                        port.Read(tempbuffer, 0, bytestoread);
                        LastOffset = 0;
                        Offset = MaximumBufferSize;
                        Array.Copy(tempbuffer, tempbuffer.Length - MaximumBufferSize, comBuffer, 0, MaximumBufferSize);

                        //port.DiscardInBuffer();
                        //port.DiscardOutBuffer();
                    }
                    else
                        if (Offset + bytestoread < MaximumBufferSize)
                        {

                            port.Read(comBuffer, Offset, bytestoread);
                            LastOffset = Offset;
                            Offset = (Offset + bytestoread) % MaximumBufferSize;
                        }
                        else
                        {
                            int read1 = MaximumBufferSize - Offset;
                            int read2 = bytestoread - read1;

                            port.Read(comBuffer, Offset, read1);
                            port.Read(comBuffer, 0, read2);
                            LastOffset = Offset + read1;
                            Offset = read2;
                        }

                    byte[] Packet = BuildPacket();




                    return Packet;
                }
            }
            catch
            {
                
            }

            return null;
            
        }

        

        public virtual void Open()
        {
                port.Open();
          
        }

        public virtual  void Close()
        {
            port.Close();
          
        }
        private int FindHeader(int end)
        {
            int headerIDX = end;
            while (comBuffer[headerIDX % MaximumBufferSize] != 255)
            {
                headerIDX = headerIDX - 1;
                if (headerIDX < 0)
                    headerIDX += MaximumBufferSize;
                if (headerIDX == end)
                    return -1;
            }
            return headerIDX;
        }

    
    private byte[] GetSubBuffer(int start, int end)
        {
            byte[] subBuffer = new byte[Distance(start, end)];
            
            if (start < end)
            {
                int size = end - start;
                
                Array.Copy(comBuffer,start, subBuffer, 0,size);
            }
            else
            {
                int read1 = MaximumBufferSize - start;
                int read2 = end;
                Array.Copy(comBuffer,start,subBuffer,0, read1);
                Array.Copy(comBuffer,0, subBuffer, read1,end);
           
            }
                return subBuffer;

        }


        private int Distance(int start, int end)
        {
            if (start == end)
                return 0;
            else if (start < end) 
                return end - start;
            else 
                return (MaximumBufferSize - start + end);
        }
        
        private byte[] BuildPacket()
        {
            int Headeridx = FindHeader(Offset);
            int LastHeaderIdx = FindHeader((Headeridx + MaximumBufferSize  - 1 ) % MaximumBufferSize );
            //Amount of new data in the last header
            int HeaderLength = Distance(LastHeaderIdx,Headeridx);

         
            int dist = Distance(Headeridx,Offset);


            //return GetSubBuffer(LastHeaderIdx + 1 , LastHeaderIdx + HeaderLength + 4);

            if (dist == 0)
                return null;

            
            
            byte [] first = GetSubBuffer(Headeridx + 1,Offset);

            byte[] Second = null;
            int Remaning = HeaderLength - dist;
            if (Remaning > 0)
            {
                Second = GetSubBuffer(((Headeridx - Remaning) + MaximumBufferSize) % MaximumBufferSize , Headeridx);
            }

            byte [] result = new byte[HeaderLength];

            //Debug
            //if (HeaderLength < first.Length)
            //{
            //    int i = 0;
            //    i++;
            //}

            first.CopyTo(result,0);
            
            if (Second != null)
            {
                Second.CopyTo(result,first.Length);
            }

            return result;

        }
    }

    class AsyncSerialPoller : SerialPoller
    {
        public  delegate void ParsePacketDelegate(byte[] packet);
        ParsePacketDelegate ParsePacket;
        Thread pollingThread;
        Control Parent;
        int Interval;

        public override void Open()
        {
            base.Open();
            if (pollingThread != null)
                pollingThread.Start();

        }

        public override void Close()
        {
            
            if (pollingThread != null)
                pollingThread.Abort();
            base.Close();
        }

        public AsyncSerialPoller(Control control, string PortName, int BaudRate, int pollingInterVal, ParsePacketDelegate ParsePacketDelegate) :base(PortName,BaudRate)
        {
            Parent = control;
            Interval = pollingInterVal;
            ParsePacket = ParsePacketDelegate;

            pollingThread = new Thread(new ThreadStart(StartSerialPolling));
            pollingThread.IsBackground = true;

        }

        void StartSerialPolling()
        {
            while (true)
            {
                byte []Packet=  SerialPolling();
                if (Packet != null && !(Parent == null || Parent.IsDisposed))
                    Parent.Invoke(ParsePacket, Packet);
                Thread.Sleep(Interval);
            }
  
        }
    }
}
