﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
/********************************************************************
 * this class is form that represent the details of the subject     *
 * this form built with 4 textbox and one listbox                   *
 * Note that you can't save the details if you don't fill all       *
 * the details                                                      *
 * writen by Eliya ben-abu                                          *
 ********************************************************************/
namespace hend_track
{
    public partial class details : Form
    {
        string _firstName;
        string _LastName;
        string _I_D;
        string _age;
        string _gender;
        public details()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            insert_detail();//insert the details into static string (details) in the globals class
            if (!chek_full_detail ())
            {
                //this empty because the check is in the function check_full_details  
            }
            else
            {
            //Globals .details = _I_D + "_" + _firstName + "_" + _LastName + "_" + _age + "_" + _gender;
            this.Close();
            }
        }
        //this method chek if the all details fills 
        //if not he present messege box that present the missed detail
        private bool chek_full_detail()
        {
            bool ans = true;
            if (_firstName == "")
            {
                System.Windows.Forms.MessageBox.Show("input first name");
                ans = false;
            }
            if (_LastName == "")
            {
                System.Windows.Forms.MessageBox.Show("input last name");
                ans = false;
            }
            if (_I_D == "")
            {
                System.Windows.Forms.MessageBox.Show("input i.d. name");
                ans = false;
            }
            if (_age == "")
            {
                System.Windows.Forms.MessageBox.Show("input age name");
                ans = false;
            }

            if (_gender == null)
            {
                System.Windows.Forms.MessageBox.Show("select gender name");
                ans = false;
            }
            return ans;
        }

        private void listBox_gender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        //insert the details to the data
        private void insert_detail()
        {
            _firstName =  textBox_first_name.Text;
            _I_D =  textBox1_i_d.Text;
            _LastName =  textBox_last_name.Text;
            _age =  textBox_age.Text;
            _gender = (string)listBox_gender.SelectedItem;

        }
    }
}
