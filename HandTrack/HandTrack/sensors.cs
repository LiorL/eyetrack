﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using Serial;
namespace hend_track
{

    public partial class sensors : Form
    {
        AtlasSerialPoller serial;
        AtlasSerialPoller.SensorsData data;
        public sensors()
        {

            serial = new AtlasSerialPoller(Globals.ComPort,115200);
            try { serial.Open(); }
            catch { 
                System.Windows.Forms.MessageBox.Show("בדוק אם המדדים מחוברים");
                this.Close();
            }
            InitializeComponent();
           
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void SetNumber(ProgressBar trackbar, string number)
        {
          

        }

      
        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serial.Read())
            {
                try
                {
                    this.data = serial.GetAllData();
                    progressBar_Fsr.Value = data.FSR;
                    textBox_Fsr.Text = data.FSR.ToString();
                    progressBar_Gsr.Value = data.GSR;
                    textBox_Gsr.Text = data.GSR.ToString();
                    progressBar_Emg.Value = data.PPG;
                    textBox_Emg.Text = data.PPG.ToString();
                    progressBar_Asr.Value = data.ASR;
                    textBox_Asr.Text = data.ASR.ToString();
                }
                catch { ;}
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void sensors_Load(object sender, EventArgs e)
        {

        }

        private void close(object sender, FormClosingEventArgs e)
        {
            this.serial.Close();
        }
        
        
    }
}