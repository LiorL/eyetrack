﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hend_track;
using System.Threading;
using System.IO;
using Common;
using System.Diagnostics;
using UsersDB;
using hend_track.Apps;
namespace hend_track
{
    public partial class Form1 : Form
    {
        public const float version = 0.3f;
        public const float build = 6f;

        private string[] randomTrialsList; 
        private string[] randomTrialsList16;
        private string[] randomTomerList;
        private Dictionary<string, TrialData> dicTrials;
        private Dictionary<string, Phase> dicPhases;
        public const string RandomPhaseName = "Random";
        public const string RandomPhaseName16 = "Random16";
        public const string RandomTomerName = "RandomTomer";
        public static string _player_detail;
        public details _details;
        TrialData ExperimentParameters;
        int _selectedItem;

        //Info
        System.Windows.Forms.Timer fInfoTimer;
        string fPendingUserIDinfo = "";
        Control fPendingControlUserInfo;
        bool fHandleIDChange = true;
        bool fShowInfoWindow = true;


        public Form1()
        {
            InitializeComponent();

            this.Text = String.Format("{0} v{1}.{2}",Globals.Title,version,build);
            
            string [] trials = {"Test-110D", "Test-110M", "Test-110P", "Test-200D", "Test-200M", "Test-200P", "Test-50D", "Test-50M", "Test-50P", "Test-80D", "Test-80M", "Test-80P" };
            randomTrialsList = (string[])trials.Clone();
            
            string [] trialsToAdd = {"Test-200F", "Test-110F", "Test-80F", "Test-50F"};
            List<string> random16 = new List<string>(( (string[])(randomTrialsList.Clone())).ToList() );
            random16.AddRange(trialsToAdd.ToList());
            
            randomTrialsList16 = random16.ToArray();  

            string [] trialsTomer = {"Test-80P","Test-80M","Test-140P","Test-140M","Test-200P","Test-200M"};
            randomTomerList = (string[])trialsTomer.Clone();
            
            Control[] controls = new Control[Controls.Count];
            Controls.CopyTo(controls,0);
            MakeParentOf(pictureBox_background, controls);

            //UserService.Instance.Initialize(@"D:\users");

            txtID.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtID.AutoCompleteCustomSource = UserService.Instance.GetUserIdSet();

            txtName.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtName.AutoCompleteCustomSource = UserService.Instance.GetUserNameSet();

            fInfoTimer  = new System.Windows.Forms.Timer();
            fInfoTimer.Interval = 1000;
            fInfoTimer.Tick += new EventHandler(fInfoTimer_Tick);
            Width = (int)(Height * 1.61);
            
        }

        private T[] RandomizeArray<T>(T[] aArray)
        {
            T[] source = aArray.Clone() as T[];
            T[] result = new T[source.Length];
            int totalLength = source.Length;
            int targetLength = 0;
            int sourceLength = source.Length;
            Random random = new Random();

            while (targetLength < totalLength)
            {
                int srcIdx =  (int)(random.NextDouble() * sourceLength);
                result[targetLength] = source[srcIdx];
                
                SwapArray(source, srcIdx, sourceLength -1);
                targetLength++;
                sourceLength--;
            }
            return result;
        }

        private void SwapArray <T>(T[] aArray, int idx1, int idx2)
        {
            T tmp = aArray[idx1];
            aArray[idx1] = aArray[idx2];
            aArray[idx2] = tmp;
        }

        private void MakeParentOf(Control aParent,Control []aControls)
        {
            foreach (Object control in aControls)
            {
                Control ctrl = control as Control;
                if (ctrl != pictureBox_background)
                    ctrl.Parent = pictureBox_background;
            }
        }

        private void LoadData()
        {
            LoadTrials();
            LoadPhases();
            VerifyCoherence();
        }

        private void VerifyCoherence()
        {
            foreach (KeyValuePair<string,Phase> pair in  dicPhases)
                foreach (string trialName in pair.Value.lstTrials)
                    if (!dicTrials.ContainsKey(trialName))
                        throw new Exception(String.Format("Could not find trial: '{0}' in phase '{1}'",trialName,pair.Key));
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

            _details = new details();
            _details.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string filename = "movement.docx";
                filename = Path.Combine(Globals.PathDocumentation, filename);
                System.Diagnostics.Process.Start("Winword.exe", filename);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("הקובץ לא נמצא");
            }
        }

        
        private ExperimentLayOut GetCurrentLayOut()
        {
            return radioAirPlane.Checked ? ExperimentLayOut.Modern : radioClassic.Checked ? ExperimentLayOut.Classic : radioAirForce.Checked ? ExperimentLayOut.LendoltC : ExperimentLayOut.NONE;
        }


        public EyeConfig SelectedEye
        {
            get
            {
                if (rbtnEyeLeft.Checked)
                    return EyeConfig.Left;
                else if (rbtnEyeRight.Checked)
                    return EyeConfig.Right;
                else if (rbtnEyeBino.Checked)
                    return EyeConfig.Binocular;
                else
                    return EyeConfig.None;
            }
        }

        
        private void DemoButton_Click(object sender, EventArgs e)
        {
            if (lstboxTrials.SelectedIndex > -1 )
            {
                List<TrialData> lstTrials = new List<TrialData>();
                lstTrials.Add(dicTrials[(string)lstboxTrials.SelectedItem]);

                ExperimentSettings settings = new ExperimentSettings()
                {
                    Eye = SelectedEye,
                    IsTraining = true,
                    Demo = true,
                    EyeLink = checkBox_eyelink.Checked,
                    Layout =  GetCurrentLayOut(),
                    Physical = chkMeasure.Checked,
                    UseEyeLinkAsController = checkBox_eyelink.Checked & chkEyeLinkController.Checked,
                    PhaseName = "TempPhase",
                    Trials = new List<TrialData>(lstTrials),
                    UserProfile = new Profile(txtID.Text)
                };

                if (Globals.settings.EnableEyeLink && chkCalibration.Checked)
                    EyeLinkManager.Instance.calibration();
                    
                BeginStartApp(new AppStartInfo() { AppType = typeof(PassingShadow), StartParam = settings });
            }
        }

        private void StartExperiment_Click(object sender, EventArgs e)
        {
            if (lstboxPhases.SelectedIndex > -1)
            {
                string phaseName = (string)lstboxPhases.SelectedItem;
                List<TrialData> lstTrials = GeneratePhaseTrials((string)lstboxPhases.SelectedItem);
                if (phaseName == RandomPhaseName)
                {
                    dicPhases[phaseName].lstTrials = CreateNewRandom().ToList();
                    SetPhase(phaseName);
                }

                if (phaseName == RandomPhaseName16)
                {
                    dicPhases[phaseName].lstTrials = CreateNewRandom16().ToList();
                    SetPhase(phaseName);
                }

                if (phaseName == RandomTomerName)
                {
                    dicPhases[phaseName].lstTrials = CreateNewRandomTomer().ToList();
                    SetPhase(phaseName);
                }


                bool showResults = phaseName.ToLower() == "training";
                ExperimentSettings settings = new ExperimentSettings()
                {
                    Eye = SelectedEye,
                    IsTraining = showResults,
                    Demo = false,
                    EyeLink = Globals.settings.EnableEyeLink,
                    Layout = GetCurrentLayOut(),
                    Physical = Globals.settings.EnableSensors,
                    UseEyeLinkAsController = chkEyeLinkController.Checked && Globals.settings.EnableEyeLink,
                    PhaseName = phaseName,
                    Trials = lstTrials,
                    UserProfile = new Profile(txtID.Text)
                };

                if (Globals.settings.EnableEyeLink && chkCalibration.Checked)
                    EyeLinkManager.Instance.calibration();
                    
                BeginStartApp(new AppStartInfo() { AppType = typeof(PassingShadow), StartParam = settings });
            }
        }

        private void button_help_Click(object sender, EventArgs e)
        {
            try
            {
                string filename = Path.Combine(Globals.PathDocumentation, "Hend_track_documentation.docx");
                System.Diagnostics.Process.Start("Winword.exe", filename);
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("הקובץ לא קיים");
            }
        }

        private void button_create_new_experiment_Click(object sender, EventArgs e)
        {

            TrialForm experiment_details = new TrialForm(new TrialForm.ExperimentDetailsDelegate(
                delegate(TrialData parameters)
                {
                    ExperimentParameters = parameters;
                }));
            experiment_details.Show();
            LoadTrials();
        }

        

        
        private void LoadTrials()
        {
            string dest = "*.xml";
            dicTrials = new Dictionary<string, TrialData>();
            string[] files = Directory.GetFiles(Globals.PathLevels, dest, SearchOption.TopDirectoryOnly);
            foreach (string fileName in files)
                AddTrial(fileName);
        }

        private void LoadPhases()
        {
            string dest = "*.xml";
            dicPhases = new Dictionary<string, Phase>();
            Phase phase = new Phase();
            phase.lstTrials = CreateNewRandom().ToList();
            dicPhases.Add(RandomPhaseName, phase);

            phase = new Phase();
            phase.lstTrials = CreateNewRandom16().ToList();
            dicPhases.Add(RandomPhaseName16, phase);

            phase = new Phase();
            phase.lstTrials = CreateNewRandomTomer().ToList();
            dicPhases.Add(RandomTomerName, phase);

            string[] files = Directory.GetFiles(Globals.PathPhases, dest, SearchOption.TopDirectoryOnly);
            foreach (string fileName in files)
                AddPhase(fileName);
                
        }

        private void AddPhase(string fileName)
        {
            Phase phase = Phase.Deserialize(fileName);
            if (phase!= null)
                dicPhases.Add(Path.GetFileNameWithoutExtension(fileName), phase);
        }

        private void AddTrial(string fileName)
        {
            TrialData trialData = TrialData.Deserialize(fileName);
            if (trialData != null)
                dicTrials.Add(Path.GetFileNameWithoutExtension(fileName), trialData);
        }

        private List<TrialData> GeneratePhaseTrials(string aPhaseName)
        {
            List <TrialData> lstTrials = new List<TrialData>();
            List<string> trialNames = dicPhases[aPhaseName].lstTrials;

            foreach (string trialName in trialNames)
                lstTrials.Add(dicTrials[trialName]);

            return lstTrials;
            
        }
     
        private void selected_change_list_box(object sender, EventArgs e)
        {
            this._selectedItem = this.lstboxTrials.SelectedIndex;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        private void Initialize()
        {
            
            try
            {
                LoadData();
                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }

            foreach (string phaseName in dicPhases.Keys)
                lstboxPhases.Items.Add(phaseName);
        }
        
 

        private void lstboxPhases_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetPhase((string)((sender as ListBox).SelectedItem));
        }

        private void SetPhase(string aPhaseName)
        {
            lstboxTrials.Items.Clear();
            Phase currentPhase = dicPhases[aPhaseName];
            List<string> lstTrials = currentPhase.lstTrials;
            foreach (string trialName in lstTrials)
                lstboxTrials.Items.Add(trialName);
        }

        private string[] CreateNewRandom()
        {
            return  RandomizeArray<string>(randomTrialsList);
        }

        private string[] CreateNewRandom16()
        {
            return RandomizeArray<string>(randomTrialsList16);
        }

        private string[] CreateNewRandomTomer()
        {
            return RandomizeArray<string>(randomTomerList);
        }

        private void OnSensorsButtonClick(object sender, EventArgs e)
        {
            new SensorsForm().ShowDialog();
            //Utility.ShowCenteredModalForm(this, new SensorsForm());
        }

        private void OnKeyboardCalibrationButtonClick(object sender, EventArgs e)
        {
            //Utility.ShowCenteredModalForm(this, new KbdCalibationForm());
            new KbdCalibationForm().ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
             System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.FileName = Path.Combine(@"E:\DataPS_2.0", "PassingShadowPsData.exe");
                process.Start();
                process.WaitForExit();
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            if (!fHandleIDChange)
                return;
            string userID = (sender as TextBox).Text;

            if (!String.IsNullOrWhiteSpace(userID))
                RequestInfoWindowFor(sender, userID);
            else
                fShowInfoWindow = false;
        }

        
        
        private void RequestInfoWindowFor(object sender,string userID)
        {
            fPendingControlUserInfo = sender as Control;
            fPendingUserIDinfo = userID;
            fInfoTimer.Enabled = false;
            fInfoTimer.Enabled = true;
            fShowInfoWindow = true;
        }

        void  fInfoTimer_Tick(object sender, EventArgs e)
        {
            if (fShowInfoWindow)
                UserService.Instance.ShowInfoWindow(fPendingUserIDinfo,fPendingControlUserInfo);
            fInfoTimer.Enabled = false;
        }

        private void SetIDNoEvent(string aID)
        {
            fHandleIDChange = false;
            txtID.Text = aID;
            fHandleIDChange = true;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
                Dictionary<string,string> userData =  UserService.Instance.GetDataByName(txtName.Text);
                if (userData != null && userData.ContainsKey("id"))
                {
                    RequestInfoWindowFor(sender, userData["id"]);
                    SetIDNoEvent(userData["id"]);
                }
                else
                {
                    fShowInfoWindow = false;
                }
        }

        private void button_StartEyeReading(object sender, EventArgs e)
        {
            BeginStartApp(new AppStartInfo() { AppType = typeof(EyeRead) });
        }

        private void button_StartTest(object sender, EventArgs e)
        {
            BeginStartApp(new AppStartInfo() { AppType = typeof(Test) });
        }

         private void StartApp(object obj)
        {
            AppStartInfo aAppStartInfo = obj as AppStartInfo;
             using (GameThreadSafe app = Activator.CreateInstance(aAppStartInfo.AppType,aAppStartInfo.StartParam) as GameThreadSafe)
                app.Run();
        }

         private void BeginStartApp(AppStartInfo aAppStartInfo)
        {
          if (Globals.settings.DebugMode)
                Globals.settings = Settings.CreateFromFile("Settings.xml");

          Thread thread = new Thread(new ParameterizedThreadStart(StartApp));
            thread.Start(aAppStartInfo);
            thread.Join();
        }

         private void button8_Click(object sender, EventArgs e)
         {
             ExperimentSettings settings = new ExperimentSettings()
             {
                 Eye = SelectedEye,
                 Demo = false,
                 EyeLink = Globals.settings.EnableEyeLink,
                 Layout = GetCurrentLayOut(),
                 Physical = Globals.settings.EnableSensors,
                 UseEyeLinkAsController = chkEyeLinkController.Checked && Globals.settings.EnableEyeLink,
                 UserProfile = new Profile(txtID.Text)
             };

             if (Globals.settings.EnableEyeLink && chkCalibration.Checked)
                 EyeLinkManager.Instance.calibration();

             BeginStartApp(new AppStartInfo() { AppType = typeof(NewApp) ,StartParam = settings });
         }

       
         
    }
}
