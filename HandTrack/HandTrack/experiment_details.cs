﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace hend_track
{
    
    public partial class experiment_details : Form
    {
        public delegate void ExperimentDetailsDelegate(experiment_parameter details);
        public ExperimentDetailsDelegate method;
        public experiment_details(ExperimentDetailsDelegate callback)
        {
            InitializeComponent();
            method = callback;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int type;
            if (radioButton_p.Checked) type = 1;
            if (radioButton_m.Checked) type = 2;
            else type = 3;
            if (textBox_for_main_mission.Text.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("הכנס מהירות למשימה עיקרית");
            }
            else
            {
                experiment_parameter _experiment = new experiment_parameter(
                    textBox_name_file .Text ,
                    type ,
                    Convert.ToInt16 (textBox_size_of_circle .Text) ,
                    Convert.ToInt16(textBox_for_main_mission.Text),
                    Convert.ToDouble (textBox_for_small_mission.Text),
                    Convert.ToInt16(textBox_blue_rectangle_size.Text),
                    Convert.ToInt16(textBox_red_rectangle_size.Text),
                    Convert.ToInt16(textBox_mercy_zone.Text)        
                    );

                _experiment.WriteToXml();
                method(_experiment);
                Close();
                
            }
            
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }
    }
}
