﻿namespace hend_track
{
    partial class sensors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sensors));
            this.textBox_Gsr = new System.Windows.Forms.TextBox();
            this.button_end = new System.Windows.Forms.Button();
            this.progressBar_Gsr = new System.Windows.Forms.ProgressBar();
            this.progressBar_Fsr = new System.Windows.Forms.ProgressBar();
            this.textBox_Fsr = new System.Windows.Forms.TextBox();
            this.textBox_Emg = new System.Windows.Forms.TextBox();
            this.textBox_Asr = new System.Windows.Forms.TextBox();
            this.progressBar_Emg = new System.Windows.Forms.ProgressBar();
            this.progressBar_Asr = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox_Gsr
            // 
            this.textBox_Gsr.Location = new System.Drawing.Point(719, 66);
            this.textBox_Gsr.Name = "textBox_Gsr";
            this.textBox_Gsr.Size = new System.Drawing.Size(100, 22);
            this.textBox_Gsr.TabIndex = 1;
            // 
            // button_end
            // 
            this.button_end.Location = new System.Drawing.Point(407, 416);
            this.button_end.Name = "button_end";
            this.button_end.Size = new System.Drawing.Size(75, 23);
            this.button_end.TabIndex = 2;
            this.button_end.Text = "סיום";
            this.button_end.UseVisualStyleBackColor = true;
            this.button_end.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBar_Gsr
            // 
            this.progressBar_Gsr.Location = new System.Drawing.Point(12, 65);
            this.progressBar_Gsr.Maximum = 10000;
            this.progressBar_Gsr.Name = "progressBar_Gsr";
            this.progressBar_Gsr.Size = new System.Drawing.Size(689, 23);
            this.progressBar_Gsr.Step = 1000;
            this.progressBar_Gsr.TabIndex = 3;
            this.progressBar_Gsr.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // progressBar_Fsr
            // 
            this.progressBar_Fsr.Location = new System.Drawing.Point(12, 136);
            this.progressBar_Fsr.Maximum = 60000;
            this.progressBar_Fsr.Name = "progressBar_Fsr";
            this.progressBar_Fsr.Size = new System.Drawing.Size(689, 23);
            this.progressBar_Fsr.Step = 100;
            this.progressBar_Fsr.TabIndex = 4;
            // 
            // textBox_Fsr
            // 
            this.textBox_Fsr.Location = new System.Drawing.Point(719, 137);
            this.textBox_Fsr.Name = "textBox_Fsr";
            this.textBox_Fsr.Size = new System.Drawing.Size(100, 22);
            this.textBox_Fsr.TabIndex = 5;
            // 
            // textBox_Emg
            // 
            this.textBox_Emg.Location = new System.Drawing.Point(719, 212);
            this.textBox_Emg.Name = "textBox_Emg";
            this.textBox_Emg.Size = new System.Drawing.Size(100, 22);
            this.textBox_Emg.TabIndex = 6;
            // 
            // textBox_Asr
            // 
            this.textBox_Asr.Location = new System.Drawing.Point(719, 283);
            this.textBox_Asr.Name = "textBox_Asr";
            this.textBox_Asr.Size = new System.Drawing.Size(100, 22);
            this.textBox_Asr.TabIndex = 7;
            // 
            // progressBar_Emg
            // 
            this.progressBar_Emg.Location = new System.Drawing.Point(12, 211);
            this.progressBar_Emg.Maximum = 60000;
            this.progressBar_Emg.Name = "progressBar_Emg";
            this.progressBar_Emg.Size = new System.Drawing.Size(689, 23);
            this.progressBar_Emg.Step = 100;
            this.progressBar_Emg.TabIndex = 8;
            // 
            // progressBar_Asr
            // 
            this.progressBar_Asr.Location = new System.Drawing.Point(12, 283);
            this.progressBar_Asr.Maximum = 60000;
            this.progressBar_Asr.Name = "progressBar_Asr";
            this.progressBar_Asr.Size = new System.Drawing.Size(689, 23);
            this.progressBar_Asr.Step = 100;
            this.progressBar_Asr.TabIndex = 9;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(839, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "GSR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(839, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "FSR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(842, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "PPG (HR)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(845, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "ASR";
            // 
            // sensors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 580);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar_Asr);
            this.Controls.Add(this.progressBar_Emg);
            this.Controls.Add(this.textBox_Asr);
            this.Controls.Add(this.textBox_Emg);
            this.Controls.Add(this.textBox_Fsr);
            this.Controls.Add(this.progressBar_Fsr);
            this.Controls.Add(this.progressBar_Gsr);
            this.Controls.Add(this.button_end);
            this.Controls.Add(this.textBox_Gsr);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.KeyPreview = true;
            this.Name = "sensors";
            this.Text = "sensors";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.close);
            this.Load += new System.EventHandler(this.sensors_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Gsr;
        private System.Windows.Forms.Button button_end;
        private System.Windows.Forms.ProgressBar progressBar_Gsr;
        private System.Windows.Forms.ProgressBar progressBar_Fsr;
        private System.Windows.Forms.TextBox textBox_Fsr;
        private System.Windows.Forms.TextBox textBox_Emg;
        private System.Windows.Forms.TextBox textBox_Asr;
        private System.Windows.Forms.ProgressBar progressBar_Emg;
        private System.Windows.Forms.ProgressBar progressBar_Asr;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;




    }
}