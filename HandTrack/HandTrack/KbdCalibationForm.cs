﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SlimDX.RawInput;

namespace hend_track
{
    public partial class KbdCalibationForm : Form
    {
        Dictionary<IntPtr, int> dicAlias = new Dictionary<IntPtr, int>();
        const string template = @"\cf1 Please press any key on the \cf2 {0} \cf1 Keyboard";
        SmartButtons.InputDevice jointDevice;

        int fCurrentKeyboard;
        int fTotalKeyboards;

        private string GetMessageFor(string aKeyboardName)
        {
            string message = String.Format(template, aKeyboardName);
            string rtfMessage =
                String.Concat(@"{\rtf1\ansi\deff0",
                @"{\colortbl;\red20\green20\blue20;\red200\green0\blue0;}",
                message,
                "}");


            return rtfMessage;
        }
        
        public KbdCalibationForm()
        {
            fCurrentKeyboard = 0;
            InitializeComponent();
            InitializeRawInput();
            
            jointDevice = SmartButtons.InputManager.Instance.CreateDevice();

            richTextBox1.Rtf = GetMessageFor("Primary");
                
        }

        private void InitializeRawInput()
        {
                SlimDX.RawInput.Device.RegisterDevice(SlimDX.Multimedia.UsagePage.Generic, SlimDX.Multimedia.UsageId.Keyboard, DeviceFlags.None, this.Handle, true);
                SlimDX.RawInput.Device.KeyboardInput += new EventHandler<SlimDX.RawInput.KeyboardInputEventArgs>(Device_KeyboardInput);

                ICollection<SlimDX.RawInput.DeviceInfo> devices = SlimDX.RawInput.Device.GetDevices();
                int idx = 0;
                foreach (DeviceInfo device in devices)
                    if (device.DeviceType == DeviceType.Keyboard
                        &&( ( device as KeyboardInfo).DeviceName.IndexOf("ACPI")  != -1
                        || (device as KeyboardInfo).DeviceName.IndexOf("HID")  != -1))
                        dicAlias.Add(device.Handle, idx++);

                fTotalKeyboards = dicAlias.Count;
                 
        }


        bool fIswaitingForRelease;

        void Device_KeyboardInput(object sender, KeyboardInputEventArgs e)
        {
            int kbdIDX = dicAlias[e.Device];

            if (fIswaitingForRelease && e.State == KeyState.Pressed)
                return;

            if (e.State == KeyState.Released)
            {
                fIswaitingForRelease = false;
                return;
            }

            fIswaitingForRelease = true;

            if (fCurrentKeyboard == 1 && kbdIDX == Globals.settings.KeyBoardPrim)
                return;

            string nextKeyboardName = "";
            
            switch (fCurrentKeyboard)
            {
                case 0 :
                    Globals.settings.KeyBoardPrim = kbdIDX;
                    nextKeyboardName = "Secondary";
                    break;
                case 1:
                    nextKeyboardName = "none";
                    Globals.settings.KeyBoardSec = kbdIDX;
                    break;
            }

            richTextBox1.Rtf = GetMessageFor(nextKeyboardName);
            fCurrentKeyboard++;
            if (fCurrentKeyboard >= fTotalKeyboards)
                Finish();
            
        }

        private void Finish()
        {
            Globals.SaveSettings();
            Close();
        }

        private void KeyBoardTestForm_FormClosed(object sender, FormClosedEventArgs e)
        {
             SlimDX.RawInput.Device.KeyboardInput -= new EventHandler<SlimDX.RawInput.KeyboardInputEventArgs>(Device_KeyboardInput);
        }
    }
}
