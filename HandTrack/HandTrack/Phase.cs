﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Common;

namespace hend_track
{
    public class Phase
    {

        [XmlArray("trials")]
        [XmlArrayItem("t")]
        public List<string> lstTrials;

        public static Phase Deserialize(string aFilePath)
        {
            Phase result = null;
            try
            {
                result = Common.XMLSerializationUtility.Deserialize<Phase>(aFilePath);
            }
            catch 
            {
            }
            return result;
        }

    }
}
