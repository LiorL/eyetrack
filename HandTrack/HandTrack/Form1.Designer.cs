﻿namespace hend_track
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button_eperiement = new System.Windows.Forms.Button();
            this.button_demo = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button_help = new System.Windows.Forms.Button();
            this.lstboxTrials = new System.Windows.Forms.ListBox();
            this.button_create_new_experiment = new System.Windows.Forms.Button();
            this.checkBox_eyelink = new System.Windows.Forms.CheckBox();
            this.chkMeasure = new System.Windows.Forms.CheckBox();
            this.lstboxPhases = new System.Windows.Forms.ListBox();
            this.chkCalibration = new System.Windows.Forms.CheckBox();
            this.chkEyeLinkController = new System.Windows.Forms.CheckBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbtnEyeLeft = new System.Windows.Forms.RadioButton();
            this.rbtnEyeBino = new System.Windows.Forms.RadioButton();
            this.rbtnEyeRight = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioAirForce = new System.Windows.Forms.RadioButton();
            this.radioClassic = new System.Windows.Forms.RadioButton();
            this.radioAirPlane = new System.Windows.Forms.RadioButton();
            this.pictureBox_background = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_background)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(5, 79);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 26);
            this.button1.TabIndex = 0;
            this.button1.Text = "בדיקת חיישנים";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.OnSensorsButtonClick);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(5, 50);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 26);
            this.button3.TabIndex = 2;
            this.button3.Text = "משוואות תנועה";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button_eperiement
            // 
            this.button_eperiement.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button_eperiement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button_eperiement.ForeColor = System.Drawing.Color.White;
            this.button_eperiement.Location = new System.Drawing.Point(65, 188);
            this.button_eperiement.Margin = new System.Windows.Forms.Padding(2);
            this.button_eperiement.Name = "button_eperiement";
            this.button_eperiement.Size = new System.Drawing.Size(176, 28);
            this.button_eperiement.TabIndex = 5;
            this.button_eperiement.Text = "New Session";
            this.button_eperiement.UseVisualStyleBackColor = false;
            this.button_eperiement.Click += new System.EventHandler(this.StartExperiment_Click);
            // 
            // button_demo
            // 
            this.button_demo.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button_demo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button_demo.ForeColor = System.Drawing.Color.White;
            this.button_demo.Location = new System.Drawing.Point(65, 220);
            this.button_demo.Margin = new System.Windows.Forms.Padding(2);
            this.button_demo.Name = "button_demo";
            this.button_demo.Size = new System.Drawing.Size(176, 28);
            this.button_demo.TabIndex = 8;
            this.button_demo.Text = "Single Trial";
            this.button_demo.UseVisualStyleBackColor = false;
            this.button_demo.Click += new System.EventHandler(this.DemoButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "load_data";
            // 
            // button_help
            // 
            this.button_help.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button_help.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button_help.ForeColor = System.Drawing.Color.White;
            this.button_help.Location = new System.Drawing.Point(5, 281);
            this.button_help.Margin = new System.Windows.Forms.Padding(2);
            this.button_help.Name = "button_help";
            this.button_help.Size = new System.Drawing.Size(103, 26);
            this.button_help.TabIndex = 10;
            this.button_help.Text = "עזרה";
            this.button_help.UseVisualStyleBackColor = false;
            this.button_help.Click += new System.EventHandler(this.button_help_Click);
            // 
            // lstboxTrials
            // 
            this.lstboxTrials.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lstboxTrials.ForeColor = System.Drawing.Color.Black;
            this.lstboxTrials.FormattingEnabled = true;
            this.lstboxTrials.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.lstboxTrials.ItemHeight = 16;
            this.lstboxTrials.Location = new System.Drawing.Point(154, 26);
            this.lstboxTrials.Margin = new System.Windows.Forms.Padding(2);
            this.lstboxTrials.Name = "lstboxTrials";
            this.lstboxTrials.Size = new System.Drawing.Size(92, 84);
            this.lstboxTrials.TabIndex = 11;
            this.lstboxTrials.SelectedIndexChanged += new System.EventHandler(this.selected_change_list_box);
            // 
            // button_create_new_experiment
            // 
            this.button_create_new_experiment.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button_create_new_experiment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button_create_new_experiment.ForeColor = System.Drawing.Color.White;
            this.button_create_new_experiment.Location = new System.Drawing.Point(5, 18);
            this.button_create_new_experiment.Margin = new System.Windows.Forms.Padding(2);
            this.button_create_new_experiment.Name = "button_create_new_experiment";
            this.button_create_new_experiment.Size = new System.Drawing.Size(103, 26);
            this.button_create_new_experiment.TabIndex = 17;
            this.button_create_new_experiment.Text = "צור ניסוי חדש";
            this.button_create_new_experiment.UseVisualStyleBackColor = false;
            this.button_create_new_experiment.Click += new System.EventHandler(this.button_create_new_experiment_Click);
            // 
            // checkBox_eyelink
            // 
            this.checkBox_eyelink.AutoSize = true;
            this.checkBox_eyelink.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_eyelink.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.checkBox_eyelink.ForeColor = System.Drawing.Color.White;
            this.checkBox_eyelink.Location = new System.Drawing.Point(14, 39);
            this.checkBox_eyelink.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox_eyelink.Name = "checkBox_eyelink";
            this.checkBox_eyelink.Size = new System.Drawing.Size(75, 20);
            this.checkBox_eyelink.TabIndex = 23;
            this.checkBox_eyelink.Text = "EyeLink";
            this.checkBox_eyelink.UseVisualStyleBackColor = false;
            // 
            // chkMeasure
            // 
            this.chkMeasure.AutoSize = true;
            this.chkMeasure.BackColor = System.Drawing.Color.Transparent;
            this.chkMeasure.ForeColor = System.Drawing.Color.White;
            this.chkMeasure.Location = new System.Drawing.Point(14, 60);
            this.chkMeasure.Margin = new System.Windows.Forms.Padding(2);
            this.chkMeasure.Name = "chkMeasure";
            this.chkMeasure.Size = new System.Drawing.Size(116, 24);
            this.chkMeasure.TabIndex = 24;
            this.chkMeasure.Text = "Bio sernsors";
            this.chkMeasure.UseVisualStyleBackColor = false;
            // 
            // lstboxPhases
            // 
            this.lstboxPhases.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lstboxPhases.ForeColor = System.Drawing.Color.Black;
            this.lstboxPhases.FormattingEnabled = true;
            this.lstboxPhases.ItemHeight = 16;
            this.lstboxPhases.Location = new System.Drawing.Point(46, 26);
            this.lstboxPhases.Name = "lstboxPhases";
            this.lstboxPhases.Size = new System.Drawing.Size(98, 84);
            this.lstboxPhases.TabIndex = 25;
            this.lstboxPhases.SelectedIndexChanged += new System.EventHandler(this.lstboxPhases_SelectedIndexChanged);
            // 
            // chkCalibration
            // 
            this.chkCalibration.AutoSize = true;
            this.chkCalibration.BackColor = System.Drawing.Color.Transparent;
            this.chkCalibration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chkCalibration.ForeColor = System.Drawing.Color.White;
            this.chkCalibration.Location = new System.Drawing.Point(14, 81);
            this.chkCalibration.Margin = new System.Windows.Forms.Padding(2);
            this.chkCalibration.Name = "chkCalibration";
            this.chkCalibration.Size = new System.Drawing.Size(91, 20);
            this.chkCalibration.TabIndex = 26;
            this.chkCalibration.Text = "Calibration";
            this.chkCalibration.UseVisualStyleBackColor = false;
            // 
            // chkEyeLinkController
            // 
            this.chkEyeLinkController.AutoSize = true;
            this.chkEyeLinkController.BackColor = System.Drawing.Color.Transparent;
            this.chkEyeLinkController.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chkEyeLinkController.ForeColor = System.Drawing.Color.White;
            this.chkEyeLinkController.Location = new System.Drawing.Point(14, 18);
            this.chkEyeLinkController.Margin = new System.Windows.Forms.Padding(2);
            this.chkEyeLinkController.Name = "chkEyeLinkController";
            this.chkEyeLinkController.Size = new System.Drawing.Size(132, 20);
            this.chkEyeLinkController.TabIndex = 27;
            this.chkEyeLinkController.Text = "EyeLinkController";
            this.chkEyeLinkController.UseVisualStyleBackColor = false;
            // 
            // txtID
            // 
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtID.ForeColor = System.Drawing.Color.Black;
            this.txtID.Location = new System.Drawing.Point(134, 136);
            this.txtID.Margin = new System.Windows.Forms.Padding(2);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 22);
            this.txtID.TabIndex = 0;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(85, 139);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 16);
            this.label4.TabIndex = 29;
            this.label4.Text = "I.D";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.radioAirForce);
            this.groupBox1.Controls.Add(this.radioClassic);
            this.groupBox1.Controls.Add(this.radioAirPlane);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.groupBox1.Location = new System.Drawing.Point(20, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(96, 249);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Experiment Type";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.chkEyeLinkController);
            this.groupBox2.Controls.Add(this.checkBox_eyelink);
            this.groupBox2.Controls.Add(this.chkMeasure);
            this.groupBox2.Controls.Add(this.chkCalibration);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.groupBox2.Location = new System.Drawing.Point(57, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(154, 115);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.button_create_new_experiment);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button_help);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.groupBox3.Location = new System.Drawing.Point(46, 74);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(114, 314);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Helpers";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(6, 107);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(103, 26);
            this.button4.TabIndex = 10;
            this.button4.Text = "כיול מקלדות";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.OnKeyboardCalibrationButtonClick);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(5, 167);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(103, 26);
            this.button6.TabIndex = 10;
            this.button6.Text = "בפיתוח";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button_StartTest);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(6, 254);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(103, 26);
            this.button8.TabIndex = 10;
            this.button8.Text = "NewAPP";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(6, 137);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(103, 26);
            this.button5.TabIndex = 10;
            this.button5.Text = "קריאת טקסט";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button_StartEyeReading);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(5, 197);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(103, 26);
            this.button7.TabIndex = 10;
            this.button7.Text = "נתוני משתתף";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(5, 224);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 26);
            this.button2.TabIndex = 10;
            this.button2.Text = "משתמשים";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.lstboxPhases);
            this.groupBox4.Controls.Add(this.lstboxTrials);
            this.groupBox4.Controls.Add(this.button_demo);
            this.groupBox4.Controls.Add(this.button_eperiement);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtName);
            this.groupBox4.Controls.Add(this.txtID);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.groupBox4.Location = new System.Drawing.Point(94, 133);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(308, 264);
            this.groupBox4.TabIndex = 33;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Experiment parameters";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(85, 165);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 16);
            this.label1.TabIndex = 29;
            this.label1.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtName.ForeColor = System.Drawing.Color.Black;
            this.txtName.Location = new System.Drawing.Point(134, 162);
            this.txtName.Margin = new System.Windows.Forms.Padding(2);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 22);
            this.txtName.TabIndex = 1;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(840, 532);
            this.splitContainer1.SplitterDistance = 128;
            this.splitContainer1.TabIndex = 34;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(840, 400);
            this.splitContainer2.SplitterDistance = 176;
            this.splitContainer2.TabIndex = 34;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox5);
            this.splitContainer3.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer3.Panel1.Controls.Add(this.groupBox4);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer3.Size = new System.Drawing.Size(660, 400);
            this.splitContainer3.SplitterDistance = 458;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbtnEyeLeft);
            this.groupBox5.Controls.Add(this.rbtnEyeBino);
            this.groupBox5.Controls.Add(this.rbtnEyeRight);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.groupBox5.Location = new System.Drawing.Point(274, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(146, 111);
            this.groupBox5.TabIndex = 34;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Eye configuration";
            // 
            // rbtnEyeLeft
            // 
            this.rbtnEyeLeft.AutoSize = true;
            this.rbtnEyeLeft.Checked = true;
            this.rbtnEyeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.rbtnEyeLeft.ForeColor = System.Drawing.Color.White;
            this.rbtnEyeLeft.Location = new System.Drawing.Point(7, 21);
            this.rbtnEyeLeft.Name = "rbtnEyeLeft";
            this.rbtnEyeLeft.Size = new System.Drawing.Size(47, 20);
            this.rbtnEyeLeft.TabIndex = 2;
            this.rbtnEyeLeft.TabStop = true;
            this.rbtnEyeLeft.Text = "Left";
            this.rbtnEyeLeft.UseVisualStyleBackColor = true;
            // 
            // rbtnEyeBino
            // 
            this.rbtnEyeBino.AutoSize = true;
            this.rbtnEyeBino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.rbtnEyeBino.ForeColor = System.Drawing.Color.White;
            this.rbtnEyeBino.Location = new System.Drawing.Point(7, 46);
            this.rbtnEyeBino.Name = "rbtnEyeBino";
            this.rbtnEyeBino.Size = new System.Drawing.Size(82, 20);
            this.rbtnEyeBino.TabIndex = 1;
            this.rbtnEyeBino.Text = "Binocular";
            this.rbtnEyeBino.UseVisualStyleBackColor = true;
            // 
            // rbtnEyeRight
            // 
            this.rbtnEyeRight.AutoSize = true;
            this.rbtnEyeRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.rbtnEyeRight.ForeColor = System.Drawing.Color.White;
            this.rbtnEyeRight.Location = new System.Drawing.Point(7, 69);
            this.rbtnEyeRight.Name = "rbtnEyeRight";
            this.rbtnEyeRight.Size = new System.Drawing.Size(57, 20);
            this.rbtnEyeRight.TabIndex = 0;
            this.rbtnEyeRight.Text = "Right";
            this.rbtnEyeRight.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::hend_track.Properties.Resources.ariel2;
            this.pictureBox1.Location = new System.Drawing.Point(199, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(444, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // radioAirForce
            // 
            this.radioAirForce.BackColor = System.Drawing.Color.Transparent;
            this.radioAirForce.BackgroundImage = global::hend_track.Properties.Resources.tomer;
            this.radioAirForce.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioAirForce.Location = new System.Drawing.Point(14, 165);
            this.radioAirForce.Name = "radioAirForce";
            this.radioAirForce.Size = new System.Drawing.Size(61, 70);
            this.radioAirForce.TabIndex = 17;
            this.radioAirForce.TabStop = true;
            this.radioAirForce.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioAirForce.UseVisualStyleBackColor = false;
            // 
            // radioClassic
            // 
            this.radioClassic.BackColor = System.Drawing.Color.Transparent;
            this.radioClassic.BackgroundImage = global::hend_track.Properties.Resources.redcirclets;
            this.radioClassic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioClassic.Checked = true;
            this.radioClassic.Location = new System.Drawing.Point(15, 52);
            this.radioClassic.Margin = new System.Windows.Forms.Padding(2);
            this.radioClassic.Name = "radioClassic";
            this.radioClassic.Size = new System.Drawing.Size(49, 49);
            this.radioClassic.TabIndex = 15;
            this.radioClassic.TabStop = true;
            this.radioClassic.UseVisualStyleBackColor = false;
            // 
            // radioAirPlane
            // 
            this.radioAirPlane.BackColor = System.Drawing.Color.Transparent;
            this.radioAirPlane.BackgroundImage = global::hend_track.Properties.Resources.helli;
            this.radioAirPlane.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radioAirPlane.Location = new System.Drawing.Point(15, 94);
            this.radioAirPlane.Margin = new System.Windows.Forms.Padding(2);
            this.radioAirPlane.Name = "radioAirPlane";
            this.radioAirPlane.Size = new System.Drawing.Size(60, 66);
            this.radioAirPlane.TabIndex = 16;
            this.radioAirPlane.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioAirPlane.UseVisualStyleBackColor = false;
            // 
            // pictureBox_background
            // 
            this.pictureBox_background.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_background.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_background.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox_background.Name = "pictureBox_background";
            this.pictureBox_background.Size = new System.Drawing.Size(840, 532);
            this.pictureBox_background.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_background.TabIndex = 24;
            this.pictureBox_background.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(46)))), ((int)(((byte)(60)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(840, 532);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pictureBox_background);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "hend track experiment v003";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_background)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button_eperiement;
        private System.Windows.Forms.Button button_demo;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button_help;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton radioClassic;
        private System.Windows.Forms.Button button_create_new_experiment;
        public System.Windows.Forms.ListBox lstboxTrials;
        private System.Windows.Forms.CheckBox checkBox_eyelink;
        private System.Windows.Forms.RadioButton radioAirPlane;
        private System.Windows.Forms.PictureBox pictureBox_background;
        private System.Windows.Forms.CheckBox chkMeasure;
        private System.Windows.Forms.ListBox lstboxPhases;
        private System.Windows.Forms.CheckBox chkCalibration;
        private System.Windows.Forms.CheckBox chkEyeLinkController;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioAirForce;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbtnEyeLeft;
        private System.Windows.Forms.RadioButton rbtnEyeBino;
        private System.Windows.Forms.RadioButton rbtnEyeRight;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
    }
}

