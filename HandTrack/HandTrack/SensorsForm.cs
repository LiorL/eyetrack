﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using Serial;
using PenLink;
using System.IO;
namespace hend_track
{

    public partial class SensorsForm : Form
    {
        AtlasSerialPoller fSerial;
        AtlasSerialPoller.SensorsData data;
        StatusStripNotification.DeviceStatus fSensorsStatus;
        System.Windows.Forms.Timer fProbeDevicesTimer;
        
        public SensorsForm()
        {
            InitializeComponent();
            fProbeDevicesTimer = new System.Windows.Forms.Timer();
            fProbeDevicesTimer.Interval = 3000;
            fProbeDevicesTimer.Enabled = false;
            fProbeDevicesTimer.Tick += new EventHandler(ProbeDevices);
            SetSensors(StatusStripNotification.DeviceStatus.Off);
          
        }


        private void SetSensors(StatusStripNotification.DeviceStatus aStatus)
        {
            fSensorsStatus = aStatus;
            statusStripNotification1.ChangeOrAddElementStatus("Sensors", fSensorsStatus);
        }
        
        void ProbeDevices(object obj, EventArgs args)
        {
            bool result = true;

          
            if (fSensorsStatus == StatusStripNotification.DeviceStatus.Off)
                result &= ProbSensors();

            if (result)
                fProbeDevicesTimer.Enabled = false;
           
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (fSensorsStatus == StatusStripNotification.DeviceStatus.On)
            {
                if (fSerial.Read())
                {
                        this.data = fSerial.GetAllData();
                        progressBar_Fsr.Value = data.FSR;
                        textBox_Fsr.Text = data.FSR.ToString();
                        progressBar_Gsr.Value = data.GSR;
                        textBox_Gsr.Text = data.GSR.ToString();
                        progressBar_Emg.Value = data.PPG;
                        textBox_Emg.Text = data.PPG.ToString();
                        progressBar_Asr.Value = data.ASR;
                        textBox_Asr.Text = data.ASR.ToString();
                    }
                else {
                        //SetSensors(StatusStripNotification.DeviceStatus.Off);
                        //timer1.Enabled = false;
                        //fProbeDevicesTimer.Enabled = true;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
            
        }

        private void close(object sender, FormClosingEventArgs e)
        {
            fSerial.Close();
        }

        bool ProbSensors()
        {
            bool result = true;
            if (fSensorsStatus == StatusStripNotification.DeviceStatus.On)
                return result;
            if (fSerial == null)
                fSerial = new AtlasSerialPoller(Globals.settings.SensorsComPort, 115200);
            try
            {
                fSerial.Open();
                SetSensors(StatusStripNotification.DeviceStatus.On);
            }
            catch (IOException exception)
            {
                SetSensors(StatusStripNotification.DeviceStatus.Off);
                result = false;
            }
            //fSerial.Close();
            return result;
        }

        private void SensorsForm_Load(object sender, EventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(Start));
        }

        private void Start()
        {
            if (!ProbSensors())
            {
                fProbeDevicesTimer.Enabled = true;
            }
        }
    }
}