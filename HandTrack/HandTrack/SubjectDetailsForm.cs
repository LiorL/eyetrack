﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UIModels;
/********************************************************************
 * this class is form that represent the details of the subject     *
 * this form built with 4 textbox and one listbox                   *
 * Note that you can't save the details if you don't fill all       *
 * the details                                                      *
 * writen by Eliya ben-abu                                          *
 ********************************************************************/
namespace hend_track
{
    public partial class SubjectDetailsForm : Form,ISubjectDetails
    {
       
        public SubjectDetailsForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.GetDetails!=null)
            {
                foreach (var item in GetDetails.GetInvocationList())
                {
                    GetDetails(this, null);
                }
            }
        }

        public string FirstName
        {
            get 
            {
                if (this.textBox_first_name.Text.CompareTo("")==0)
                {
                    MessageBox.Show("input First Name");
                    return null;
                }
                return this.textBox_first_name.Text; 
            }
        }

        public string LastName
        {
            get
            {
                if (this.textBox_last_name.Text.CompareTo("") == 0)
                {
                    MessageBox.Show("input last Name");
                    return null;
                }
                return this.textBox_last_name.Text;
            }
        }

        public string ID
        {
            get
            {
                if (this.textBox1_i_d.Text.CompareTo("") == 0)
                {
                    MessageBox.Show("input id");
                    return null;
                }
                return this.textBox1_i_d.Text;
            }
        }

        public string Age
        {
            get
            {
                if (this.textBox_age.Text.CompareTo("") == 0)
                {
                    MessageBox.Show("input age");
                    return null;
                }
                return this.textBox_age.Text;
            }
        }

        public string Gender
        {
            get
            {
                if (this.listBox_gender.SelectedItem.ToString().CompareTo("") == 0)
                {
                    MessageBox.Show("input gender");
                    return null;
                }
                return this.listBox_gender.SelectedItem.ToString();
            }
        }

        public event EventHandler GetDetails;





        public void ShowMassege()
        {
            MessageBox.Show("the details dont saved input the details that you missed");
        }


    }
}
