﻿namespace hend_track
{
    partial class level
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_insert = new System.Windows.Forms.Button();
            this.listBox_level = new System.Windows.Forms.ListBox();
            this.label_level = new System.Windows.Forms.Label();
            this.listBox_speed = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_insert
            // 
            this.button_insert.Location = new System.Drawing.Point(95, 282);
            this.button_insert.Name = "button_insert";
            this.button_insert.Size = new System.Drawing.Size(139, 43);
            this.button_insert.TabIndex = 0;
            this.button_insert.Text = "שמור";
            this.button_insert.UseVisualStyleBackColor = true;
            this.button_insert.Click += new System.EventHandler(this.button_insert_Click);
            // 
            // listBox_level
            // 
            this.listBox_level.FormattingEnabled = true;
            this.listBox_level.ItemHeight = 16;
            this.listBox_level.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.listBox_level.Location = new System.Drawing.Point(114, 36);
            this.listBox_level.Name = "listBox_level";
            this.listBox_level.Size = new System.Drawing.Size(40, 52);
            this.listBox_level.TabIndex = 1;
            // 
            // label_level
            // 
            this.label_level.AutoSize = true;
            this.label_level.Location = new System.Drawing.Point(164, 36);
            this.label_level.Name = "label_level";
            this.label_level.Size = new System.Drawing.Size(87, 17);
            this.label_level.TabIndex = 2;
            this.label_level.Text = "בחר רמת קושי";
            // 
            // listBox_speed
            // 
            this.listBox_speed.FormattingEnabled = true;
            this.listBox_speed.ItemHeight = 16;
            this.listBox_speed.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.listBox_speed.Location = new System.Drawing.Point(114, 127);
            this.listBox_speed.Name = "listBox_speed";
            this.listBox_speed.ScrollAlwaysVisible = true;
            this.listBox_speed.Size = new System.Drawing.Size(40, 84);
            this.listBox_speed.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "בחר מהירות";
            // 
            // level
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 354);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_speed);
            this.Controls.Add(this.label_level);
            this.Controls.Add(this.listBox_level);
            this.Controls.Add(this.button_insert);
            this.Name = "level";
            this.Text = "רמת קושי";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_insert;
        private System.Windows.Forms.ListBox listBox_level;
        private System.Windows.Forms.Label label_level;
        private System.Windows.Forms.ListBox listBox_speed;
        private System.Windows.Forms.Label label1;
    }
}