﻿namespace hend_track
{
    partial class TrialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrialForm));
            this.button_create = new System.Windows.Forms.Button();
            this.radioButton_m = new System.Windows.Forms.RadioButton();
            this.radioButton_p = new System.Windows.Forms.RadioButton();
            this.radioButton_d = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_for_small_mission = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_name_file = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_for_main_mission = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_size_of_circle = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_red_rectangle_size = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox_blue_rectangle_size = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox_mercy_zone = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_create
            // 
            this.button_create.Location = new System.Drawing.Point(284, 424);
            this.button_create.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_create.Name = "button_create";
            this.button_create.Size = new System.Drawing.Size(140, 33);
            this.button_create.TabIndex = 0;
            this.button_create.Text = "צור ניסוי";
            this.button_create.UseVisualStyleBackColor = true;
            this.button_create.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButton_m
            // 
            this.radioButton_m.AutoSize = true;
            this.radioButton_m.Checked = true;
            this.radioButton_m.Location = new System.Drawing.Point(625, 323);
            this.radioButton_m.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButton_m.Name = "radioButton_m";
            this.radioButton_m.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton_m.Size = new System.Drawing.Size(98, 17);
            this.radioButton_m.TabIndex = 1;
            this.radioButton_m.TabStop = true;
            this.radioButton_m.Text = "תנועה מעגלית";
            this.radioButton_m.UseVisualStyleBackColor = true;
            // 
            // radioButton_p
            // 
            this.radioButton_p.AutoSize = true;
            this.radioButton_p.Location = new System.Drawing.Point(635, 344);
            this.radioButton_p.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButton_p.Name = "radioButton_p";
            this.radioButton_p.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton_p.Size = new System.Drawing.Size(89, 17);
            this.radioButton_p.TabIndex = 2;
            this.radioButton_p.Text = "קווים ישרים";
            this.radioButton_p.UseVisualStyleBackColor = true;
            // 
            // radioButton_d
            // 
            this.radioButton_d.AutoSize = true;
            this.radioButton_d.Location = new System.Drawing.Point(608, 367);
            this.radioButton_d.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButton_d.Name = "radioButton_d";
            this.radioButton_d.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton_d.Size = new System.Drawing.Size(123, 17);
            this.radioButton_d.TabIndex = 3;
            this.radioButton_d.Text = "קווים ישרים ונעלם";
            this.radioButton_d.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Indigo;
            this.label1.Location = new System.Drawing.Point(605, 295);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "משוואות תנועה";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Indigo;
            this.label7.Location = new System.Drawing.Point(410, 141);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(144, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "מהירות משימה משנית";
            // 
            // textBox_for_small_mission
            // 
            this.textBox_for_small_mission.Location = new System.Drawing.Point(508, 196);
            this.textBox_for_small_mission.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_for_small_mission.Name = "textBox_for_small_mission";
            this.textBox_for_small_mission.Size = new System.Drawing.Size(27, 20);
            this.textBox_for_small_mission.TabIndex = 12;
            this.textBox_for_small_mission.Text = "40";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(469, 168);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "ברירת מחדל-40";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Indigo;
            this.label9.Location = new System.Drawing.Point(356, 7);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "שם ניסוי";
            // 
            // textBox_name_file
            // 
            this.textBox_name_file.Location = new System.Drawing.Point(311, 79);
            this.textBox_name_file.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_name_file.Name = "textBox_name_file";
            this.textBox_name_file.Size = new System.Drawing.Size(103, 20);
            this.textBox_name_file.TabIndex = 15;
            this.textBox_name_file.Text = "experiment";
            // 
            // label10
            // 
            this.label10.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label10.AllowDrop = true;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(294, 32);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "אותיות באנגלית ומספרים בלבד";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.UseCompatibleTextRendering = true;
            // 
            // textBox_for_main_mission
            // 
            this.textBox_for_main_mission.Location = new System.Drawing.Point(676, 259);
            this.textBox_for_main_mission.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_for_main_mission.Name = "textBox_for_main_mission";
            this.textBox_for_main_mission.Size = new System.Drawing.Size(27, 20);
            this.textBox_for_main_mission.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(631, 236);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "140-מהיר מאוד";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(654, 213);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "110-מהיר";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(662, 192);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "80-איטי";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(639, 171);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "50-איטי מאוד";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Indigo;
            this.label2.Location = new System.Drawing.Point(568, 141);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "מהירות משימה ראשית";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Indigo;
            this.label11.Location = new System.Drawing.Point(320, 141);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 17);
            this.label11.TabIndex = 23;
            this.label11.Text = "גודל הכדור";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // textBox_size_of_circle
            // 
            this.textBox_size_of_circle.Location = new System.Drawing.Point(359, 196);
            this.textBox_size_of_circle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_size_of_circle.Name = "textBox_size_of_circle";
            this.textBox_size_of_circle.Size = new System.Drawing.Size(27, 20);
            this.textBox_size_of_circle.TabIndex = 24;
            this.textBox_size_of_circle.Text = "70";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(310, 171);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "גודל הכדור והכוונת";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(118, 168);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(216, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "גודל הריבוע האדום באחוזים בין 0 ל-100";
            // 
            // textBox_red_rectangle_size
            // 
            this.textBox_red_rectangle_size.Location = new System.Drawing.Point(268, 196);
            this.textBox_red_rectangle_size.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_red_rectangle_size.Name = "textBox_red_rectangle_size";
            this.textBox_red_rectangle_size.Size = new System.Drawing.Size(27, 20);
            this.textBox_red_rectangle_size.TabIndex = 27;
            this.textBox_red_rectangle_size.Text = "50";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Indigo;
            this.label14.Location = new System.Drawing.Point(178, 141);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 17);
            this.label14.TabIndex = 26;
            this.label14.Text = "גודל הריבוע האדום ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(110, 323);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(215, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "גודל הריבוע הכחול באחוזים בין 0 ל-100";
            // 
            // textBox_blue_rectangle_size
            // 
            this.textBox_blue_rectangle_size.Location = new System.Drawing.Point(261, 350);
            this.textBox_blue_rectangle_size.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_blue_rectangle_size.Name = "textBox_blue_rectangle_size";
            this.textBox_blue_rectangle_size.Size = new System.Drawing.Size(27, 20);
            this.textBox_blue_rectangle_size.TabIndex = 30;
            this.textBox_blue_rectangle_size.Text = "50";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Indigo;
            this.label16.Location = new System.Drawing.Point(170, 295);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 17);
            this.label16.TabIndex = 29;
            this.label16.Text = "גודל הריבוע הכחול ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(362, 323);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(161, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "גודל אזור הרחמים בין 0 ל-100";
            // 
            // textBox_mercy_zone
            // 
            this.textBox_mercy_zone.Location = new System.Drawing.Point(471, 350);
            this.textBox_mercy_zone.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_mercy_zone.Name = "textBox_mercy_zone";
            this.textBox_mercy_zone.Size = new System.Drawing.Size(27, 20);
            this.textBox_mercy_zone.TabIndex = 33;
            this.textBox_mercy_zone.Text = "10";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Indigo;
            this.label18.Location = new System.Drawing.Point(376, 295);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 17);
            this.label18.TabIndex = 32;
            this.label18.Text = "גודל אזור הרחמים  ";
            // 
            // label19
            // 
            this.label19.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label19.AllowDrop = true;
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label19.Location = new System.Drawing.Point(325, 60);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 17);
            this.label19.TabIndex = 35;
            this.label19.Text = "לא מילה שמתחילה ב";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.UseCompatibleTextRendering = true;
            // 
            // label20
            // 
            this.label20.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label20.AllowDrop = true;
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.MediumPurple;
            this.label20.Location = new System.Drawing.Point(294, 60);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 17);
            this.label20.TabIndex = 36;
            this.label20.Text = "Test-";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label20.UseCompatibleTextRendering = true;
            // 
            // experiment_details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 477);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBox_mercy_zone);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBox_blue_rectangle_size);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBox_red_rectangle_size);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBox_size_of_circle);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox_for_main_mission);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox_name_file);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox_for_small_mission);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButton_d);
            this.Controls.Add(this.radioButton_p);
            this.Controls.Add(this.radioButton_m);
            this.Controls.Add(this.button_create);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "experiment_details";
            this.Text = "experiment_details";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_create;
        private System.Windows.Forms.RadioButton radioButton_m;
        private System.Windows.Forms.RadioButton radioButton_p;
        private System.Windows.Forms.RadioButton radioButton_d;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_for_small_mission;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_name_file;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_for_main_mission;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_size_of_circle;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_red_rectangle_size;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox_blue_rectangle_size;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox_mercy_zone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
    }
}