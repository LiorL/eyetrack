﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
/********************************************************************
 * this class is form that represent the level and speed of the     *
 * subject.                                                         *
 * this form built with 2 listbox.                                  *
 * the level is between 1-3 difficult level.                        *
 * the speed is between 1-10 .                                      *
 * Note that you don't must to fill select the item                 *
 * there are a defult values for the level is 3 and for the speed   *
 * is 5.                                                            *
 * the details                                                      *
 * writen by Eliya ben-abu                                          *
 ********************************************************************/
namespace hend_track
{
    public partial class level : Form
    {
        public level()
        {
            InitializeComponent();
        }

        private void button_insert_Click(object sender, EventArgs e)
        {
            globals.level = (int)listBox_level.SelectedItem;
            globals.speed = (int)listBox_speed.SelectedItem;
        }
    }
}
